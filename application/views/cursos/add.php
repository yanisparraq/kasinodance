<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Crear Cursos</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">                        
          <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Inicio</a>
          </li>
          <li class="breadcrumb-item"><a  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>usuarios/','#resultado2');">Crear Cursos</a>
          </li>
          <li class="breadcrumb-item active">Crear Cursos</li>             
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->    
</div> 
    <div class="card shadow mb-4">   
        <?php
        if($this->session->flashdata('mensaje')!='')
            {
               ?>
               <div class="alert alert-<?php echo $this->session->flashdata('css')?>"><?php echo $this->session->flashdata('mensaje')?></div>
               <?php 
            }
            ?>    
        <div class="card-body">
    <!-- Nested Row within Card Body -->
             <?php
        //acá visualizamos los mensajes de error
            $errors=validation_errors('<li>','</li>');
            if($errors!="")
            {
                ?>
                <div class="alert alert-danger">
                    <ul>
                        <?php echo $errors;?>
                    </ul>
                </div>
                <?php
            } ?> 
            <form action="<?php echo base_url();?>cursos/add" id="loginform" method="post">
            <div class="row "> 
                <div class="col-lg-6 col-md-12 ">
                    <label for="correo">Nombre del Curso:</label>
                      <select   class="form-control" name="id_categoria" id="id_categoria" >
                <option value=''>Seleccione Categoria del Curso</option>    
                  <?php 
                  foreach($datos_Cursos as $dato_Curso)
                  {
                    
                      ?>
                      <option value="<?php echo $dato_Curso->id?>"><B><?php echo $dato_Curso->nombre_categoria?></B></option>            
                      <?php
                       
                   
                  } ?> 

                            </select>   
                </div> 
                <div class="col-lg-6 col-md-12 ">
                    <label for="correo">Profesor:</label>
                   <select   class="form-control" name="id_usuario_profesor" id="id_usuario_profesor" >
                <option value=''>Selecciones Profesor</option>    
                  <?php 
                  foreach($datos as $dato)
                  {
                    
                      ?>
                      <option value="<?php echo $dato->id?>"><B><?php echo $dato->nombre_de_usuario?></B></option>            
                      <?php
                  } ?> 

                            </select>
                        </div> 
                <div class="col-lg-4 col-md-12 ">
                    <label for="correo">Nivel:</label>
                   <select   class="form-control" name="nivel" id="nivel" >
                <option value=''>Selecciones Nivel</option>    
                  <?php 
                  foreach($datos_niveles as $nivele)
                  {
                    
                      ?>
                      <option value="<?php echo $nivele->id?>"><B><?php echo $nivele->nombre_nivel?></B></option>            
                      <?php
                       
                   
                  } ?> 

                            </select>
                        </div> 

                 <div class="col-lg-4 col-md-12 ">
                     <label> Período:</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control float-right" value="reservation" name="reservation" id="reservation">
                  </div>

                </div>
               

                <div class="col-lg-4 col-md-12 ">
                        <label for="correo">Costo:</label>
                        <input type="number" id="costo"name="costo" value="" class="form-control" />
                </div>
            </div>  
            <div class="col-lg-12 col-md-12 " style="text-align: center;">
                <hr>
                <button type="submit" style="text-align: center;" class="btn btn-success btn-icon-split">                          
                    <span class="icon text-white-50 aling-center">
                        <i class="fas fa-plus"></i>
                    </span>
                    <span class="text">Guardar</span>
                </button>
                            <?php echo form_close();?> 
            </div>   
                </div> 
               
        </div>
    </div>
</div>
<script>

      $(document).ready(function() { 
    $('#loginform').submit(function(e) {
      // alert(765764764);
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>/cursos/add',
            data: $(this).serialize(),
            success: function(result)
            {              
               $('#resultado2').html(result);
           }
       });
     });
});
 
  $(function () {
    //Initialize Select2 Elements


  
    //Date picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });

    //Date and time picker
    $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    
  })


  // DropzoneJS Demo Code Start




  // Update the total progress bar



  // Setup the buttons for all transfers
  // The "add files" button doesn't need to be setup because the config
  // `clickable` has already been specified.

  // DropzoneJS Demo Code End
</script>
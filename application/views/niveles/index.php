    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Niveles Por Edad</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Inicio</a></li>
              <li class="breadcrumb-item active">Niveles</li>
             
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
       <div id="chat">
        </div>
    </div>          
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">                
        <a href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>niveles/add','#resultado2');" class="btn btn-success btn-sm btn-icon-split">
            <span class="icon text-white-50">
              <i class="fas fa-plus"></i>
            </span>
            <span class="text">Agregar Nivel</span>
        </a></h6>
 
     



                                 <?php
    if($this->session->flashdata('mensaje')!='')
    {
       ?>
      
          <script> 
       alertas('<?php echo $this->session->flashdata('css')?>','<?php echo $this->session->flashdata('mensaje')?>');
       </script>
          

      
       <?php 
    }
    ?>  
  

    </div>
    <div class="card-body">
      <div class="table-responsive">


     <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Nombre del Nivel</th>
 
              <th>Edad </th> 
              <th>Accion</th>           
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Nombre del Nivel</th>
 
              <th>Edad </th> 
              <th>Accion</th>        
            </tr>
          </tfoot>
          <tbody>
<?php
                foreach($datos as $dato)
                {                  
                    ?>
                    <tr>
                        <td><?php echo $dato->nombre_nivel?></td>
                        <td>De <?php echo $dato->edad_minima?> a <?php echo $dato->edad_mayor?> años</td>
                      
                        
                        <td  class="text-center" >
                       <div class="btn-group btn-group-sm">
                     
                          <a href="javascript:void(0);" onclick="editar('<?php echo base_url()?>niveles/edit/<?php echo $dato->id?>')" class="btn btn-success"><i class="fas fa-pencil-alt"></i></a>
                          <a href="javascript:void(0);" onclick="eliminar('<?php echo base_url()?>niveles/delete/<?php echo $dato->id?>')" class="btn btn-danger"><i class="fas fa-trash"></i></a> 
                      </div>
                        </td>
                    </tr>
                    <?php
                }
            ?>        
            <tr>       
          </tbody>
        </table>         
      </div>
    </div>
  </div>
</div>        
<br>   
<br>    
 

  
 
 


        
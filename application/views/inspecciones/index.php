    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Informe de Inspección </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Inicio</a></li>
              <li class="breadcrumb-item active">Informe de Inspección</li>
             
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
       <div id="chat">
        </div>
    </div>          
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">                
        <a href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>inspecciones/add','#resultado2');" class="btn btn-success btn-sm btn-icon-split">
            <span class="icon text-white-50">
              <i class="fas fa-plus"></i>
            </span>
            <span class="text">Agregar Inspección</span>
        </a></h6>
 
    </div>
    <div class="card-body">
      <div class="table-responsive">


     <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Nº</th>
            
              
              <th>Emisor</th>
              <th>Fecha de Públicación</th>

             
                <th>Acción</th>
           
            </tr>
          </thead>
          <tfoot>
            <tr>
                <th>Nº</th>

            
              
              <th>Emisor</th>
              <th>Fecha de Públicación</th>

             
                <th>Acción</th>
            </tr>
          </tfoot>
          <tbody>
<?php

                foreach($datos as $dato)
                {
                  
                    ?>
                    <tr>
                        <td><?php echo $dato->id_documento?></td>
                     
                        <td>
                            <?php  $usuario=$this->usuarios_model->getTodosPorId($dato->id_usuario);

                             echo $usuario->nombre_de_usuario .' '. $usuario->apellido?>
                               
                             </td>
                         <td class="text-center"><?php  echo  fecha($dato->fecha_registro) ?></td>
                     

                        <td  class="text-center" >
                            <a href="javascript:void(0);"onclick="editar('<?php echo base_url()?>inspecciones/edit/<?php echo $dato->id_documento?>/<?php echo $pagina?>')"><span class="fas fa-edit text-center mr-2" aria-hidden="true"></span></a>
                                 <a href="<?php echo base_url()?>inspecciones/doc2/<?php echo $dato->id_documento?>" target="blak"><span class="fas fa-download text-center mr-2" aria-hidden="true"></span></a>
                            <a href="javascript:void(0);" onclick="" ><span class="fas fa-trash" aria-hidden="true"></span></a>
                        </td>
                    </tr>
                    <?php
                }
            ?>
        
            <tr>
       
          </tbody>
        </table>
          
                        </div>
    </div>
  </div>
</div>
     <?php
 echo $this->pagination->create_links()?>
<br>    
 

                       <?php
    if($this->session->flashdata('mensaje')!='')
    {
       ?>
      
          <script> 
       alertas('<?php echo $this->session->flashdata('css')?>','<?php echo $this->session->flashdata('mensaje')?>');
       </script>
          

      
       <?php 
    }
    ?>  



 
  

 
 


        
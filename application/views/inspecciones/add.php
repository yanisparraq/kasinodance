    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Informe de Inspección</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                        
              <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Inicio</a></li>


              <li class="breadcrumb-item"><a  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>usuarios/','#resultado2');">Informe de Inspección</a></li>
              <li class="breadcrumb-item active">Crear Inspección</li>
             
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
       <div id="chat">
        </div>
    </div> 
<div class="card shadow mb-4">
   
                <?php
        if($this->session->flashdata('mensaje')!='')
            {
               ?>
               <div class="alert alert-<?php echo $this->session->flashdata('css')?>"><?php echo $this->session->flashdata('mensaje')?></div>
               <?php 
            }
            ?>
    
            <div class="card-body">
                <!-- Nested Row within Card Body -->
                         <?php
                    //acá visualizamos los mensajes de error
                        $errors=validation_errors('<li>','</li>');
                        if($errors!="")
                        {
                            ?>
                            <div class="alert alert-danger">
                                <ul>
                                    <?php echo $errors;?>
                                </ul>
                            </div>
                            <?php
                        } ?> 
 <form action="<?php echo base_url();?>/inspecciones/add" id="loginform" method="post">
        <div class="row"> 
            <div class="input-group mb-3 col-lg-3 col-md-12">
              <input type="text" class="form-control"  name="nsg"  id="nsg" placeholder="N.S.G." title="N.S.G." required autocomplete="off">
             
            </div>

            <div class="input-group mb-3 col-lg-3 col-md-12">
              <input type="text" class="form-control"  name="serie"  id="serie" placeholder="Serie" title="Serie" required autocomplete="off">
             
            </div>
 
            <div class="input-group  mb-3 col-lg-3 col-md-12">
              <input type="text" class="form-control" name="articulo" id="articulo"placeholder="Articulo" title="Articulo"id="articulo" required autocomplete="off">
             
            </div> 
            <div class="input-group  mb-3 col-lg-3 col-md-12">
              <input type="text" class="form-control" name="marca" id="marca" placeholder="Marca" title ="Marca" required autocomplete="off">
             
            </div> 
            <div class="input-group  mb-3 col-lg-4 col-md-12">
                <input type="text" class="form-control" name="modelo" id="modelo" placeholder="Modelo" title ="Modelo" required autocomplete="off">
              
            </div> 

             <div class="input-group  mb-3 col-lg-4 col-md-12">
                <input type="text" class="form-control" name="tipo" id="tipo" placeholder="Tipo" title ="Tipo" required autocomplete="off">

            </div>
            <div class="input-group  mb-3 col-lg-4 col-md-12">
                <input type="text" class="form-control" name="unidad" id="unidad" placeholder="Unidad" title ="Unidad" required autocomplete="off">
               
            </div>
            <div class="input-group  mb-3 col-lg-6 col-md-12">
                <input type="text" class="form-control" name="estadofisico" id="estadofisico" placeholder="ESTADO FISICO DEL EQUIPO CAT" title ="ESTADO FISICO DEL EQUIPO CAT" required autocomplete="off">
               
            </div>

            <div class="input-group  mb-3 col-lg-6 col-md-12">
                <input type="text" class="form-control" name="situación_Actual" id="situación_Actual" placeholder="Situación Actual" title ="Situación Actual" required autocomplete="off">
  
            </div>

  
        </div>
       <div class="row"> 
           
           <div class="col-lg-4 col-md-12 mb-3 ">
                 <label for="correo">Color:</label>   
                <input type="text" id = "color" name="color" value="" class="form-control" placeholder="Color" title ="Color" required autocomplete="off">
              
                 
            </div>
                       <div class="col-lg-4 col-md-12 mb-3 ">
                 <label for="correo">Fecha de Fabrica:</label>   
                <input type="date" id = "fecha_fabrica" name="fecha_fabrica" value="" class="form-control"  required autocomplete="off">
              
                 
            </div>
            
            <div class="col-lg-4 col-md-12 mb-3">
                <label for="correo">Fecha de Reparación:</label>    
                <input type="date" id = "fecha_reparacion" name="fecha_reparacion" value="" class="form-control" required autocomplete="off">
            </div>
             <div class="col-lg-4 col-md-12 mb-3">
                      <!-- textarea -->
                      <div class="form-group">
                        <label>Problemas que Presenta:</label>
                        <textarea class="form-control" rows="3" name="problema" id="problema" placeholder="Enter ..."></textarea>
                      </div>
                    </div>
             <div class="col-lg-4 col-md-12 mb-3">
                    <div class="form-group">
                        <label>Analísis</label>
                        <textarea class="form-control" rows="3" name="analisi" id="analisi" placeholder="Enter ..."></textarea>
                      </div>
                    </div>
             <div class="col-lg-4 col-md-12 mb-3">
                    <div class="form-group">
                        <label>Recomendaciones</label>
                        <textarea class="form-control" name="recomendacion" id="recomendacion" rows="3" placeholder="Enter ..."></textarea>
                      </div>
                    </div>
              <div class="col-12 mb-3">
            <button type="submit" class="btn btn-primary btn-block">Registrar</button>
          </div>




        </div>
        
        

               


        <div class="row">
          
               <!-- /.col -->
        
          <!-- /.col -->
        </div>


      </form>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
<script>

  $(document).ready(function() { 
    $('#loginform').submit(function(e) {
        
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>/inspecciones/add',
            data: $(this).serialize(),
            success: function(result)
            {
              
               $('#resultado2').html(result);

           }
       });
     });
});

  $(function () {


  
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
 



    //Date range as a button
 var pais = document.getElementById('codigo');
var telf = document.getElementById('telefono');
 
pais.onchange = function(e) {
 
  //document.getElementById("telefono").focus({preventScroll:false});
  if((this.value).trim() != '') {
    telf.disabled = false;
  } else 
  {
    telf.disabled = true
  }
}

  })





</script>

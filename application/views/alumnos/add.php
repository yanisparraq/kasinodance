    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Agregar Alumnos</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">                        
              <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Inicio</a></li>
              <li class="breadcrumb-item"><a  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>alumnos/','#resultado2');">Alumnos</a></li>
              <li class="breadcrumb-item active">Agregar Alumnos</li>             
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div> 
<div class="card shadow mb-4">   
                <?php
        if($this->session->flashdata('mensaje')!='')
            {
               ?>
               <div class="alert alert-<?php echo $this->session->flashdata('css')?>"><?php echo $this->session->flashdata('mensaje')?></div>
               <?php 
            }
            ?>    
            <div class="card-body">
                <!-- Nested Row within Card Body -->
                         <?php
                    //acá visualizamos los mensajes de error
                        $errors=validation_errors('<li>','</li>');
                        if($errors!="")
                        {
                            ?>
                            <div class="alert alert-danger">
                                <ul>
                                    <?php echo $errors;?>
                                </ul>
                            </div>
                            <?php
                        } ?> 
                <form action="<?php echo base_url();?>/Alumnos/add" id="loginform" method="post">
                <div class="row "> 
                    <div class="col-lg-3 col-md-12 ">
                            <label for="correo">Primer Nombre:</label>
                            <input type="text" id="nombre1"name="nombre1" value="" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ]+" autocomplete="off" class="form-control" title="* Solo letras sin espacios." />   
                    </div> 

                    <div class="col-lg-3 col-md-12 ">
                            <label for="correo">Segundo Nombre:</label>
                            <input type="text" id="nombre2"name="nombre2" value="" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ]+" autocomplete="off" class="form-control" title="* Solo letras sin espacios."  />   
                    </div> 
                    <div class="col-lg-3 col-md-12 ">
                            <label for="correo">Primer Apellido:</label>
                            <input type="text" id="apellido"name="apellido" value="" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ]+" autocomplete="off" class="form-control" title="* Solo letras sin espacios."  />   
                    </div> 
                    <div class="col-lg-3 col-md-12 ">
                            <label for="correo">Segundo Apellido:</label>
                            <input type="text" id="apellido2"name="apellido2" value="" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ]+" autocomplete="off" class="form-control" title="* Solo letras sin espacios."  />   
                    </div>
                    <div class="col-lg-4 col-md-12 mt-2 ">
                            <label for="password">C.I:</label>
                            <input type="number" id="dni" name="dni" value=""required  autocomplete="off" class="form-control" title="* Solo letras sin espacios."  />
                    </div>

                      <div class="col-lg-4 col-md-12  mt-2">
                            <label for="password">Telefono Móvil:</label>
                            <input type="text" id="telefono_movil" name="telefono_movil"  autocomplete="off" value="" class="form-control" />
                      </div>
                      <div class="col-lg-4 col-md-12  mt-2">
                            <label for="password">Telefono Fijo:</label>
                            <input type="text" id="telefono_fijo" name="telefono_fijo" value="" autocomplete="off" class="form-control" />
                      </div>

                    <div class="col-lg-4 col-md-12  mt-2 ">
                        <label>Dirección de Habitación</label>
                        <textarea class="form-control" rows="2"  required id="direccion_habitacion" autocomplete="off" name="direccion_habitacion" ></textarea>
                      </div>
                      <div class="col-lg-4 col-md-12  mt-2">
                        <label>Dirección de Trabajo</label>
                        <textarea class="form-control" rows="2" id="direccion_trabajo" required autocomplete="off" name="direccion_trabajo" ></textarea>
                      </div>
                    <div class="col-lg-4 col-md-12  mt-2">
                        <label for="correo">Fecha de Nacimiento:</label>    
                        <input type="date" id = "fecha_nacimiento"  name="fecha_nacimiento" autocomplete="off" value="" class="form-control" required autocomplete="off">
                    </div>
                    <div class="col-lg-3 col-md-12  mt-2">
                            <label for="correo">Correo:</label>
                            <input type="email" id="correo"name="correo" value="" required="" autocomplete="off"class="form-control" />   
                    </div>
                    <div class="col-lg-3 col-md-12  mt-2">
                            <label for="password">Instagran:</label>
                            <input type="text" id="instagran" name="instagran" required="" autocomplete="off"value="" class="form-control" />
                    </div>
                    <div class="col-lg-3 col-md-12  mt-2 ">
                            <label for="password">Facebook:</label>
                            <input type="text" id="facebook" name="facebook" required="" value="" autocomplete="off" class="form-control" />
                    </div>

                    <div class="col-lg-3 col-md-12  mt-2">                    
                            <label for="password2">Tik tok:</label>
                            <input type="text" id="tiltok"name="tiltok" value="" required="" autocomplete="off" class="form-control" />                 
                    </div>
                
                    </div>    
                     
                                         
                    <div class="col-lg-12 col-md-12 " style="text-align: center;">                                    
<hr>
 <button style="text-align: center;" type="submit" class="btn btn-success btn-icon-split"> 
                          
                                <span class="icon text-white-50 aling-center">
                                    <i class="fas fa-plus"></i>
                                </span>
                                <span  type="submit" class="text">Guardar</span>
                            </button>
                           
                    </div>                
                </div>
            </div>
        </div>
    </div>
</div>
<script>

  $(document).ready(function() { 
    $('#loginform').submit(function(e) {
        
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>/alumnos/add',
            data: $(this).serialize(),
            success: function(result)
            {
              
               $('#resultado2').html(result);

           }
       });
     });
});

  $(function () {


  
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
 



    //Date range as a button
 var pais = document.getElementById('codigo');
var telf = document.getElementById('telefono');
 
pais.onchange = function(e) {
 
  //document.getElementById("telefono").focus({preventScroll:false});
  if((this.value).trim() != '') {
    telf.disabled = false;
  } else 
  {
    telf.disabled = true
  }
}

  })





</script>

               
 <p class="login-box-msg">Inicia sesión para comenzar</p>
<?php echo form_open(null,array('name'=>'form','class'=>'user'));?>
<?php
            if($this->session->flashdata('mensaje')!='')
            {
               ?>
               <div class="alert alert-<?php echo $this->session->flashdata('css')?>"><?php echo $this->session->flashdata('mensaje')?></div>
               <?php 
            }
            ?>
<?php
                //acá visualizamos los mensajes de error
                $errors=validation_errors('<li>','</li>');
                if($errors!="")
                {
                    ?>
                    <div class="alert alert-danger">
                        <ul>
                            <?php echo $errors;?>
                        </ul>
                    </div>
                    <?php
                }
                ?>
<div class="form-group">

    <div class="input-group mb-3">
      <input type="email" class="form-control form-control-user" name="correo" placeholder="Email">
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-envelope"></span>
          </div>
        </div>
      </div>
    </div>
    <div class="input-group mb-3">
      <input type="password" name="pass" class="form-control form-control-user" placeholder="Password">
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-lock"></span>
          </div>
        </div>
    </div> 
    
 <div class="row">
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block ">Iniciar</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <div class="social-auth-links text-center mb-3">
        <hr>
        <a href="#" class="btn btn-block btn-primary ">
           Olvidé mi contraseña
        </a>
        <a href="" class="btn btn-block btn-danger">
           Crear Cuenta
        </a>
      </div>
      <!-- /.social-auth-links -->

     
    </div>
    <!-- /.login-card-body -->
  </div>
                   
<?php echo form_close();?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Sistema V1.0</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
   <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/toastr/toastr.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
 


  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
   <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/bs-stepper/css/bs-stepper.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/summernote/summernote-bs4.min.css">
</head>
<body class="sidebar-mini layout-fixed sidebar-collapse">
<div class="wrapper">
<div id="loading-screen" style="display: none;background-color: rgba(25,25,25,0.7);
  height: 100%;
  width: 100%;
  position: fixed;
  z-index: 9999;
  margin-top: 0;
  top: 0;
  text-align: center">
  <div class="spinner-border  text-primary" id = "progress" style="width: 3rem; height: 3rem;width: 100px;
  height: 100px;
  position: relative;
  margin-top: -50px;
  margin-left: -50px;
  top: 50%;
  font-size: 30px; " >       
  </div> 
</div>
  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="<?php echo base_url();?>public/dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
  </div>
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url()?>" class="nav-link">Inicio</a>
      </li>
    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->      
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>     
    </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="<?php echo base_url()?>public/dist/img/AdminLTELogo.png" alt="Sistema V1.0" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <a  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>perfil/','#resultado2');clearInterval(alex);" class="d-block"><?php echo $this->session->userdata('nombre_de_usuario')." ".$this->session->userdata('apellido')?></a>
        </div>
        <div class="info">

        </div>
      </div>
<nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          
          <li class="nav-item">
            <a href="#" class="nav-link">
             <i class="nav-icon fas fa-chalkboard-teacher"></i>
              <p>
                Alumnos
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right">2</span>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                 <a href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>alumnos/','#resultado2');" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Adultos</p>
                </a>
              </li>
              <li class="nav-item">
               <a href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>data_alumnas/','#resultado2');" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Niños</p>
                </a>
              </li>
              
            </ul>
          </li>
          
          <li class="nav-header">CONFIGURACIONES</li>
          <li class="nav-item">
            <a  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>usuarios/','#resultado2');clearInterval(alex);" class="nav-link">
              <i class="nav-icon fas  fa-user-alt"></i>
              <p>
                Usuarios              
              </p>
            </a>
          </li>        
      <li class="nav-item">
             <a  href="<?php echo base_url()?>acceso/salir"  class="nav-link">
              <i class="nav-icon fas  fas fa-door-open"></i>
              <p>
                Salir              
              </p>
            </a>
          </li>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>          
     
  <div class="content-wrapper">
    <div class="container-fluid" id="resultado2">
          <!-- Page Heading -->


    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <!-- Main content -->
        <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Agregar Alumnos</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">                        
              <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Inicio</a></li>
              <li class="breadcrumb-item"><a  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>alumnos/','#resultado2');">Usuarios</a></li>
              <li class="breadcrumb-item active">Agregar Alumnos</li>             
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div> 
    
          <!-- Page Heading -->


 
          <div class="col-md-12">
            <div class="card card-default">
  
              <div class="card-body p-0">
                <div class="bs-stepper">
                  <div class="bs-stepper-header" role="tablist">
                    <!-- your steps here -->
                    <div class="step" data-target="#logins-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="logins-part" id="logins-part-trigger">
                        <span class="bs-stepper-circle">1</span>
                        <span class="bs-stepper-label">Datos Personales</span>
                      </button>
                    </div>
                    <div class="line"></div>
                    <div class="step" data-target="#information-part2">
                      <button type="button" class="step-trigger" role="tab" aria-controls="information-part" id="information-part-trigger">
                        <span class="bs-stepper-circle">2</span>
                        <span class="bs-stepper-label">Datos de la Madre</span>
                      </button>
                    </div>
                     <div class="line"></div>
                    <div class="step" data-target="#information-part3">
                      <button type="button" class="step-trigger" role="tab" aria-controls="information-part" id="information-part-trigger">
                        <span class="bs-stepper-circle">3</span>
                        <span class="bs-stepper-label">Datos del Padre</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#information-part4">
                      <button type="button" class="step-trigger" role="tab" aria-controls="information-part" id="information-part-trigger">
                        <span class="bs-stepper-circle">4</span>
                        <span class="bs-stepper-label">Persona que Inscribe al Niño</span>
                      </button>
                    </div>
                  </div>

                  <div class="bs-stepper-content">
                    <!-- your steps content here -->
                    <div id="logins-part" class="content" role="tabpanel" aria-labelledby="logins-part-trigger">
            
                    <div class="row "> 
                      <div class="col-lg-3 col-md-12 ">
                              <label for="correo">Primer Nombre:</label>
                              <input type="text" id="nombre1"name="nombre1" value="" class="form-control" />   
                      </div> 

                      <div class="col-lg-3 col-md-12 ">
                              <label for="correo">Segundo Nombre:</label>
                              <input type="text" id="nombre2"name="nombre2" value="" class="form-control" />   
                      </div> 
                      <div class="col-lg-3 col-md-12 ">
                              <label for="correo">Primer Apellido:</label>
                              <input type="text" id="apellido"name="apellido" value="" required="" class="form-control" />   
                      </div> 
                      <div class="col-lg-3 col-md-12 ">
                              <label for="correo">Segundo Apellido:</label>
                              <input type="text" id="apellido2"name="apellido2" value="" class="form-control" />   
                      </div>
                      <div class="col-lg-3 col-md-12 mt-2 ">
                            <label for="password">C.I:</label>
                            <input type="text" id="dni" name="dni" value="" class="form-control" />
                      </div>
                      <div class="col-lg-3 col-md-12  mt-2">
                        <label>Lugar de Nacimiento</label>
                        <textarea class="form-control" rows="1" id="lugar_nacimiento" name="lugar_nacimiento" ></textarea>
                      </div>
                                         <div class="col-lg-3 col-md-12  mt-2">
                        <label for="correo">Fecha Nacimiento:</label>    
                        <input type="date" id = "fecha_nacimiento" name="fecha_nacimiento" value="" class="form-control" required autocomplete="off">
                    </div>
 

                      <div class="col-lg-3 col-md-12 mt-2">
                              <label for="correo">Talla Malla:</label>
                              <input type="text" id="malla"name="malla" value="" class="form-control" />   
                      </div> 
                      <div class="col-lg-3 col-md-12 mt-2">
                              <label for="correo">Talla Zapatilla:</label>
                              <input type="text" id="zapatilla"name="zapatilla" value="" class="form-control" />   
                      </div> 
                      <div class="col-lg-3 col-md-12 mt-2 ">
                              <label for="correo">Tacon:</label>
                              <input type="text" id="tacon"name="tacon" value="" class="form-control" />   
                      </div> 
                      <div class="col-lg-3 col-md-12 mt-2 ">
                              <label for="correo">Peso:</label>
                              <input type="text" id="peso"name="peso" value="" class="form-control" />   
                      </div> 




                      <div class="col-lg-3 col-md-12 mt-2">
                              <label for="correo">Tipo de Sangre:</label>
                              <input type="text" id="sangre"name="sangre" value="" class="form-control" />   
                      </div> 
                      <div class="col-lg-3 col-md-12 mt-2">
                              <label for="correo">Año Escolar:</label>
                              <input type="text" id="anio_escolar"name="anio_escolar" value="" class="form-control" />   
                      </div> 
                      <div class="col-lg-3 col-md-12 mt-2 ">
                              <label for="correo">Colegio:</label>
                              <input type="text" id="colegio"name="colegio" value="" class="form-control" />   
                      </div> 
                      <div class="col-lg-3 col-md-12 mt-2 ">
                              <label for="correo">Poliza de Seguro:</label>
                              <input type="text" id="seguro"name="seguro" value="" class="form-control" />   
                      </div>    
                      

                      <div class="col-lg-3 col-md-12 mt-2">
                              <label for="correo">Nombre del Seguro:</label>
                              <input type="text" id="nombre_seguro"name="nombre_seguro" value="" class="form-control" />   
                      </div> 
                      <div class="col-lg-12 col-md-12  mt-2">
                        <label>Enfermedad o Limitaciones físicas  que requieran tratamiento actual:</label>
                        <textarea class="form-control" rows="1" id="limitaciones" name="limitaciones" ></textarea>
                      </div>
                                                                             
                    </div>
                     <div class="col-lg-12 col-md-12 " style="text-align: center;">                                    
                      <hr>

                           
                    </div>  
                    <div class="col-lg-3 col-md-12 ">
                              <button class="btn btn-primary" onclick="stepper.next()">Siguiente</button>  
                      </div>
                    </div>
                   
                    <div id="information-part2" class="content" role="tabpanel" aria-labelledby="information-part-trigger">
                      <div class="form-group">
                    <div class="row "> 
                                           <div class="col-lg-4 col-md-12 ">
                              <label for="correo">Nombre Madre:</label>
                              <input type="text" id="nombre_madre"name="nombre_madre" value="" class="form-control" />   
                      </div> 
                      <div class="col-lg-4 col-md-12 ">
                              <label for="correo">Apellido Madre:</label>
                              <input type="text" id="apellido_madre"name="apellido_madre" value="" class="form-control" />   
                      </div>
                      <div class="col-lg-4 col-md-12">
                            <label for="password">C.I Madre:</label>
                            <input type="text" id="dni_madre" name="dni_madre" value="" class="form-control" />
                      </div>
                     <div class="col-lg-3 col-md-12  mt-2">
                        <label>Dirección:</label>
                        <textarea class="form-control" rows="1" id="direccion_habitacion_m" name="direccion_habitacion_m" ></textarea>
                      </div>
                      <div class="col-lg-3 col-md-12  mt-2">
                            <label for="password">Telefono Mobil:</label>
                            <input type="text" id="telefono_m" name="telefono_m" value="" class="form-control" />
                      </div>
                                           <div class="col-lg-3 col-md-12  mt-2">
                        <label>Dirección Trabajo:</label>
                        <textarea class="form-control" rows="1" id="direccion_trabajo_m" name="direccion_trabajo_m" ></textarea>
                      </div>
                      <div class="col-lg-3 col-md-12  mt-2">
                            <label for="password">Telefono Trabajo:</label>
                            <input type="text" id="telefono_trabajo_m" name="telefono_trabajo_m" value="" class="form-control" />
                      </div>



                      <div class="col-lg-3 col-md-12  mt-2">
                            <label for="correo">Correo:</label>
                            <input type="email" id="correo_madre"name="correo_madre" value="" class="form-control" />   
                    </div>
                                                                                
                    </div>
                      </div>
                      <button class="btn btn-primary" onclick="stepper.previous()">Anterior</button>

                      <button class="btn btn-primary" onclick="stepper.next()">Siguiente</button>  
                     
                    </div>














                    <div id="information-part3" class="content" role="tabpanel" aria-labelledby="information-part-trigger">
                    <div class="form-group">
                    <div class="row "> 
                                           <div class="col-lg-4 col-md-12 ">
                              <label for="correo">Nombre Padre:</label>
                              <input type="text" id="nombre_padre"name="nombre_padre" value="" class="form-control" />   
                      </div> 
                      <div class="col-lg-4 col-md-12 ">
                              <label for="correo">Apellido Padre:</label>
                              <input type="text" id="apellido_padre"name="apellido_padre" value="" class="form-control" />   
                      </div>
                      <div class="col-lg-4 col-md-12  ">
                            <label for="password">C.I Padre:</label>
                            <input type="text" id="dni_padre" name="dni_padre" value="" class="form-control" />
                      </div>
                     <div class="col-lg-3 col-md-12  mt-2">
                        <label>Dirección:</label>
                        <textarea class="form-control" rows="1" id="direccion_habitacion_p" name="direccion_habitacion_p" ></textarea>
                      </div>
                      <div class="col-lg-3 col-md-12  mt-2">
                            <label for="password">Telefono Mobil:</label>
                            <input type="text" id="telefono_p" name="telefono_p" value="" class="form-control" />
                      </div>
                                           <div class="col-lg-3 col-md-12  mt-2">
                        <label>Dirección Trabajo:</label>
                        <textarea class="form-control" rows="1" id="direccion_trabajo_p" name="direccion_trabajo_p" ></textarea>
                      </div>
                      <div class="col-lg-3 col-md-12  mt-2">
                            <label for="password">Telefono Trabajo:</label>
                            <input type="text" id="telefono_trabajo_p" name="telefono_movil" value="" class="form-control" />
                      </div>

                      <div class="col-lg-3 col-md-12  mt-2">
                            <label for="correo">Correo:</label>
                            <input type="email" id="correo_padre"name="correo_padre" value="" class="form-control" />   
                    </div>
                                                                                
                    </div>
                      </div>
                      <button class="btn btn-primary" onclick="stepper.previous()">Anterior</button>

                            <button class="btn btn-primary" onclick="stepper.next()">Siguiente</button>  

                      
                    </div>












 


                  <div id="information-part4" class="content" role="tabpanel" aria-labelledby="information-part-trigger">
                    <div class="form-group">
                    <div class="row "> 
                                           <div class="col-lg-4 col-md-12 ">
                              <label for="correo">Nombre Representante:</label>
                              <input type="text" id="nombre_representante"name="nombre_representante" value="" class="form-control" />   
                      </div> 
                      <div class="col-lg-4 col-md-12 ">
                              <label for="correo">Apellido Representante:</label>
                              <input type="text" id="apellido_epresentante"name="apellido_epresentante" value="" class="form-control" />  
                      </div>
                      <div class="col-lg-4 col-md-12  ">
                            <label for="password">C.I Representante:</label>
                            <input type="text" id="dni_representante" name="dni_representante" value="" class="form-control" />
                      </div>  
                      <div class="col-lg-4 col-md-12  ">
                            <label for="password">Correo:</label>
                            <input type="text" id="correo_representante" name="correo_representante" value="" class="form-control" />
                      </div>               
                                                    
                    </div>
                      </div>
                      <button class="btn btn-primary" onclick="stepper.previous()">Anterior</button>
   
                    <button style="text-align: center;" href="javascript:void(0);" onclick="addUser2('<?php echo base_url()?>Data_alumnas/add3','<?php echo base_url()?>alumnos/add2');" class="btn btn-success btn-icon-split"> 
                          
                                <span class="icon text-white-50 aling-center">
                                    <i class="fas fa-plus"></i>
                                </span>
                                <span class="text">Guardar</span>
                            </button>
                            <?php echo form_close();?> 
                    </div>
                  </div>
                </div>
              </div>
            </form>
              <!-- /.card-body -->
              
            </div>
            <!-- /.card -->
          </div> 
          </div>      
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Control Sidebar -->
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>public/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url();?>public/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url();?>public/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url();?>public/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>public/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?php echo base_url();?>public/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?php echo base_url();?>public/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>public/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url();?>public/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url();?>public/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url();?>public/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo base_url();?>public/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url();?>public/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>public/dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>public/plugins/bs-stepper/js/bs-stepper.min.js"></script>
<script src="<?php echo base_url();?>public/dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url();?>public/dist/js/pages/dashboard.js"></script>
<script src="<?php echo base_url();?>public/plugins/toastr/toastr.min.js"></script>

<script type="text/javascript" src="<?php echo base_url()?>public/js/funciones.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>public/js/push.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>public/js/ajax.js"></script>








<!-- Bootstrap 4 -->

<!-- SweetAlert2 -->
<script src="<?php echo base_url()?>public/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->

<!-- AdminLTE App -->
<script src="<?php echo base_url()?>public/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->

<script>
  
    // BS-Stepper Init
  document.addEventListener('DOMContentLoaded', function () {
    window.stepper = new Stepper(document.querySelector('.bs-stepper'))
  })

</script>


</body>
</html>


    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Detalles Alumno Niños</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">                        
              <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Inicio</a></li>
              <li class="breadcrumb-item"><a  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>data_alumnas/','#resultado2');">Alumnos</a></li>
              <li class="breadcrumb-item active">Detalles Alumno</li>             
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div> 
                     
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">



            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fas fa-globe"></i> Kasino Dance 
                    <small class="float-right"> <?php echo  fecha(date("Y-m-d")); ?> </small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  <address>
                    <strong>Nombres y Apellidos:</strong><br>
                    <?php echo $datos->nombre1." ".$datos->nombre2." ".$datos->apellido." ".$datos->apellido2; ?><br>
                    <strong>Cedula:</strong><?php echo $datos->dni; ?><br>
                     Fecha de Nacimiento:<br> <?php echo  fecha($datos->fecha_nacimiento); ?> <br>
                    Edad:  <?php echo calculaEdad($datos->fecha_nacimiento); ?>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                <address>
                    <strong>Direcciones:</strong><br>
                    Habitación: <?php echo $datos->direccion_habitacion; ?><br>                   
                    Trabjo: <?php echo $datos->direccion_habitacion; ?><br>
                    <strong>Email:</strong><br> <?php echo $datos->correo; ?>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <b>Telf. Móvil:</b>
                  <?php echo $datos->telefono_movil; ?><br>
                   <b>Telf. Fijo:</b>  <?php echo $datos->telefono_fijo; ?><br>
                  <b>Facebook:</b> <?php echo $datos->facebook; ?><br>
                  <b>Instagram:</b>  <?php echo $datos->instagran; ?><br>
                  <b>TikTok:</b> <?php echo $datos->tiltok; ?>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>#</th>
                      <th>Cursos</th>
                      <th >Nivel</th>
                      <th>Profesor</th>
                      <th>status</th>

                    </tr>
                    </thead>
                    <tbody>
                   

<?php
$j =1;
                foreach($cursos as $dato)
                {
                  
                    ?>
                    <tr>
                       <td><?php echo $j; ?></td>
                       <td><?php echo  $dato->nombre_categoria; ?></td>
                       <td><?php echo  $dato->nombre_nivel; ?></td>

                     
                      <td><?php echo  $dato->nombre_de_usuario; ?></td>
                     
                      <td>Activo</td>
                    </tr>
                    <?php
                    $j++;
                }
            ?>
                    
                 
                   
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->


                <!-- /.col -->
          
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                  <a href="<?php echo base_url()?>data_alumnas/imprimir/<?php echo $datos->id?>" rel="noopener" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Imprimir</a>
         
           
                </div>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section> 
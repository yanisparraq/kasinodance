<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Agregar Alumnos</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">                        
          <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Inicio</a></li>
          <li class="breadcrumb-item"><a  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>data_alumnas/','#resultado2');">Alumnos</a></li>
          <li class="breadcrumb-item active">Agregar Alumnos</li>             
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div> 
   
  <?php
      if($this->session->flashdata('mensaje')!='')
        {
        ?>
          <div class="alert alert-<?php echo $this->session->flashdata('css')?>"><?php echo $this->session->flashdata('mensaje')?></div>
               <?php 
        }
        ?>    

         <form action="<?php echo base_url()?>Data_alumnas/add3" id="loginform" method="post">
  <div class="card card-secondary">
    <div class="card-header">
      <h3 class="card-title">Datos Personales</h3>
    </div>
            <!-- /.card-header -->
      <div class="card-body">
        
          <div class="row">                     
            <div class="col-lg-3 col-md-12 ">
              <label for="correo">Primer Nombre:</label>
                <input type="text" id="nombre1"name="nombre1" value=""  required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ]+" autocomplete="off" class="form-control" title="* Solo letras sin espacios."  />   
            </div>
            <div class="col-lg-3 col-md-12 ">
              <label for="correo">Segundo Nombre:</label>
              <input type="text" id="nombre2"name="nombre2" value="" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ]+" autocomplete="off" class="form-control" title="* Solo letras sin espacios."  />    
            </div> 
            <div class="col-lg-3 col-md-12 ">
              <label for="correo">Primer Apellido:</label>
              <input type="text" id="apellido"name="apellido" value="" required="" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ]+" autocomplete="off" class="form-control" title="* Solo letras sin espacios."  />   
            </div> 
            <div class="col-lg-3 col-md-12 ">
              <label for="correo">Segundo Apellido:</label>
              <input type="text" id="apellido2"name="apellido2" value="" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ]+" autocomplete="off" class="form-control" title="* Solo letras sin espacios."  />      
            </div>
            <div class="col-lg-3 col-md-12 mt-2 ">
              <label for="password">C.I:</label>
                <input type="number" id="dni" name="dni" value="" required class="form-control" />
            </div>
            <div class="col-lg-3 col-md-12  mt-2">
              <label>Lugar de Nacimiento</label>
              <textarea class="form-control" rows="1" id="lugar_nacimiento" required name="lugar_nacimiento" ></textarea>
            </div>
            <div class="col-lg-3 col-md-12  mt-2">
              <label for="correo">Fecha Nacimiento:</label>    
              <input type="date" id = "fecha_nacimiento" name="fecha_nacimiento" required value="" class="form-control" required autocomplete="off">
            </div>
            <div class="col-lg-3 col-md-12 mt-2">
              <label for="correo">Talla Malla:</label>
              <input type="text" id="malla"name="malla" value="" required class="form-control" />   
            </div> 
            <div class="col-lg-3 col-md-12 mt-2">
              <label for="correo">Talla Zapatilla:</label>
              <input type="number" id="zapatilla"name="zapatilla" value=""  class="form-control" />   
            </div> 
            <div class="col-lg-3 col-md-12 mt-2 ">
                <label for="correo">Tacon:</label>
                <input type="number" id="tacon"name="tacon" value=""  class="form-control" />   
            </div> 
            <div class="col-lg-3 col-md-12 mt-2 ">
              <label for="correo">Peso:</label>
              <input type="number" id="peso"name="peso" value=""  class="form-control" />   
            </div>
            <div class="col-lg-3 col-md-12 mt-2">
              <label for="correo">Tipo de Sangre:</label>
              <input type="text" id="sangre"name="sangre" value="" required class="form-control" />   
            </div> 
            <div class="col-lg-3 col-md-12 mt-2">
              <label for="correo">Año Escolar:</label>
              <input type="number" id="anio_escolar"name="anio_escolar" value="" required class="form-control" />   
            </div> 
            <div class="col-lg-3 col-md-12 mt-2 ">
              <label for="correo">Colegio:</label>
              <input type="text" id="colegio"name="colegio" value="" required class="form-control" />   
            </div> 
            <div class="col-lg-3 col-md-12 mt-2 ">
              <label for="correo">Poliza de Seguro:</label>
              <input type="text" id="seguro"name="seguro" value="" required class="form-control" />   
            </div> 
            <div class="col-lg-3 col-md-12 mt-2">
              <label for="correo">Nombre del Seguro:</label>
              <input type="text" id="nombre_seguro"name="nombre_seguro" value="" required class="form-control" />   
            </div> 
            <div class="col-lg-12 col-md-12  mt-2">
              <label>Enfermedad o Limitaciones físicas  que requieran tratamiento actual:</label>
              <textarea class="form-control" rows="1" id="limitaciones"  name="limitaciones" ></textarea>
            </div>                                                                       
          </div>         
        </div>                
      </div>
    </div>
  </div>



<div class="card card-secondary">
    <div class="card-header">
      <h3 class="card-title">Datos Personales de la Madre</h3>
    </div>
            <!-- /.card-header -->
      <div class="card-body">
       
          <div class="row"> 
          <div class="col-lg-4 col-md-12 ">
                              <label for="correo">Nombre Madre:</label>
                              <input type="text" id="nombre_madre"name="nombre_madre" value="" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ]+" autocomplete="off" class="form-control" title="* Solo letras sin espacios."  />   
                      </div> 
                      <div class="col-lg-4 col-md-12 ">
                              <label for="correo">Apellido Madre:</label>
                              <input type="text" id="apellido_madre"name="apellido_madre" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ]+" autocomplete="off" class="form-control" title="* Solo letras sin espacios."  />   
                      </div>
                      <div class="col-lg-4 col-md-12">
                            <label for="password">C.I Madre:</label>
                            <input type="number" id="dni_madre" name="dni_madre" required="" value="" class="form-control" autocomplete="of" />
                      </div>
                     <div class="col-lg-3 col-md-12  mt-2">
                        <label>Dirección:</label>
                        <textarea class="form-control" rows="1" id="direccion_habitacion_m" required="" name="direccion_habitacion_m" autocomplete="of"></textarea>
                      </div>
                      <div class="col-lg-3 col-md-12  mt-2">
                            <label for="password">Telefono Mobil:</label>
                            <input type="text" id="telefono_m" name="telefono_m" value="" required="" class="form-control"autocomplete="of" />
                      </div>
                                           <div class="col-lg-3 col-md-12  mt-2">
                        <label>Dirección Trabajo:</label>
                        <textarea class="form-control" rows="1" id="direccion_trabajo_m" required="" name="direccion_trabajo_m" autocomplete="of"></textarea>
                      </div>
                      <div class="col-lg-3 col-md-12  mt-2">
                            <label for="password">Telefono Trabajo:</label>
                            <input type="text" id="telefono_trabajo_m" required="" name="telefono_trabajo_m" value="" class="form-control" autocomplete="of"/>
                      </div>



                      <div class="col-lg-3 col-md-12  mt-2">
                            <label for="correo">Correo:</label>
                            <input type="email" id="correo_madre"name="correo_madre"  required="" value="" class="form-control" autocomplete="of" />   
                    </div>
                                          
                                                                           
          </div>         
      </div>                
    </div>
  </div>





<div class="card card-secondary">
    <div class="card-header">
      <h3 class="card-title">Datos Personales del Padre</h3>
    </div>
            <!-- /.card-header -->
      <div class="card-body">
       
          <div class="row"> 

              <div class="col-lg-4 col-md-12 ">
                              <label for="correo">Nombre Padre:</label>
                              <input type="text" id="nombre_padre"name="nombre_padre" value="" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ]+" autocomplete="off" class="form-control" title="* Solo letras sin espacios."  />   
                      </div> 
                      <div class="col-lg-4 col-md-12 ">
                              <label for="correo">Apellido Padre:</label>
                              <input type="text" id="apellido_padre"name="apellido_padre" value="" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ]+" autocomplete="off" class="form-control" title="* Solo letras sin espacios."  />   
                      </div>
                      <div class="col-lg-4 col-md-12  ">
                            <label for="password">C.I Padre:</label>
                            <input type="number" id="dni_padre" name="dni_padre" value="" class="form-control" required autocomplete="off" />
                      </div>
                     <div class="col-lg-3 col-md-12  mt-2">
                        <label>Dirección:</label>
                        <textarea class="form-control" rows="1" id="direccion_habitacion_p" name="direccion_habitacion_p" required autocomplete="off"></textarea>
                      </div>
                      <div class="col-lg-3 col-md-12  mt-2">
                            <label for="password">Telefono Móvil:</label>
                            <input type="text" id="telefono_p" name="telefono_p" value="" class="form-control" required autocomplete="off" />
                      </div>
                                           <div class="col-lg-3 col-md-12  mt-2">
                        <label>Dirección Trabajo:</label>
                        <textarea class="form-control" rows="1" id="direccion_trabajo_p" name="direccion_trabajo_p" required autocomplete="off"></textarea>
                      </div>
                      <div class="col-lg-3 col-md-12  mt-2">
                            <label for="password">Telefono Trabajo:</label>
                            <input type="text" id="telefono_trabajo_p" name="telefono_trabajo_p" value="" class="form-control" required autocomplete="off"/>
                      </div>

                      <div class="col-lg-3 col-md-12  mt-2">
                            <label for="correo">Correo:</label>
                            <input type="email" id="correo_padre"name="correo_padre" value="" class="form-control" required autocomplete="off" />   
                    </div>
                      

                                                                           
          </div>         
      </div>                
    </div>
  </div>









<div class="card card-secondary">
    <div class="card-header">
      <h3 class="card-title">Datos del Representante</h3>
    </div>
      <!-- /.card-header -->
      <div class="card-body">
          <div class="row"> 
            <div class="col-lg-4 col-md-12 ">
              <label for="correo">Nombre Representante:</label>
                <input type="text" id="nombre_representante"name="nombre_representante" value="" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ]+" autocomplete="off" class="form-control" title="* Solo letras sin espacios."  />     
            </div> 
            <div class="col-lg-4 col-md-12 ">
              <label for="correo">Apellido Representante:</label>
                <input type="text" id="apellido_epresentante"name="apellido_epresentante" value="" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ]+" autocomplete="off" class="form-control" title="* Solo letras sin espacios."  />     
            </div>
            <div class="col-lg-4 col-md-12  ">
              <label for="password">C.I Representante:</label>
                <input type="number" id="dni_representante" name="dni_representante" value="" class="form-control" required autocomplete="off" />
            </div>  
            <div class="col-lg-4 col-md-12  ">
              <label for="password">Correo:</label>
              <input type="text" id="correo_representante" name="correo_representante" value="" class="form-control" required autocomplete="off" />
            </div>  
                  <button type="submit" style="text-align: center;" class="btn btn-success btn-icon-split">                           
                    <span class="icon text-white-50 aling-center">
                      <i class="fas fa-plus"></i>
                    </span>
                    <span class="text">Guardar</span>
                  </button>                                                       
          </div>         
      </div>                
    </div>
  </div>
  <?php echo form_close();?>
</div>
<script>

  $(document).ready(function() { 
    $('#loginform').submit(function(e) {
        
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>/Data_alumnas/add3',
            data: $(this).serialize(),
            success: function(result)
            {
              
               $('#resultado2').html(result);

           }
       });
     });
});

  $(function () {


  
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
 



    //Date range as a button
 var pais = document.getElementById('codigo');
var telf = document.getElementById('telefono');
 
pais.onchange = function(e) {
 
  //document.getElementById("telefono").focus({preventScroll:false});
  if((this.value).trim() != '') {
    telf.disabled = false;
  } else 
  {
    telf.disabled = true
  }
}

  })





</script>

                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Bienvenido</h1>
                  </div>

<?php echo form_open(null,array('name'=>'form','class'=>'user'));?>
<?php
            if($this->session->flashdata('mensaje')!='')
            {
               ?>
               <div class="alert alert-<?php echo $this->session->flashdata('css')?>"><?php echo $this->session->flashdata('mensaje')?></div>
               <?php 
            }
            ?>
<?php
                //acá visualizamos los mensajes de error
                $errors=validation_errors('<li>','</li>');
                if($errors!="")
                {
                    ?>
                    <div class="alert alert-danger">
                        <ul>
                            <?php echo $errors;?>
                        </ul>
                    </div>
                    <?php
                }
                ?>
<div class="form-group">
                      <input  class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" type="text" name="correo" placeholder="Introduzca la dirección de correo electrónico...">
                    </div>
    
    
                    <div class="form-group">
                      <input type="password" name="pass" class="form-control form-control-user" id="exampleInputPassword" placeholder="Contraseña">
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                    </div>



    <input type="submit" value="Enviar" class="btn btn-primary btn-user btn-block" />

                      <hr>
                    <a href="index.html" class="btn btn-google btn-user btn-block">
                      <i class="fab fa-google fa-fw"></i> Inicia con Google
                    </a>
                    <a href="index.html" class="btn btn-facebook btn-user btn-block">
                      <i class="fab fa-facebook-f fa-fw"></i> Inicia con Facebook
                    </a>

                    <hr>
                  <div class="text-center">
                    <a class="small" href="#">¿Se te olvidó tu contraseña?</a>
                  </div>
                  <div class="text-center">
                    <a class="small" href="<?php echo base_url()?>acceso/registro">¡Crea una cuenta!</a>
                  </div>
<?php echo form_close();?>

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Inicio</a></li>

              <li class="breadcrumb-item"><a  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>usuarios/','#resultado2');">Categorias</a></li>
              <li class="breadcrumb-item active">Editar Categorias</li>
             
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
       <div id="chat">
        </div>
    </div> 
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">                
           
               Editar Categorias
           
        </h6>
                <?php
        if($this->session->flashdata('mensaje')!='')
            {
               ?>

               
               <div class="alert alert-<?php echo $this->session->flashdata('css')?>">
            <a class="close" data-dismiss="alert">×</a>
             <?php echo $this->session->flashdata('mensaje')?>

          </div>
               <?php 
            }
            ?>
    </div>
    <div class="card-body">
        <?php
        //acá visualizamos los mensajes de error
        $errors=validation_errors('<li>','</li>');
         if($errors!="")
            {
                ?>
                <div class="alert alert-danger">
                    <ul>
                        <?php echo $errors;?>
                    </ul>
                </div>
                <?php
            }
            ?>
            <form action="<?php echo base_url();?>categorias/edit" id="loginform" method="post">
        <div class="row">

            <div class="col-lg-4 col-md-12 ">
                
                    <label for="nombre">Nombre Categoria:</label>
                    <input type="text" id="nombre_categoria"name="nombre_categoria" value="<?php echo $dato->nombre_categoria?>" class="form-control" autofocus="true" />
              
                

            </div>




            <input type="hidden" id = "id" name="id" value="<?php echo $id?>" />
            <div class="col-lg-12 col-md-12" style="text-align: center;">
                 <hr>
                    <button type="submit" class="btn btn-success btn-icon-split"> <span class="icon text-white-70">
                    <i class="fas fa-edit"></i>
                            </span>
                            <span class="text">Editar</span></button>
            </div>
                    <?php echo form_close();?> 
        </div>
    </div>
</div>

 <script>
  $(document).ready(function() { 
    $('#loginform').submit(function(e) {
      // alert(765764764);
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>/categorias/edit',
            data: $(this).serialize(),
            success: function(result)
            {
              
               $('#resultado2').html(result);

           }
       });
     });
});

</script>
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Categorias</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Inicio</a></li>
              <li class="breadcrumb-item active">Categorias</li>
             
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->

    </div>          
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">   
        <form action="<?php echo base_url();?>categorias/add" id="loginform" method="post">
        <div class="row "> 
          <div class="col-lg-4 col-md-12 ">
            <input type="text" id="nombre_categoria"name="nombre_categoria" value="" placeholder="Nombre de la Categoria" class="form-control" />   
          </div>
           <div class="col-lg-3 col-md-12 ">            
                                                     
              <select   class="form-control" name="nivel" id="nivel" >
                <option value=''>Selccione Nivel</option>    
                 
                              <option value="1">Baby Dancer: 3 a 5 años</option> 
                               <option value="2">Mini Dancer: 6 a 9 años</option> 
                                <option value="3">Pre- Juvenil: 9 a 12 años</option> 
                                 <option value="4">Juvenil: 12 a 18 años</option> 
                                  <option value="5">Grupo Elite: 18 en adelante</option>            
                        
                            </select>
                        </div> 

           <div class="col-lg-3 col-md-12 ">            
                                                     
              <select   class="form-control" name="id_categoria_pa" id="id_categoria_pa" >
                <option value=''>Categoria Padre</option>    
                  <?php 
                  foreach($datos as $dato)
                  {
                    if( $dato->id_categori_pa == 0)
                    {
                      ?>
                      <option value="<?php echo $dato->id?>"><B>*<?php echo $dato->nombre_categoria?></B></option>            
                      <?php
                        foreach($datos as $dato2)
                        {
                          if( $dato2->id_categori_pa == $dato->id)
                          {

                            ?>
                              <option value="<?php echo $dato2->id?>">&nbsp;&nbsp;&nbsp;-<?php echo $dato2->nombre_categoria?></option>            
                            <?php
                          foreach($datos as $dato3)
                          {
                            if( $dato3->id_categori_pa == $dato2->id)
                            {

                              ?>
                                <option value="<?php echo $dato2->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $dato3->nombre_categoria?></option>            
                              <?php
                            }
                          }
                          }
                         
                        }
                    }
                  } ?> 

                            </select>
                        </div> 

           <div class="col-lg-2 col-md-12 ">
           <button type="submit" class="btn btn-primary btn-block">Agregar</button>
            
          </div>

        </div>             
       </h6>                                 <?php
    if($this->session->flashdata('mensaje')!='')
    {
       ?>
      
          <script> 
       alertas('<?php echo $this->session->flashdata('css')?>','<?php echo $this->session->flashdata('mensaje')?>');
       </script>
          

      
       <?php 
    }
    ?> 
    </div>
   <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Categorias y Niveles</h3>
              </div>
              <!-- ./card-header -->
              <div class="card-body p-0">
                <table class="table table-hover">
                  <tbody>
                    <?php foreach($datos as $dato)
                      {
                        if( $dato->id_categori_pa == 0){
                          ?>
                    <tr data-widget="expandable-table" aria-expanded="false">
                      <td>
                        <i class="expandable-table-caret fas fa-caret-right fa-fw"></i>
                      <strong>  <?php echo $dato->nombre_categoria?> </strong> <small class="float-right"> <div class="btn-group btn-group-sm">
                     
                        <a href="javascript:void(0);" onclick="editar('<?php echo base_url()?>categorias/edit/<?php echo $dato->id?>')" class="btn btn-success"><i class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0);" onclick="eliminar('<?php echo base_url()?>categorias/delete/<?php echo $dato->id?>')" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                      </div></small>
                       </td>
                    </tr>  
                          <tr class="expandable-body d-none">
                            <td>
                              <div class="p-0">
                                <table class="table table-hover">
                                  <tbody>
                       <?php foreach($datos as $dato2)
                      {
                              if( $dato2->id_categori_pa == $dato->id){
                                ?>
                                    <tr data-widget="expandable-table" aria-expanded="false">
                                      <td>
                                        <i class="expandable-table-caret fas fa-caret-right fa-fw"></i>
                                        <?php echo $dato2->nombre_categoria?>   <small class="float-right"> <div class="btn-group btn-group-sm">
                     
                       <a href="javascript:void(0);" onclick="editar('<?php echo base_url()?>categorias/edit/<?php echo $dato2->id?>')" class="btn btn-success"><i class="fas fa-pencil-alt"></i></a>
                   <a href="javascript:void(0);" onclick="eliminar('<?php echo base_url()?>categorias/delete/<?php echo $dato2->id?>')" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                      </div></small>
                                      </td>
                                    </tr>

                                     <tr class="expandable-body d-none">
                            <td>
                              <div class="p-0">
                                <table class="table table-hover text-dark">
                                  <tbody>

                       <?php foreach($datos as $dato3)
                      {
                              if( $dato3->id_categori_pa == $dato2->id){
                                ?>
                              

                                    

                                    <tr data-widget="expandable-table" aria-expanded="false">
                                      <td>
                                        
                                         <?php echo $dato3->nombre_categoria?> aqui <small class="float-right"> <div class="btn-group btn-group-sm">
                     
                     <a href="javascript:void(0);" onclick="editar('<?php echo base_url()?>categorias/edit/<?php echo $dato3->id?>')" class="btn btn-success"><i class="fas fa-pencil-alt"></i></a>
                        <a href="javascript:void(0);" onclick="eliminar('<?php echo base_url()?>categorias/delete/<?php echo $dato3->id?>')" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                      </div></small>
                                      </td>
                                    </tr>
                                    

                    
                          <?php
                    }  } ?> 

                                </tbody>
                                </table>
                              </div>
                            </td>
                          </tr>


                    
                          <?php
                    }  } ?> 

                                </tbody>
                                </table>
                              </div>
                            </td>
                          </tr>

                          <?php
                    }  } ?> 

                    
                 
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

  </div>
</div>
<br>    
 

       
<br>    
 

  
 <script>
  $(document).ready(function() { 
    $('#loginform').submit(function(e) {
     //   alert(765764764);
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>/categorias/add',
            data: $(this).serialize(),
            success: function(result)
            {
              
               $('#resultado2').html(result);

           }
       });
     });
});
  $(function () {  
    //Initialize Select2 Elements
    $('.select2').select2()
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    //Date range as a button
  var pais = document.getElementById('codigo');
  var telf = document.getElementById('telefono');
 
  pais.onchange = function(e) {
 
  //document.getElementById("telefono").focus({preventScroll:false});
  if((this.value).trim() != '') {
    telf.disabled = false;
  } else 
  {
    telf.disabled = true
  }
}

  })
</script>

 


        
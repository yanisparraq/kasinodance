<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Sistema V1.0</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
   <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/toastr/toastr.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
 


  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
   <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/bs-stepper/css/bs-stepper.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/summernote/summernote-bs4.min.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
<div id="loading-screen" style="display: none;background-color: rgba(25,25,25,0.7);
  height: 100%;
  width: 100%;
  position: fixed;
  z-index: 9999;
  margin-top: 0;
  top: 0;
  text-align: center">
  <div class="spinner-border  text-primary" id = "progress" style="width: 3rem; height: 3rem;width: 100px;
  height: 100px;
  position: relative;
  margin-top: -50px;
  margin-left: -50px;
  top: 50%;
  font-size: 30px; " >       
  </div> 
</div>
  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="<?php echo base_url();?>public/dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
  </div>
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url()?>" class="nav-link">Inicio</a>
      </li>
    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->      
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>     
    </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="<?php echo base_url()?>public/dist/img/AdminLTELogo.png" alt="Sistema V1.0" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Kasino Dance</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          
        </div>
        <div class="info">
         <a  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>perfil/','#resultado2');clearInterval(alex);" class="d-block"><?php echo $this->session->userdata('nombre_de_usuario')." ".$this->session->userdata('apellido')?></a>
        </div>
      </div>
<nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          <li class="nav-item">
            <a href="#" class="nav-link">
             <i class="nav-icon fas fa-chalkboard-teacher"></i>
              <p>
                Alumnos
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right">2</span>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                 <a href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>alumnos/','#resultado2');" class="nav-link">
                  <i class="far fa-address-card nav-icon"></i>
                  <p>Adultos</p>
                </a>
              </li>
              <li class="nav-item">
               <a href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>data_alumnas/','#resultado2');" class="nav-link">
                  <i class="fas fa-baby nav-icon"></i>
                  <p>Niños</p>
                </a>
              </li>              
            </ul>
          </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
             <i class="nav-icon fas fa-book-open"></i>
              <p>
                Cursos
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right">2</span> 
             </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>categorias/','#resultado2');" class="nav-link">
              <i class="nav-icon fas  fa-award"></i>
              <p>
                Categorias              
              </p>
            </a>
              </li>
              <li class="nav-item">
               <a  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>niveles/','#resultado2');" class="nav-link">
              <i class="nav-icon fas  fa-tasks"></i>
              <p>
                Niveles
                              
              </p>
            </a>
              </li>
                        <li class="nav-item">
            <a  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>cursos/','#resultado2');" class="nav-link">
              <i class="nav-icon fas  fa-file-alt"></i>
              <p>
                Listado de Cursos
                              
              </p>
            </a>
          </li>
              
            </ul>
          </li>


          <li class="nav-item">
            <a href="#" class="nav-link">
             <i class="nav-icon fas  fa-edit"></i>
              <p>
                Asistencias
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right">2</span>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                 <a href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>asistencias/','#resultado2');" class="nav-link">
                  <i class="nav-icon fas  fa-edit"></i>
                  <p>Registrar Asistencia</p>
                </a>
              </li>
              <li class="nav-item">
               <a href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>asistencias/lista','#resultado2');" class="nav-link">
                  <i class="nav-icon fas  fa-edit"></i>
                  <p>Reporte general Asis.</p>
                </a>
              </li>              
            </ul>
          </li>


          <li class="nav-item">
            <a  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>pagos/','#resultado2');" class="nav-link">
              <i class="nav-icon fas fa-donate"></i>
              <p>
                Pagos                              
              </p>
            </a>
          </li> 

          
        
          <li class="nav-item">
            <a  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>usuarios/','#resultado2');" class="nav-link">
              <i class="nav-icon fas  fa-user-alt"></i>
              <p>
                Usuarios              
              </p>
            </a>
          </li> 
          <li class="nav-item">
            
          </li> 
 
          <li class="nav-item">
            
          </li> 




        
       
          <li class="nav-item">
             <a  href="<?php echo base_url()?>acceso/salir"  class="nav-link">
              <i class="nav-icon fas  fas fa-door-open"></i>
              <p>
                Salir              
              </p>
            </a>
          </li>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>          
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="container-fluid" id="resultado2">
          <!-- Page Heading -->
           <?php echo $content_for_layout;?>    
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Control Sidebar -->
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>public/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url();?>public/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url();?>public/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url();?>public/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>public/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?php echo base_url();?>public/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?php echo base_url();?>public/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>public/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url();?>public/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url();?>public/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url();?>public/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo base_url();?>public/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url();?>public/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>public/dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>public/plugins/bs-stepper/js/bs-stepper.min.js"></script>
<script src="<?php echo base_url();?>public/dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url();?>public/dist/js/pages/dashboard.js"></script>
<script src="<?php echo base_url();?>public/plugins/toastr/toastr.min.js"></script>

<script type="text/javascript" src="<?php echo base_url()?>public/js/funciones.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>public/js/push.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>public/js/ajax.js"></script>








<!-- Bootstrap 4 -->

<!-- SweetAlert2 -->
<script src="<?php echo base_url()?>public/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->

<!-- AdminLTE App -->
<script src="<?php echo base_url()?>public/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->


</body>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });

    //Date and time picker
    $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    })

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    })

  })
  // BS-Stepper Init
  document.addEventListener('DOMContentLoaded', function () {
    window.stepper = new Stepper(document.querySelector('.bs-stepper'))
  })

  // DropzoneJS Demo Code Start
  Dropzone.autoDiscover = false

  // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
  var previewNode = document.querySelector("#template")
  previewNode.id = ""
  var previewTemplate = previewNode.parentNode.innerHTML
  previewNode.parentNode.removeChild(previewNode)

  var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
    url: "/target-url", // Set the url
    thumbnailWidth: 80,
    thumbnailHeight: 80,
    parallelUploads: 20,
    previewTemplate: previewTemplate,
    autoQueue: false, // Make sure the files aren't queued until manually added
    previewsContainer: "#previews", // Define the container to display the previews
    clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
  })

  myDropzone.on("addedfile", function(file) {
    // Hookup the start button
    file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file) }
  })

  // Update the total progress bar
  myDropzone.on("totaluploadprogress", function(progress) {
    document.querySelector("#total-progress .progress-bar").style.width = progress + "%"
  })

  myDropzone.on("sending", function(file) {
    // Show the total progress bar when upload starts
    document.querySelector("#total-progress").style.opacity = "1"
    // And disable the start button
    file.previewElement.querySelector(".start").setAttribute("disabled", "disabled")
  })

  // Hide the total progress bar when nothing's uploading anymore
  myDropzone.on("queuecomplete", function(progress) {
    document.querySelector("#total-progress").style.opacity = "0"
  })

  // Setup the buttons for all transfers
  // The "add files" button doesn't need to be setup because the config
  // `clickable` has already been specified.
  document.querySelector("#actions .start").onclick = function() {
    myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED))
  }
  document.querySelector("#actions .cancel").onclick = function() {
    myDropzone.removeAllFiles(true)
  }
  // DropzoneJS Demo Code End
</script>
</html>


<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Asistencias</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Inicio</a></li>
          <li class="breadcrumb-item active">Asistencias</li>             
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->     
</div>
          
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">
      <form action="<?php echo base_url();?>asistencias/" id="loginform" method="post">
       <div class="row">
        <div class="col-lg-6 col-md-12 ">
                
         
          <input type="password" id="codigo" name="codigo" value="" class="form-control" placeholder="Codigo" autofocus="true" focus = "true" autocomplete="off" />
        </div>

        <button type="submit" style="text-align: center;" class="btn btn-success btn-icon-split">                          
                    <span class="icon text-white-50 aling-center">
                        <i class="fas fa-plus"></i>
                    </span>
                    <span class="text">Registrar Asistencia</span>
        </button>
      <?php echo form_close();?>
         

       </div>
         
     </h6>
 
     



                                     <?php
        if($this->session->flashdata('mensaje')!='')
        {
           ?>
          
              <script> 
           alertas('<?php echo $this->session->flashdata('css')?>','<?php echo $this->session->flashdata('mensaje')?>');
           </script>
              

          
           <?php 
        }
        ?>  
      

    </div>
  </div>
</div>        
<br>   
<br>    
<script>
  $(document).ready(function() { 
    $('#loginform').submit(function(e) {
     //   alert(765764764);
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>/asistencias/',
            data: $(this).serialize(),
            success: function(result)
            {
              
               $('#resultado2').html(result);

           }
       });
     });
});
</script>

  
 
 


        
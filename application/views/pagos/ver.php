    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Usuarios</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Inicio</a></li>
              <li class="breadcrumb-item active">Usuarios</li>
             
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
       <div id="chat">
        </div>
    </div>          
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">                
        <a href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>usuarios/add','#resultado2');" class="btn btn-success btn-sm btn-icon-split">
            <span class="icon text-white-50">
              <i class="fas fa-plus"></i>
            </span>
            <span class="text">Agregar Usuario</span>
        </a></h6>
 
     



                                 <?php
    if($this->session->flashdata('mensaje')!='')
    {
       ?>
      
          <script> 
       alertas('<?php echo $this->session->flashdata('css')?>','<?php echo $this->session->flashdata('mensaje')?>');
       </script>
          

      
       <?php 
    }
    ?>  
  

    </div>
    <div class="card-body">
      <div class="table-responsive">


     <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Usuario</th>
              <th>Correo</th>
              <th>Tipo de Usuario</th>
              <th>Acción</th>           
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Usuario</th>
              <th>Correo</th>
                <th>Tipo de Usuario</th>
                <th>Acción</th>
            </tr>
          </tfoot>
          <tbody>
<?php
                foreach($datos as $dato)
                {                  
                    ?>
                    <tr>
                        <td><?php// echo $dato->nombre_de_usuario?></td>
                        <td><?php// echo $dato->correo?></td>
                        <td><?php //echo $dato->nombre_rol?></td>
                        <td  class="text-center" >
                            <div class="btn-group btn-group-sm">
                        <a href="javascript:void(0);" onclick="editar('<?php echo base_url()?>asistencias/ver/<?php echo $dato->id?>')" class="btn btn-info"><i class="fas fa-eye"></i></a>
                          <a href="javascript:void(0);" onclick="editar('<?php echo base_url()?>usuarios/edit/<?php echo $dato->id?>')" class="btn btn-success"><i class="fas fa-pencil-alt"></i></a>
                          <a href="javascript:void(0);" onclick="eliminar('<?php echo base_url()?>usuarios/delete/<?php echo $dato->id?>')" class="btn btn-danger"><i class="fas fa-trash"></i></a> 
                      </div>
                        </td>
                    </tr>
                    <?php
                }
            ?>        
            <tr>       
          </tbody>
        </table>         
      </div>
    </div>
  </div>
</div>
        <?php
 echo $this->pagination->create_links()?>
<br>    
 

       
<br>    
 

  
 
 


        
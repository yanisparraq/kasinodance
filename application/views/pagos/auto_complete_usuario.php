<style>
.suggestionsBox{
  cursor: pointer;
    list-style: none;
    padding: 5px 3px 5px 13px;
    position: relative;
    background: #4b4b4b;
    border: 1px solid #444;
    width: 268px;
    height:auto;
    color: #f8f8f8;
    transition: all .1s ease-in-out;
}

.suggestionList{
    margin: 0px;
    padding: 0px;
}

.suggestionList li{
  cursor: pointer;
    list-style: none;
    padding: 5px 3px 5px 13px;
    position: relative;
    background: #ffffff;
    border: 1px solid #ffffff;
    width: 268px;
    height:auto;
    color: #ffffff;
    transition: all .1s ease-in-out;
}

.suggestionsBox li:hover{
    background: rgba(255,255,255,0.7);
    color:#444;
    padding: 5px 3px 5px 13px;
    position: relative;
    transform: scale(1.1);
}
</style>
<?php
  $i = 0;
    foreach($datos as $dato)
    {
        $i++;
        ?>
        <li onClick="fill_info('<?php echo $dato->id?>','<?php echo $dato->nombre1.' '.$dato->apellido?>');"><?php echo $dato->dni.' - '.$dato->nombre1.' '.$dato->apellido?></li>
      
        <?php
    }
    if ($i == 0){
           ?>
        <li>Sin Resultados</li>
      
        <?php
    }

?>

  <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pagos</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Inicio</a></li>
               <li class="breadcrumb-item"><a href="<?php echo base_url().'cursos'?>">Cursos</a></li>
              <li class="breadcrumb-item active">Pagos</li>
             
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->

    </div>          
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">
        <form action="<?php echo base_url();?>pagos/add/" id="loginform" method="post">
          <div class="row">
            <div class="col-lg-4 col-md-6 ">
                         
                <input type="text" id="nombre" name="nombre" value="" class="form-control" onKeyUp="lookup2(this.value,'<?php echo base_url()?>pagos','suggestions',1);"   autocomplete="off" placeholder="Nombre del Estudiante" />
                <div class="suggestionsBox" id="suggestions" style="display:none;"> <!-- div para imprimir resultados -->
                    <div class="suggestionList" id="autoSuggestionsList">   
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 ">
            <input type="hidden" id = "curso" name="curso" value="" />
              <div class="lista" id="lista">
                
                                            
                                             
                <select   class="form-control" name="rol" id="rol" >
                <option value="">Agregue Estudiante</option>
                 </div> 
                              
                                       
                </select>
              </div>

            </div>    
            <div class="col-lg-2 col-md-6 ">
                         
                <input type="number" id="monto" name="monto" value="" class="form-control" placeholder="$"   autocomplete="off" />
               <!-- div para imprimir resultados -->
                   
            </div>                         
            <div class="col-lg-2 col-md-12 ">
               <input type="hidden" id = "id_usuario" name="id_usuario" value="" />
             <button type="submit" class="btn btn-primary btn-block">Facturar</button>
              
            </div>

          </div>
        </form>
      </h6>
 
     



                                 <?php
    if($this->session->flashdata('mensaje')!='')
    {
       ?>
      
          <script> 
       alertas('<?php echo $this->session->flashdata('css')?>','<?php echo $this->session->flashdata('mensaje')?>');
       </script>
          

      
       <?php 
    }
    ?>  
  

    </div>
    <div class="card-body">
      <div class="table-responsive">


     <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Nombre</th>
 
              
           
              <th>Cedula</th>
             <th>Facha de Pago</th>
              <th>Curso</th>
              <th>Monto</th>  
              <th>Acción</th>         
            </tr>
          </thead>
          <tfoot>
            <tr>
               <th>Nombre </th>
 
              
           
              <th>Cedula</th>
              <th>Facha de Pago</th>
              <th>Curso</th>
              <th>Monto</th> 
                <th>Acción</th>   
            </tr>
          </tfoot>
          <tbody>
<?php
                foreach($datos as $dato)
                {                  
                    ?>
                    <tr>
                        <td><?php  echo $dato->nombre_de_usuario?></td>
                        <td><?php  echo $dato->dni?></td>
                        <td><?php  echo fecha($dato->fecha)?></td>
                        <td><?php  echo $dato->nombre_categoria?></td>
                        <td class="text-center"><?php  echo $dato->monto?>$</td>
                        <td  class="text-center" >
                          <div class="btn-group btn-group-sm">
                              <a href="javascript:void(0);" onclick="editar('<?php echo base_url()?>pagos/ver/<?php echo $dato->id?>')" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                        <a href="javascript:void(0);" onclick="editar('<?php echo base_url()?>pagos/ver/<?php echo $dato->id?>')" class="btn btn-info"><i class="fas fa-eye"></i></a>

                          <a href="javascript:void(0);" onclick="editar('<?php echo base_url()?>pagos/edit/<?php echo $dato->id?>')" class="btn btn-success"><i class="fas fa-pencil-alt"></i></a>
                          <a href="javascript:void(0);" onclick="eliminar('<?php echo base_url()?>pagos/delete/<?php echo $dato->id?>')" class="btn btn-danger"><i class="fas fa-trash"></i></a> 
                          </div>
                        </td>
                    </tr>
                    <?php
                }
            ?>        
            <tr>       
          </tbody>
        </table>         
      </div>
    </div>
  </div>
</div>        
<br>   
<br>    
 

  <script>

      $(document).ready(function() { 
    $('#loginform').submit(function(e) {
      // alert(765764764);
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>/pagos/add',
            data: $(this).serialize(),
            success: function(result)
            {              
               $('#resultado2').html(result);
           }
       });
     });
});
 
 


        
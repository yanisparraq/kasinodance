<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Casino | Dance</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>public/dist/css/adminlte.min.css">
</head>
<body>
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-12">
        <h2 class="page-header">
          <i class="fas fa-globe"></i> Kasino Dance
          <small class="float-right"><?php echo  fecha(date("Y-m-d")); ?>  </small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        <address>
                  <strong>Nombres y Apellidos:</strong><br>
                    <?php echo $datos->nombre1." ".$datos->nombre2." ".$datos->apellido." ".$datos->apellido2; ?><br>
                   
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
      <address>
          <strong>Direcciones:</strong><br>
          Habitación: <?php echo $datos->direccion_habitacion; ?><br>
         
          Trabjo: <?php echo $datos->direccion_habitacion; ?><br>
          <strong>Email:</strong><br> <?php echo $datos->correo; ?>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        <b>Telf. Móvi:</b>
        <?php echo $datos->telefono_movil; ?><br>
         <b>Telf. Fijo:</b>  <?php echo $datos->telefono_fijo; ?><br>
        <b>Facebook:</b> <?php echo $datos->facebook; ?><br>
        <b>Instagram:</b>  <?php echo $datos->instagran; ?><br>
        <b>TikTok:</b> <?php echo $datos->tiltok; ?>
      </div>
      <!-- /.col -->
<!-- /.col -->
    </div>
    <!-- /.row -->
    <!-- Table row -->

    <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                <th>#</th>
                      <th>Cursos</th>
                      <th >Nivel</th>
                      <th>Periodos</th>
                      <th>status</th>

                    </tr>
                    </thead>
                    <tbody>
                   

<?php
$j =1;
                foreach($cursos as $dato)
                {
                  
                    ?>
                    <tr>
                       <td><?php echo $j; ?></td>
                       <td><?php echo  $dato->nombre_categoria; ?></td>
                       <td><?php echo  $dato->nombre_nivel; ?></td>

                     
                      <td><?php echo  $dato->periodo; ?></td>
                     
                      <td>Activo</td>
                    </tr>
                    <?php
                    $j++;
                }
            ?>
                    
                 
                   
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
  </section>
  <!-- /.content -->
</div><!-- ./wrapper -->
<!-- Page specific script -->
<br><br>
<div class="text-center">
  <img  align="center" src="<?php echo base_url();?>public/qr/123123.png" height="100" width="100">
</div>
<script>
  window.addEventListener("load", window.print());
</script>
</body>
</html>

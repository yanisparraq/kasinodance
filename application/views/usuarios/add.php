    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Crear Usuarios</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                        
              <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Inicio</a></li>


              <li class="breadcrumb-item"><a  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>usuarios/','#resultado2');">Usuarios</a></li>
              <li class="breadcrumb-item active">Crear Usuarios</li>
             
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
       <div id="chat">
        </div>
    </div> 
<div class="card shadow mb-4">
   
                <?php
        if($this->session->flashdata('mensaje')!='')
            {
               ?>
               <div class="alert alert-<?php echo $this->session->flashdata('css')?>"><?php echo $this->session->flashdata('mensaje')?></div>
               <?php 
            }
            ?>
    
            <div class="card-body">
                <!-- Nested Row within Card Body -->
                         <?php
                    //acá visualizamos los mensajes de error
                        $errors=validation_errors('<li>','</li>');
                        if($errors!="")
                        {
                            ?>
                            <div class="alert alert-danger">
                                <ul>
                                    <?php echo $errors;?>
                                </ul>
                            </div>
                            <?php
                        } ?> 
                <div class="row "> 
                <div class="col-lg-4 col-md-12 ">
                            <label for="correo">Nombre:</label>
                            <input type="text" id="nombre_de_usuario"name="nombre_de_usuario" value="<?php echo set_value_input(array(),'nombre_de_usuario','nombre_de_usuario')?>" class="form-control" />   
                    </div> 
                    <div class="col-lg-4 col-md-12 ">
                            <label for="correo">Apellido:</label>
                            <input type="text" id="apellido"name="apellido" value="<?php echo set_value_input(array(),'apellido','apellido')?>" class="form-control" />   
                    </div> 
                    <div class="col-lg-4 col-md-12 ">
                            <label for="password">DNI:</label>
                            <input type="text" id="dni" name="dni" value="<?php echo set_value_input(array(),'dni','dni')?>" class="form-control" />
                    </div>
                    <div class="col-lg-6 col-md-12 ">
                            <label for="correo">Correo:</label>
                            <input type="email" id="correo"name="correo" value="<?php echo set_value_input(array(),'correo','correo')?>" class="form-control" />   
                    </div>
                    <div class="col-lg-6 col-md-12 ">
                            <label for="password">Contraseña:</label>
                            <input type="text" id="password" name="password" value="<?php echo set_value_input(array(),'password','password')?>" class="form-control" />
                    </div>

                    <div class="col-lg-4 col-md-12 ">
                                              
                           
                            <label for="password2">Repetir Contraseña:</label>
                            <input type="text" id="password2"name="password2" value="<?php echo set_value_input(array(),'password2','password2')?>" class="form-control" />                        
                        
                    </div>
 




                        <div class="col-lg-4 col-md-12 ">

                                              
                            <label for="rol">Rol:</label>                               
                            <select   class="form-control" name="rol" id="rol" >
                            <option value='<?php   if(!set_value_input(array(),'rol','rol')){}
                            else{ echo set_value_input(array(),'rol','rol');}?>'><?php   if(!set_value_input(array(),'rol','rol')){ echo "Seleccione";
                                }
                                    else{ foreach($rol as $roles){
                                    if($roles->id == set_value_input(array(),'rol','rol')){

                                   
                                }

                            }
                                   
                                 }?></option>
                                          
                                 <?php foreach($rol as $roles)
                                    {
                                         if($roles->id <> 1 && $roles->id <> 2){
                                        ?>
                                          <option value="<?php echo $roles->id?>"><?php echo $roles->nombre_rol?></option>
                                          
                                        <?php
                                    }
                                    } ?>                                       
                            </select>
                        </div> 


                   <div class="col-lg-4 col-md-12 ">

                                              
                            <label for="rol">Tipo de Usuario:</label>                               
                            <select   class="form-control" name="tipo_usuario" id="tipo_usuario" >
                            <option value='<?php   if(!set_value_input(array(),'tipo_usuario','rol')){}
                            else{ echo set_value_input(array(),'tipo_usuario','tipo_usuario');}?>'><?php   if(!set_value_input(array(),'tipo_usuario','tipo_usuario')){ echo "Seleccione";
                                }
                                    else{ foreach($tipo_usuario as $tipo){
                                    if($tipo->id == set_value_input(array(),'tipo_usuario','tipo_usuario')){

                                   
                                }

                            }
                                    
                                 }?></option>
                                          
                                 <?php foreach($tipo_usuario as $tipo)
                                    {
                                         if($tipo->id <> set_value_input(array(),'tipo_usuario','tipo_usuario')){
                                        ?>
                                          <option value='<?php echo $tipo->id?>'><?php echo $tipo->nombre_tipo_usuario?></option>
                                          
                                        <?php
                                    }
                                    } ?>                                       
                            </select>
                        </div> 


                    </div>    
                     
                    </div>                         
                    <div class="col-lg-12 col-md-12 " style="text-align: center;">                                    
<hr>
 <button style="text-align: center;" href="javascript:void(0);" onclick="addUser('<?php echo base_url()?>/usuarios/add','#resultado2');" class="btn btn-success btn-icon-split"> 
                          
                                <span class="icon text-white-50 aling-center">
                                    <i class="fas fa-plus"></i>
                                </span>
                                <span class="text">Guardar</span>
                            </button>
                            <?php echo form_close();?> 
                    </div>                
                </div>
            </div>
        </div>
    </div>
</div>  

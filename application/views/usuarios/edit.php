    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Inicio</a></li>

              <li class="breadcrumb-item"><a  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>usuarios/','#resultado2');">Usuarios</a></li>
              <li class="breadcrumb-item active">Editar Usuarios</li>
             
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
       <div id="chat">
        </div>
    </div> 
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">                
           
               Editar Usuario
           
        </h6>
                <?php
        if($this->session->flashdata('mensaje')!='')
            {
               ?>

               
               <div class="alert alert-<?php echo $this->session->flashdata('css')?>">
            <a class="close" data-dismiss="alert">×</a>
             <?php echo $this->session->flashdata('mensaje')?>

          </div>
               <?php 
            }
            ?>
    </div>
    <div class="card-body">
        <?php
        //acá visualizamos los mensajes de error
        $errors=validation_errors('<li>','</li>');
         if($errors!="")
            {
                ?>
                <div class="alert alert-danger">
                    <ul>
                        <?php echo $errors;?>
                    </ul>
                </div>
                <?php
            }
            ?>
              <form action="<?php echo base_url();?>usuarios/edit/<?php  echo $id?>" id="loginform" method="post">
        <div class="row">
            <div class="col-lg-4 col-md-12 ">
                
                    <label for="nombre">Nombre:</label>
                    <input type="text" id="nombre"name="nombre" value="<?php echo $datos->nombre1?>" class="form-control" autofocus="true" />
              
                

            </div>

            <div class="col-lg-4 col-md-12 ">
                       
                <label for="correo">Apellido:</label>
                <input type="text" id="apellido"name="apellido" value="<?php echo $datos->apellido?>" class="form-control" autofocus="true" /> 
            </div> 
            <div class="col-lg-4 col-md-12 ">
                       
                <label for="correo">DNI:</label>
                <input type="text" id="dni"name="dni" value="<?php echo $datos->dni?>" class="form-control" autofocus="true" /> 
            </div> 
                        <div class="col-lg-6 col-md-12 ">
                       
                <label for="correo">Correo:</label>
                <input type="email" id="correo"name="correo" value="<?php echo $datos->correo?>" class="form-control" autofocus="true" /> 
            </div>            
            <div class="col-lg-6 col-md-12 ">
                  <label for="password">Nueva Contraseña:</label>
                    <input type="text" name="password" id="password" name="password" value="" class="form-control" />
            </div>
            <div class="col-lg-4 col-md-12 ">
                <label for="password2">Repetir Contraseña:</label>
                <input type="text" id="password2"name="password2" value="" class="form-control" />
            </div>

                <div class="col-lg-4 col-md-12 ">

                                              
                            <label for="rol">Rol:</label>                               
                            <select   class="form-control" name="rol" id="rol" >
                            <option value="">Seleccione</option>
                                          
                                 <?php foreach($rol as $roles)
                                    {
                                         if($roles->id <> 1 && $roles->id <> 2){
                                        ?>
                                          <option value="<?php echo $roles->id?>"><?php echo $roles->nombre_rol?></option>
                                          
                                        <?php
                                    }
                                    } ?>                                       
                            </select>
                        </div> 





<div class="col-lg-4 col-md-12 ">

                                              
                            <label for="rol">Tipo de Usuario:</label>                               
                            <select   class="form-control" name="tipo_usuario" id="tipo_usuario" >
                            <option value=''>Selccione</option>
                                          
                                 <?php foreach($tipo_usuario as $tipo)
                                    {
                                         if($tipo->id <> set_value_input(array(),'tipo_usuario','tipo_usuario')){
                                        ?>
                                          <option value='<?php echo $tipo->id?>'><?php echo $tipo->nombre_tipo_usuario?></option>
                                          
                                        <?php
                                    }
                                    } ?>                                       
                            </select>
                        </div> 




            <input type="hidden" id = "id" name="id" value="<?php echo $id?>" />
            <div class="col-lg-12 col-md-12" style="text-align: center;">
                 <hr>
                    <button type="submit" class="btn btn-success btn-icon-split"> <span class="icon text-white-70">
                    <i class="fas fa-edit"></i>
                            </span>
                            <span class="text">Editar</span></button>
            </div>
                    <?php echo form_close();?> 
        </div>
    </div>
</div>

<script>

      $(document).ready(function() { 
    $('#loginform').submit(function(e) {
       //alert(765764764);
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>/usuarios/edit/<?php  echo $id?>',
            data: $(this).serialize(),
            success: function(result)
            {              
               $('#resultado2').html(result);
           }
       });
     });
});
</script>
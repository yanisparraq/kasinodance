<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_alumnas extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();        
        $this->layout->setLayout("ajax");
    }
    
 
    public function index()
    {
        
        if($this->uri->segment(3))
        {
            $pagina=$this->uri->segment(3);
        }else
        {
            $pagina=0;
        }
        $porpagina=5;
        //zona de car;ga de los datos
        $datos=$this->datos_alumnas_model->getTodosPaginacion($pagina,$porpagina,"limit");
        $cuantos=$this->datos_alumnas_model->getTodosPaginacion($pagina,$porpagina,"cuantos");           //zona de configuración de la librería pagination
        $config['base_url']=base_url()."Data_alumnas/index";
        $config['total_rows']=$cuantos;
        $config['per_page']=$porpagina;
        $config['uri_segment']='3';
        $config['num_links']='4';
        $config['first_link']='Primero';
        $config['next_link']='Siguiente';
        $config['prev_link']='Anterior';
        $config['last_link']='Última';
        
        $config['full_tag_open']='<ul class="pagination text-center">';
        
       
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';  
        
        
        $config['full_tag_close']='</ul>';
        $this->pagination->initialize($config);
        $this->layout->view("index",compact('datos','cuantos','pagina'));
    
    }


    public function add()
    {
        //redirect(base_url()."productos");
       
        $this->layout->view("add");
    } 

     public function add3()
    {
        //redirect(base_url()."productos");       
               //  redirect(base_url()."productos");
            $data_usuario=array
            (
                'nombre_de_usuario'=>$this->input->post('nombre_representante',true),
                //$this->input->post('nombre1',true).' '.$this->input->post('apellido',true),
                
                'correo'=>$this->input->post('correo_representante',true),//$this->input->post('correo',true),                
              //  'fecha_registro'=>78789,
                'clave_usuario'=>$this->input->post('dni_representante',true),//sha1($this->input->post('dni',true)),
                'tipo_usuario_id'=>2,
                'id_rol'=>2,

            );

               $insertar1=$this->usuarios_model->insertar($data_usuario);

               

            $data_datos_personales=array
            (
                'nombre1'=>$this->input->post('nombre1',true),
                'nombre2'=>$this->input->post('nombre2',true),
                'apellido'=>$this->input->post('apellido',true),
                'apellido2'=>$this->input->post('apellido2',true),
                'dni'=>$this->input->post('dni',true),
               //'correo'=>$this->input->post('correo',true),
                'id_usuario'=> $insertar1,              
 
                 'fecha_nacimiento'=>$this->input->post('fecha_nacimiento',true),
            );
                $insertar2=$this->datos_personales_model->insertar($data_datos_personales);

            $data_datos_alumnas=array
            (
                'lugar_nacimiento'=>$this->input->post('lugar_nacimiento',true),
                'malla'=>$this->input->post('malla',true),
                'zapatilla'=>$this->input->post('zapatilla',true),
                'tacon'=>$this->input->post('tacon',true),
                'peso'=>$this->input->post('peso',true),
               //'correo'=>$this->input->post('correo',true),
                 'sangre'=>$this->input->post('sangre',true),
                'anio_escolar'=>$this->input->post('anio_escolar',true),
                'colegio'=>$this->input->post('colegio',true),
                'seguro'=>$this->input->post('seguro',true),
                'nombre_seguro'=>$this->input->post('nombre_seguro',true),
                 'limitaciones'=>$this->input->post('limitaciones',true),
                    'id_datos_personales'=> $insertar2, 
                     'id_usuario'=> $insertar1, 
              
            );
                $insertar3=$this->datos_alumnas_model->insertar($data_datos_alumnas);

                     $this->session->set_flashdata('css','success');
                $this->session->set_flashdata('mensaje','El registro se ha creado exitosamente');
               
                
            $data_padres=array
            (


                'nombres_padres'=>$this->input->post('nombre_madre',true),
                'apellidos_padres'=>$this->input->post('apellido_madre',true),
                'dni_padres'=>$this->input->post('dni_madre',true),
                'direccion_habitacion_padres'=>$this->input->post('direccion_habitacion_m',true),
                'telefono_padres'=>$this->input->post('telefono_m',true),
               //'correo'=>$this->input->post('correo',true),
                 'direccion_trabajo_padres'=>$this->input->post('direccion_trabajo_m',true),
                'telefono_trabajo_padres'=>$this->input->post('telefono_trabajo_m',true),
                'correo_padres'=>$this->input->post('correo_madre',true),

                'id_datos_personales'=> $insertar2, 
              
            );



                $insertar4=$this->padres_model->insertar($data_padres);
                if (!empty($this->input->post('nombre_padre',true))) 
                {

                    $data_padres2=array
                    (

                        'nombres_padres'=>$this->input->post('nombre_padre',true),
                        'apellidos_padres'=>$this->input->post('apellido_padre',true),
                        'dni_padres'=>$this->input->post('dni_padre',true),
                        'direccion_habitacion_padres'=>$this->input->post('direccion_habitacion_p',true),
                        'telefono_padres'=>$this->input->post('telefono_p',true),
                       //'correo'=>$this->input->post('correo',true),
                         'direccion_trabajo_padres'=>$this->input->post('direccion_trabajo_p',true),
                        'telefono_trabajo_padres'=>$this->input->post('telefono_trabajo_p',true),
                        'correo_padres'=>$this->input->post('correo_padre',true),

                        'id_datos_personales'=> $insertar2, 
                      
                    );
                       $insertar4=$this->padres_model->insertar($data_padres2);
                            
                }


                    $data_representante=array
                    (

                        'nombre_representante'=>$this->input->post('nombre_representante',true),
                        'apellido_representante'=>$this->input->post('apellido_epresentante',true),
                        'dni_representante'=>$this->input->post('dni_representante',true),
                        'correo_representante'=>$this->input->post('correo_representante',true),
                        'id_datos_personales'=> $insertar2,
                    );
                  $insertar5=$this->representantes_model->insertar($data_representante);

                     $this->session->set_flashdata('css','success');
                $this->session->set_flashdata('mensaje','El registro se ha creado exitosamente');
            

                if(!empty($insertar5))
                {
        
                    if($this->uri->segment(3))
                    {
                        $pagina=$this->uri->segment(3);
                    }else
                    {
                        $pagina=0;
                    }
                    $porpagina=5;
                    //zona de car;ga de los datos
                    $datos=$this->datos_alumnas_model->getTodosPaginacion($pagina,$porpagina,"limit");
                    $cuantos=$this->datos_alumnas_model->getTodosPaginacion($pagina,$porpagina,"cuantos");           //zona de configuración de la librería pagination
                    $config['base_url']=base_url()."Usuarios/index";
                    $config['total_rows']=$cuantos;
                    $config['per_page']=$porpagina;
                    $config['uri_segment']='3';
                    $config['num_links']='4';
                    $config['first_link']='Primero';
                    $config['next_link']='Siguiente';
                    $config['prev_link']='Anterior';
                    $config['last_link']='Última';
                    
                    $config['full_tag_open']='<ul class="pagination text-center">';
                    
                   
                    $config['first_tag_open'] = '<li class="page-item">';
                    $config['first_tag_close'] = '</li>';
                    $config['last_tag_open'] = '<li class="page-item">';
                    $config['last_tag_close'] = '</li>';
                    $config['next_tag_open'] = '<li class="page-item">';
                    $config['next_tag_close'] = '</li>';
                    $config['prev_tag_open'] = '<li class="page-item">';
                    $config['prev_tag_close'] = '</li>';
                    $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link"><b>';
                    $config['cur_tag_close'] = '</b></a></li>';
                    $config['num_tag_open'] = '<li class="page-item">';
                    $config['num_tag_close'] = '</li>';  
                    
                    
                    $config['full_tag_close']='</ul>';
                    $this->pagination->initialize($config);
                    $this->layout->view("index",compact('datos','cuantos','pagina'));
    
                }


    } 



    public function edit($id=null,$pagina=null)
    {        
      
    
          $datos=$this->datos_alumnas_model->getTodosPorId2($id);
        if($this->input->post())
        {
          

            $datos_personales=array
            (
                'nombre1'=>$this->input->post('nombre1',true),
                'nombre2'=>$this->input->post('nombre2',true),
                'apellido'=>$this->input->post('apellido',true),
                'apellido2'=>$this->input->post('apellido2',true),
                'dni'=>$this->input->post('dni',true),
               //'correo'=>$this->input->post('correo',true),
                           
                'telefono_movil'=>$this->input->post('telefono_movil',true),
                'telefono_fijo'=>$this->input->post('telefono_fijo',true),            
                'direccion_habitacion'=>$this->input->post('direccion_habitacion',true),
                'direccion_trabajo'=>$this->input->post('direccion_trabajo',true),
                'instagran'=>$this->input->post('instagran',true),
                'facebook'=>$this->input->post('facebook',true),
                'tiltok'=>$this->input->post('tiltok',true),
               /*  'fecha_registro'=>78789,
                'clave_usuario'=>sha1($this->input->post('password',true)),*/
            );
            $datos_usuarios=array
            (
                'nombre_de_usuario'=>$this->input->post('nombre1',true).' '.$this->input->post('apellido',true),                  
                'correo'=>$this->input->post('correo',true),
            );               
                $this->datos_personales_model->update($datos_personales,$datos->id_datos_personales);
                $this->usuarios_model->update($datos_usuarios,$datos->id_usuario);
                 $this->session->set_flashdata('css','success');
                $this->session->set_flashdata('mensaje','El registro se ha modificado exitosamente');
                redirect(base_url()."data_alumnas");
        }                   
        $this->layout->view('edit',compact('datos','id','pagina'));
    }
  




        public function delete($id=null)
    {
        if(!$id){show_404();}
        
         $datos=$this->datos_alumnas_model->getTodosPorId($id);
        if($datos->id_usuario>0)
         {
             $this->usuarios_model->delete($datos->id_usuario);
         }
        if($datos->id_datos_personales>0){
             $this->datos_personales_model->delete($datos->id_datos_personales);
         }
          $this->datos_alumnas_model->delete($id);
       
        $this->session->set_flashdata('css','success');
        $this->session->set_flashdata('mensaje','El registro se ha eliminado exitosamente');
         if($this->uri->segment(3))
                    {
                        $pagina=$this->uri->segment(3);
                    }else
                    {
                        $pagina=0;
                    }
                    $porpagina=5;
                    //zona de car;ga de los datos
                    $datos=$this->datos_alumnas_model->getTodosPaginacion($pagina,$porpagina,"limit");
                    $cuantos=$this->datos_alumnas_model->getTodosPaginacion($pagina,$porpagina,"cuantos");           //zona de configuración de la librería pagination
                    $config['base_url']=base_url()."Usuarios/index";
                    $config['total_rows']=$cuantos;
                    $config['per_page']=$porpagina;
                    $config['uri_segment']='3';
                    $config['num_links']='4';
                    $config['first_link']='Primero';
                    $config['next_link']='Siguiente';
                    $config['prev_link']='Anterior';
                    $config['last_link']='Última';
                    
                    $config['full_tag_open']='<ul class="pagination text-center">';
                    
                   
                    $config['first_tag_open'] = '<li class="page-item">';
                    $config['first_tag_close'] = '</li>';
                    $config['last_tag_open'] = '<li class="page-item">';
                    $config['last_tag_close'] = '</li>';
                    $config['next_tag_open'] = '<li class="page-item">';
                    $config['next_tag_close'] = '</li>';
                    $config['prev_tag_open'] = '<li class="page-item">';
                    $config['prev_tag_close'] = '</li>';
                    $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link"><b>';
                    $config['cur_tag_close'] = '</b></a></li>';
                    $config['num_tag_open'] = '<li class="page-item">';
                    $config['num_tag_close'] = '</li>';  
                    
                    
                    $config['full_tag_close']='</ul>';
                    $this->pagination->initialize($config);
                    $this->layout->view("index",compact('datos','cuantos','pagina'));redirect(base_url()."data_alumnas");
    }

    public function ver($id=null)
    {

            $datos=$this->alumnos_model->getverPorId($id);
          $cursos=$this->matriculaciones_model->getTodosId($datos->id_usuario);

        $url =base_url().'public/qr/'.$datos->dni.'.png';
          $url2 =base_url().'inspecciones/doc2/'.$datos->dni;
     if (!file_exists("public/qr/".$datos->dni.".png")){
        

       
        qr($url,$url2,$datos->dni);
        // qr($url,$url2,$id); 
    // pdf($datos); 
      }
         $this->layout->view('ver',compact('id','datos','cursos'));


    }
    public function imprimir($id=null)
    {
         $datos=$this->alumnos_model->getverPorId($id);

          $cursos=$this->matriculaciones_model->getTodosId($datos->id_usuario);
         $this->layout->view('imprimir',compact('id','datos','cursos'));

    }
}





<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorias extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();        
        $this->layout->setLayout("ajax");
    }   
 
    public function index()
    {        

        //zona de car;ga de los datos
                $datos=$this->categorias_model->getTodos();
                 // $datos2=$this->categorias_model->getTodos();
                    // $rol=$this->rol_model->getTodos();
        
                $this->layout->view("index",compact('datos'));
    
    }
  
    public function add()
    {
        //redirect(base_url()."productos");
            //   redirect(base_url()."productos");
            $data=array
            (
                'nombre_categoria'=>$this->input->post('nombre_categoria',true),
                
                'id_categori_pa'=>$this->input->post('id_categoria_pa',true),           
            );
                $insertar1=$this->categorias_model->insertar($data);
                        //zona de car;ga de los datos
                $datos=$this->categorias_model->getTodos();
                  $datos2=$this->categorias_model->getTodos();
                    // $rol=$this->rol_model->getTodos();        
                $this->layout->view("index",compact('datos','datos2'));
    }
    public function edit($id=null,$pagina=null)
    {
        // redirect(base_url()."usuarios");
       
        // redirect(base_url()."usuarios");
      
         
        if($this->input->post())
        {          
                  $data=array
                    (
                    'nombre_categoria'=>$this->input->post('nombre_categoria',true), 
                                                      
                    );
                $dato=$this->categorias_model->update($data,$this->input->post('id',true));
                              $this->session->set_flashdata('css','success');
                $this->session->set_flashdata('mensaje','El registro se ha modificado exitosamente');
                redirect(base_url()."categorias");
        }
                 

                $dato=$this->categorias_model->getTodosPorId($id);             
                $datos=$this->categorias_model->getTodos();
                 
                // $rol=$this->rol_model->getTodos();        
                $this->layout->view("edit",compact('datos','dato','id'));//  $this->layout->view('add',compact('datos','roles','tipo_usuario','id','pagina'));
    }
    public function delete($id=null)
    {
        if(!$id){show_404();}        

        $this->categorias_model->delete($id);
        $this->session->set_flashdata('css','success');
        $this->session->set_flashdata('mensaje','El registro se ha eliminado exitosamente');
        redirect(base_url()."categorias");
    }
   
  
}





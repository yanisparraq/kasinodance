<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();        
        if(!$this->session->userdata('id'))
        {
            // $this->layout->setLayout("frontend");
            redirect(base_url()."acceso/salir");
           // redirect(base_url()."acceso/login");
        }
        if($this->session->userdata('tipo_usuario_id')==1)
        {
            $this->layout->setLayout("frontend");
        }else
        { //redirect(base_url()."acceso/salir");
            $this->layout->setLayout("frontend");
        }
    }    
	//localhost/home/nosotros    
	public function index()
	{
      //$this->layout->setLayout("ajax");
        if($this->session->userdata('tipo_usuario_id') == 1 ||  $this->session->userdata('tipo_usuario_id') == 3)
        {
		      
              //print_r($datos);exit;
              $this->layout->view("index");
        }else{
            redirect(base_url()."acceso/salir");
        }
	}
    public function concursante()
    {
      $this->layout->setLayout("ajax");
          $this->layout->view("contacto");
      //$this->layout->view("concursante");
    } 
    public function concurso()
    {
        if($this->session->userdata('tipo_usuario_id')==1 ||  $this->session->userdata('tipo_usuario_id')==3)
        {
              $this->layout->view("index");
        }else{
            redirect(base_url()."acceso/salir");
        }
    } 
}

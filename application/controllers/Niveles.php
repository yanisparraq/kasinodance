<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Niveles extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();        
        $this->layout->setLayout("ajax");
    }   
 
    public function index()
    {        

        //zona de car;ga de los datos
                $datos=$this->niveles_model->getTodos();
                
                    // $rol=$this->rol_model->getTodos();
        
                $this->layout->view("index",compact('datos'));    
    }  
    public function add()
    {
     if($this->input->post())
        {
                    $data=array
                    (
                    'nombre_nivel'=>$this->input->post('nombre_nivel',true), 
                    'edad_minima'=>$this->input->post('edad_minima',true), 
                    'edad_mayor'=>$this->input->post('edad_mayor',true), 
                   
                    
                                                      
                    );
               $insertar1=$this->niveles_model->insertar($data);
            $this->session->set_flashdata('css','success');
            $this->session->set_flashdata('mensaje','El registro se ha creado exitosamente');
             redirect(base_url()."niveles");

        }
  
        $datos=$this->usuarios_model->getProfesores();     
        $datos_Cursos=$this->categorias_model->getCategorias();        
        $this->layout->view("add",compact('datos','datos_Cursos'));
    }
    public function edit($id=null,$pagina=null)
    {
        // redirect(base_url()."usuarios");       
        // redirect(base_url()."usuarios");
        if($this->input->post())
        {          
          $data=array
            (
                    'nombre_nivel'=>$this->input->post('nombre_nivel',true), 
                    'edad_minima'=>$this->input->post('edad_minima',true), 
                    'edad_mayor'=>$this->input->post('edad_mayor',true),  
                                              
            );
                $dato=$this->niveles_model->update($data,$this->input->post('id',true));
                              $this->session->set_flashdata('css','success');
                $this->session->set_flashdata('mensaje','El registro se ha modificado exitosamente');
                redirect(base_url()."niveles");
        }
              
                              
                    // $rol=$this->rol_model->getTodos();
        
                //$this->layout->view("index",compact('dato'));               
                // $rol=$this->rol_model->getTodos();        
               $dato=$this->niveles_model->getid($id);

                    
                $this->layout->view("edit",compact('dato','id'));
                //  $this->layout->view('add',compact('datos','roles','tipo_usuario','id','pagina'));
    }
    public function delete($id=null)
    {
        if(!$id){show_404();}  
        $this->niveles_model->delete($id);
        $this->session->set_flashdata('css','success');
        $this->session->set_flashdata('mensaje','El registro se ha eliminado exitosamente');
        redirect(base_url()."niveles");
    }
   
  
}





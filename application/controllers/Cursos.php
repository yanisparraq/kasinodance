<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cursos extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();        
        $this->layout->setLayout("ajax");
    }   
 
    public function index()
    {        

        //zona de car;ga de los datos
                $datos=$this->cursos_model->getTodos();
                
                    // $rol=$this->rol_model->getTodos();
        
                $this->layout->view("index",compact('datos'));    
    }  
    public function add()
    {
     if($this->input->post())
        {
                    $data=array
                    (
                    'id_categoria'=>$this->input->post('id_categoria',true), 
                    'id_usuario_profesor'=>$this->input->post('id_usuario_profesor',true), 
                    'periodo'=>$this->input->post('reservation',true), 
                   'costo'=>$this->input->post('costo',true),
                    'nivel'=>$this->input->post('nivel',true),

                    
                    
                                                      
                    );
               $insertar1=$this->cursos_model->insertar($data);
            $this->session->set_flashdata('css','success');
            $this->session->set_flashdata('mensaje','El registro se ha creado exitosamente');
             redirect(base_url()."cursos");

        }
  
        $datos=$this->usuarios_model->getProfesores();     
        $datos_Cursos=$this->categorias_model->getCategorias();   
         $datos_niveles=$this->niveles_model->getNiveles();        
        $this->layout->view("add",compact('datos','datos_Cursos','datos_niveles'));
    }
    public function edit($id=null,$pagina=null)
    {
        // redirect(base_url()."usuarios");       
        // redirect(base_url()."usuarios");
        if($this->input->post())
        {          
          $data=array
            (
            'id_categoria'=>$this->input->post('id_categoria',true), 
            'id_usuario_profesor'=>$this->input->post('id_usuario_profesor',true), 
            'periodo'=>$this->input->post('reservation',true), 
            'costo'=>$this->input->post('costo',true),  
                                              
            );
                $dato=$this->cursos_model->update($data,$this->input->post('id',true));
                              $this->session->set_flashdata('css','success');
                $this->session->set_flashdata('mensaje','El registro se ha modificado exitosamente');
                redirect(base_url()."cursos");
        }            
                              
                    // $rol=$this->rol_model->getTodos();        
                //$this->layout->view("index",compact('dato'));               
                // $rol=$this->rol_model->getTodos();        
               $dato=$this->cursos_model->getid($id);
                $usuarios=$this->usuarios_model->getProfesores();     
                $categorias=$this->categorias_model->getCategorias(); 
                   $datos_niveles=$this->niveles_model->getNiveles();        
                $this->layout->view("edit",compact('dato','categorias','usuarios','datos_niveles','id'));
                //  $this->layout->view('add',compact('datos','roles','tipo_usuario','id','pagina'));
    }
    public function delete($id=null)
    {
        if(!$id){show_404();}  
        $this->cursos_model->delete($id);
        $this->session->set_flashdata('css','success');
        $this->session->set_flashdata('mensaje','El registro se ha eliminado exitosamente');
        redirect(base_url()."cursos");
    }
        public function deleteMatricula($id=null,$id_curso=null)
    {
        if(!$id){show_404();}  
        $this->matriculaciones_model->delete($id);
        $this->session->set_flashdata('css','success');
        $this->session->set_flashdata('mensaje','El registro se ha eliminado exitosamente');
               

                $datos=$this->matriculaciones_model->getTodos();
                
                  
            $curso=$this->cursos_model->getid($id_curso);
                $this->layout->view("matricular",compact('datos','curso'));   
    }
    public function matricular($id=null)
    {        

        if(!$id){show_404();}  
        if($this->input->post())
        {          
          $data=array
            (
            'id_usuario_alumno'=>$this->input->post('id_curso',true), 
            'id_curso'=>$id,  
                                              
            );
            $insertar=$this->matriculaciones_model->insertar($data);
            $this->session->set_flashdata('css','success');
            $this->session->set_flashdata('mensaje','El registro se ha Creado exitosamente');
             //   redirect(base_url()."cursos");
        }   
        //zona de car;ga de los datos
            $datos=$this->matriculaciones_model->getTodos();
                
                    // $rol=$this->rol_model->getTodos();
        $curso=$this->cursos_model->getid($id);
        $this->layout->view("matricular",compact('datos','curso'));    
    } 
    public function auto_complete_usuario($id=null,$rol=null)
    {
      
        //$datos=$this->productos_model->getTodos();
        //print_r($datos);exit;
         $datos=$this->usuarios_model->getAlumnos($id);     
       // $datos=$this->productos_model->auto_complete($id);
        $this->layout->view("auto_complete_usuario",compact('datos'));
    } 
   
  
}





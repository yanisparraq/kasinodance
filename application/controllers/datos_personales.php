<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inspecciones extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();        
        $this->layout->setLayout("ajax");
    }
    
 
    public function index()
    {
        
        if($this->uri->segment(3))
        {
            $pagina=$this->uri->segment(3);
        }else
        {
            $pagina=0;
        }
        $porpagina=10;
        //zona de car;ga de los datos
        $datos=$this->inspecciones_model->getTodosPaginacion($pagina,$porpagina,"limit");
     
        $cuantos=$this->inspecciones_model->getTodosPaginacion($pagina,$porpagina,"cuantos");           //zona de configuración de la librería pagination
        $config['base_url']=base_url()."inspecciones/index";
        $config['total_rows']=$cuantos;
        $config['per_page']=$porpagina;
        $config['uri_segment']='3';
        $config['num_links']='4';
        $config['first_link']='Primero';
        $config['next_link']='Siguiente';
        $config['prev_link']='Anterior';
        $config['last_link']='Última';
        
        $config['full_tag_open']='<ul class="pagination text-center">';
        
       
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';    
        
        
        $config['full_tag_close']='</ul>';
        $this->pagination->initialize($config);
        $this->layout->view("index",compact('datos','cuantos','pagina'));
    
    }
   
    public function add()
    {
        //redirect(base_url()."productos");
        if($this->input->post())
        {
               //  redirect(base_url()."productos");
            $data=array
            (
                'serie'=>$this->input->post('serie',true),
                'fecha_registro'=>date (DATE_W3C ),
                'nsg'=>$this->input->post('nsg',true),
                 'articulo'=>$this->input->post('articulo',true),
                'marca'=>$this->input->post('marca',true),
                'modelo'=>$this->input->post('modelo',true),
                'tipo'=>$this->input->post('tipo',true),
                'unidad'=>$this->input->post('unidad',true),
                'estadofisico'=>$this->input->post('estadofisico',true),
                'situación_Actual'=>$this->input->post('situación_Actual',true),
                'id_usuario'=>$this->session->userdata('id'),
                'color'=>$this->input->post('color',true),
                  'fecha_fabrica'=>$this->input->post('fecha_fabrica',true),
                'fecha_reparacion'=>$this->input->post('fecha_reparacion',true),
                'problema'=>$this->input->post('problema',true),
                'analisi'=>$this->input->post('analisi',true),
                'recomendacion'=>$this->input->post('recomendacion',true),

               
            );
                $insertar=$this->inspecciones_model->insertar($data);
                $this->session->set_flashdata('css','success');
                $this->session->set_flashdata('mensaje','El registro se ha creado exitosamente');
                redirect(base_url()."inspecciones"); 
        }
        $tipo_usuario=$this->tipo_usuario_model->getTodos();
        $categorias=$this->categorias_model->getTodos();
      //  $usuario=$this->usuarios_model->getTodosPorId($id);
        $this->layout->view("add",compact('tipo_usuario','categorias','usuario'));

    }
    public function edit($id=null,$pagina=null)
    {
    // redirect(base_url()."usuarios");
        if(!$id){show_404();
        }
     // redirect(base_url()."usuarios");
        $datos=$this->inspecciones_model->getId($id);
        if($datos<1){show_404();
        }
         $datos=$this->inspecciones_model->getId2($id);
        if($this->input->post())
        { 

                          $data=array
            (
               'fecha_registro'=>date (DATE_W3C ),
               'nsg'=>$this->input->post('nsg',true),
                'serie'=>$this->input->post('serie',true),
                'articulo'=>$this->input->post('articulo',true),
                'marca'=>$this->input->post('marca',true),
                'modelo'=>$this->input->post('modelo',true),
                'tipo'=>$this->input->post('tipo',true),
                'unidad'=>$this->input->post('unidad',true),
                'estadofisico'=>$this->input->post('estadofisico',true),
                'situación_Actual'=>$this->input->post('situación_Actual',true),
                'id_usuario'=>$this->session->userdata('id'),
                'color'=>$this->input->post('color',true),
                  'fecha_fabrica'=>$this->input->post('fecha_fabrica',true),
                'fecha_reparacion'=>$this->input->post('fecha_reparacion',true),
                'problema'=>$this->input->post('problema',true),
                'analisi'=>$this->input->post('analisi',true),
                'recomendacion'=>$this->input->post('recomendacion',true),

               
            );
                
                $this->inspecciones_model->update($data,$id);
                 $this->session->set_flashdata('css','success');
                $this->session->set_flashdata('mensaje','El registro se ha modificado exitosamente');
                redirect(base_url()."inspecciones");
        }
       // $roles=$this->tipo_usuario_model->getTodos();              
        $this->layout->view('edit',compact('datos','id'));
    }
    public function restringido1()
    {
        if($this->session->userdata('id'))
        {
            $this->layout->view("restringido1");
        }else
        {
            redirect(base_url()."acceso/login");
        }
    }
    public function salir()
        {
            $this->session->sess_destroy("manosenelcodigo");
            redirect(base_url().'acceso/login',  301);
        }


    public function registro()
    {
        if($this->input->post())
        {
            if($this->form_validation->run('add_producto'))
            {
                $data=array
                (
                    'nombre'=>$this->input->post('nombre',true),
                    'precio'=>$this->input->post('precio',true),
                    'stock'=>$this->input->post('stock',true),
                    'fecha'=>date("Y-m-d"),
                );
                $insertar=$this->productos_model->insertar($data);
                //echo $insertar;exit;
                $this->session->set_flashdata('css','success');
                $this->session->set_flashdata('mensaje','El registro se ha creado exitosamente');
                redirect(base_url()."productos");
            }
        }

        $this->layout->view("registro");
    }


    public function delete($id=null)
    {
        if(!$id){show_404();}
        $datos=$this->usuarios_model->getTodosPorId($id);
        //if(sizeof($datos)==0){show_404();}
        $this->usuarios_model->delete($id);
        $this->session->set_flashdata('css','success');
        $this->session->set_flashdata('mensaje','El registro se ha eliminado exitosamente');
        redirect(base_url()."usuarios");
    }
    public function foto()
    {

    $post = $this->input->post();
    $messageTxt='NULL';
    $attachment_name='';
    $file_ext='';
    $mime_type='';

    if(isset($post['type'])=='Attachment')
    { 
     
      $AttachmentData = $this->ChatAttachmentUpload();
      //print_r($AttachmentData);
      $attachment_name ='public/avatar/'.$AttachmentData['file_name'];
      $file_ext = $AttachmentData['file_ext'];
      $mime_type = $AttachmentData['file_type']; 

      $data=array
      (
        
         'avatar' =>"$attachment_name",
                            
      );  

        $this->usuarios_model->update($data,$this->session->userdata('id'));
        $this->session->set_flashdata('css','success');
        $this->session->set_flashdata('mensaje','El registro se ha modificado exitosamente');
              //  redirect(base_url()."usuarios");  
               redirect(base_url()."perfil"); 
    }
        
    }

       public function doc2($id=null)
        {


            $datos=$this->inspecciones_model->prueba($id);
        //construir la estructura dinámica del PDF
        foreach($datos as $dato)
        {
         $url =base_url().'public/qr/'.$id.'.png';
          $url2 =base_url().'inspecciones/doc2/'.$id;
          // 
 if (!file_exists("public/qr/".$id.".png")){
        

       
        qr($url,$url2,$id);
        // qr($url,$url2,$id); 
    // pdf($datos); 
      }
        //  pdf($datos); 

      require('public/fpdf/fpdf.php');

      $pdf = new FPDF();
$pdf->AliasNbPages();
$pdf->AddPage();
 $fill = false;


// Cabecera de página
// Pie de página
 // Posición: a 1,5 cm del final



foreach($datos as $dato)
        {



 // Logo
    $pdf->Image('public/escudo.jpg',5,12,17);
    // Arial bold 15
    $pdf->SetFont('Arial','B',15);
    // Movernos a la derecha
    $pdf->Cell(1);
        $pdf->SetDrawColor(254,254,254);
        $pdf->SetLineWidth(0.5);
        $pdf->SetTextColor(225);
    // Título    $pdf->Cell(90,4,'______________________','',0,'C',false);  
    $pdf->Cell(10,10,'','',0,'C');
      $pdf->SetFillColor(250,0,0);
    $pdf->Cell(19,20,utf8_decode('PERU'),'1',0,'C',true);
    $pdf->SetFont('Arial','B',12);
    $pdf->SetFillColor(0);
    $pdf->Cell(40,7,'','LR',0,'L',true);
    $pdf->SetFillColor(80);
    $pdf->Cell(59,7,'','LRT',0,'L',true);
     $pdf->SetFillColor(200);
    $pdf->Cell(59,7,'','LRT',1,'C',true);

    $pdf->Cell(30);
      
      $pdf->SetFillColor(0);
    $pdf->Cell(40,5,'Ministerio','LR',0,'L',true);
     $pdf->SetFillColor(80);

    $pdf->Cell(59,5,utf8_decode('Ejército'),'LR',0,'L',true);
     $pdf->SetFillColor(200);
      $pdf->SetTextColor(0);
    $pdf->Cell(59,5,utf8_decode('Comando de Educación y'),'LR',1,'L',true);

    $pdf->Cell(30);
     $pdf->SetFillColor(0);
      $pdf->SetTextColor(250);
     $pdf->Cell(40,5,'de Defensa','RL',0,'L',true);

      $pdf->SetFillColor(80);
       $pdf->SetTextColor(250);
        $pdf->Cell(59,5,utf8_decode('del Perú'),'LR',0,'L',true);
       $pdf->SetFillColor(200); 
        $pdf->SetTextColor(0); 
    $pdf->Cell(59,5,utf8_decode('Doctrina del Ejército'),'LR',1,'L',true);


        $pdf->Cell(30);
          $pdf->SetFillColor(0);
     $pdf->Cell(40,3,'','LRB',0,'L',true);
      $pdf->SetFillColor(80);
        $pdf->Cell(59,3,'','LRB',0,'L',true);
        $pdf->SetFillColor(200);
         $pdf->SetTextColor(0);
    $pdf->Cell(59,3,'','LRB',0,'C',true);
   
    // Salto de línea
    $pdf->Ln(5);

    $pdf->Cell(25,10,'','',0,'C',$fill);
   // $pdf->Image('public/escudo.jpg',5,12,17);
    $pdf->Image('public/qr/firma.jpg',135,245,22);
    $pdf->Image('public/qr/'.$dato->id_documento.'.png',90,250,-200);
     $pdf->Cell(10);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(150,7,utf8_decode('INFORME DE INSPECCION TECNICA Nº '.$dato->id_documento.' /ST-DETEL/COEDE'),'','C',false);
    $pdf->Cell(26,5,'','',1,'C',$fill);
    $pdf->Cell(201,3,'','',1,'C',false);
    $pdf->SetFillColor(194,194,194);
    $pdf->Cell(7,5,'01.','',0,'',false);
    $pdf->SetFont('Arial','UB',10);
    $pdf->Cell(55,5,'DATOS DE IDENTIFICACION:','',1,'',false);
    $pdf->SetFont('Arial','',10);
    $pdf->Cell(7,5,'','',0,'',false);
    $pdf->Cell(35,5,'N.S.G.                      :','',0,'',false);
     $pdf->Cell(55,5,$dato->nsg,'',1,'',false);
     $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(35,5,'SERIE NUMERO     :','',0,'',false);
     $pdf->Cell(55,5,$dato->serie,'',1,'',false);
      $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(35,5,'ARTICULO               :','',0,'',false);
     $pdf->Cell(55,5,utf8_decode($dato->articulo),'',1,'',false);
      $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(35,5,'COLOR                    :','',0,'',false);
     $pdf->Cell(55,5,$dato->color,'',1,'',false);
      $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(35,5,'MARCA                    :','',0,'',false);
     $pdf->Cell(55,5,utf8_decode($dato->marca),'',1,'',false);

     $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(35,5,'MODELO                 :','',0,'',false);
     $pdf->Cell(55,5,$dato->modelo,'',1,'',false);



     $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(35,5,'TIPO                        :','',0,'',false);
     $pdf->Cell(55,5,$dato->tipo,'',1,'',false);

    $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(35,5,'UNIDAD                   :','',0,'',false);
     $pdf->Cell(55,5,$dato->unidad,'',1,'',false);

    $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(35,5,utf8_decode('AÑO FABRICAC.    :'),'',0,'',false);
     $pdf->Cell(55,5,$dato->fecha_fabrica,'',1,'',false);



    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(7,10,'02.','',0,'',false);
    $pdf->SetFont('Arial','UB',10);
    $pdf->Cell(55,10,'DATOS ADMINISTRATIVOS:','',0,'',false);

    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(7,10,'03.','',0,'',false);
    $pdf->SetFont('Arial','UB',10);
    $pdf->Cell(55,10,utf8_decode('ESTADO DEL ARTÍCULO:'),'',1,'',false);
 

    $pdf->SetFont('Arial','',10);
    $pdf->Cell(7,5,'','',0,'',false);

    $pdf->Cell(65,5,'ESTADO FISICO DEL EQUIPO CAT    :','',0,'',false);
     $pdf->Cell(55,5,utf8_decode($dato->estadofisico),'',1,'',false);
     $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(65,5,'ULT. FECHA REPARAC.                      :','',0,'',false);
     $pdf->Cell(55,5,$dato->fecha_reparacion,'',1,'',false);


    $pdf->Cell(7,5,'','',0,'',false);
          $pdf->Cell(65,5,'SITUACION ACTUAL                            :','',0,'',false);
     $pdf->Cell(55,5,utf8_decode($dato->situación_Actual),'',1,'',false);


    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(7,10,'04.','',0,'',false);
    $pdf->SetFont('Arial','UB',10);
    $pdf->Cell(55,10,'PROBLEMAS QUE PRESENTA:','',1,'',false);

  

    $pdf->SetFont('Arial','',10);
    $pdf->Cell(7,5,'','',0,'',false);
        $pdf->MultiCell(0,5,utf8_decode($dato->problema));
  
    // $pdf->Cell(55,5,utf8_decode('-   '.$dato->problema),'',1,'',false);
     

    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(7,10,'05.','',0,'',false);
    $pdf->SetFont('Arial','UB',10);
    $pdf->Cell(55,10,'ANALISIS:','',1,'',false);

  

    $pdf->SetFont('Arial','',10);
    $pdf->Cell(7,5,'','',0,'',false);

    $pdf->MultiCell(0,5,utf8_decode('-   '.$dato->analisi));
    
    

    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(7,10,'06.','',0,'',false);
    $pdf->SetFont('Arial','UB',10);
  

    $pdf->Cell(55,10,utf8_decode('RECOMENDACIÓN:'),'',1,'',false);

  

    $pdf->SetFont('Arial','',10);
    $pdf->Cell(7,5,'','',0,'',false);

   $pdf->MultiCell(0,5,utf8_decode('-   '.$dato->recomendacion));
    
  

          $pdf->SetFont('Arial','B',10);
   

    $pdf->Cell(180,7,'Chorrillos,'.  fecha($dato->fecha_registro),'',1,'R',false);   
$pdf->Ln(5);


  // Posición: a 1,5 cm del final
    $pdf->SetY(240);
    // Arial italic 8
       $pdf->Cell(90,3,'JEFE DEL  DEPARTAMENTO','',0,'C',false);  
    $pdf->Cell(90,3,'ELEMENTO TECNICO','',1,'C',false);   

     $pdf->Cell(90,3,'DE TELEMATICA','',0,'C',false);  
    $pdf->Cell(90,3,'INSPECTOR','',1,'C',false);   


$pdf->Ln(8);

    $pdf->Cell(90,4,'______________________','',0,'C',false);  
    $pdf->Cell(90,4,'______________________','',1,'C',false); 


     $pdf->Cell(90,4,'O- 24724071 - O+','',0,'C',false);  
    $pdf->Cell(90,4,'S- 621995000 - A','',1,'C',false); 

      $pdf->Cell(90,4,utf8_decode('CÉSAR SALAS VILCHEZ'),'',0,'C',false);  
    $pdf->Cell(90,4,'R. MENDOZA P','',1,'C',false); 


      $pdf->Cell(90,4,'CORONEL EP','',0,'C',false);  
    $pdf->Cell(90,4,'EC SAF','',1,'C',false); 
    $pdf->SetTitle('Informe Isnpeccion');
    $pdf->SetFont('Arial','I',8);



    // Número de página
   // $pdf->Cell(0,5,utf8_decode('Pagína').' '.$pdf->PageNo().' de {nb}',0,0,'C');








  $pdf->Output('I',$dato->id_documento.'_TECNICA.pdf');
}
    }
  }




         public function doc3($id=null)
    {
    //C 
        qr();
    }
  }

















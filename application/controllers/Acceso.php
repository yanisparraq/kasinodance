<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Acceso extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();        
        $this->layout->setLayout("frontendLogin");
          // redirect(base_url()."acceso/login");        
        //$this->layout->setLayout("frontendLogin");
    } 
	public function index()
	{		
        $datos=$this->productos_model->getTodos();
        //print_r($datos);exit;
        $this->layout->view("index",compact('datos'));
	}
    public function login()
    {
        if($this->session->userdata('id'))
        {
            redirect(base_url()."home/");           
           // redirect(base_url()."acceso/login");
        }

        if($this->input->post())
        {
              //crear y referenciar un método para preguntar si los datos
                //ingresados por el usuario existen en la bd
                $datos=$this->usuarios_model->getLogin($this->input->post('correo',true),sha1($this->input->post('pass',true)));
                //crear una condición para validar lo anterior
                if($datos==0)
                {
                    $this->session->set_flashdata('css','danger');
                    $this->session->set_flashdata('mensaje','Los datos ingresados no existen en nuestra base de datos');
                    redirect(base_url()."acceso/login");
                }else
                {
                    //darle un nombre al array general de sessiones
                    $this->session->set_userdata('manosenelcodigo');
                    //asignamos los datos a cada sessión por separado
                    $datos=$this->usuarios_model->getLogin2($this->input->post('correo',true),sha1($this->input->post('pass',true)));
                    $this->session->set_userdata('id',$datos->id);
                    $this->session->set_userdata('nombre_de_usuario',$datos->nombre_de_usuario);
                    $this->session->set_userdata('tipo_usuario_id',$datos->tipo_usuario_id);
                  
                   
                    //redireccionamos a la url principal de los contenidos restringidos
                    if($this->session->userdata('tipo_usuario_id')==1)
                    {
                        $this->layout->setLayout("frontend");
                        redirect(base_url()."home/");
                    }else
                    { //redirect(base_url()."acceso/salir");
                        $this->layout->setLayout("frontend");
                        redirect(base_url()."home/");
                    }                   
                }
            
        }
       $this->layout->view('login');
    }
    public function restringido1()
    {
        if($this->session->userdata('id'))
        {
            $this->layout->view("restringido1");
        }else
        {
            redirect(base_url()."acceso/login");
        }
    }
    public function salir()
        {
            $this->session->sess_destroy("manosenelcodigo");
            redirect(base_url().'acceso/login',  301);
        }
    public function registro()
    {
        if($this->input->post())
        {
            if($this->form_validation->run('add_usuario'))
            {
                $data=array
                (
                    'nombre'=>$this->input->post('nombre',true),
                    'correo'=>$this->input->post('correo',true),
                    'telefono'=>$this->input->post('telefono',true),
                    'pass'=>sha1($this->input->post('password',true)),
                    'rol_id'=>4,
                    'id_categoria_evento'=>1,
                   
                    'club'=>"Público",
                );
                $insertar=$this->usuarios_model->insertar($data);
                //echo $insertar;exit;
                $this->session->set_flashdata('css','success');
                $this->session->set_flashdata('mensaje','Creado exitosamente');
                redirect(base_url()."acceso/login");
            }
        }
        $this->layout->view("registro");
    }
}
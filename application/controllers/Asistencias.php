<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asistencias extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();        
        $this->layout->setLayout("ajax");
    }    
    public function index()
    {        
        //zona de car;ga de los datos
        if($this->input->post())
        {
             $dato=$this->datos_personales_model->getPersonales($this->input->post('codigo',true));
             if($dato <= 0){
            $this->session->set_flashdata('css','error');
            $this->session->set_flashdata('mensaje','El Estudiante se Encuetra Moroso');
           // $this->session->set_flashdata('tiempo','10000');
             }else{
                 $dato2=$this->asistencias_model->getAsistencia($dato,date("Y-m-d"));
                 if($dato2 >0)
                 {
                    $dato3=$this->asistencias_model->getAsistencia2($dato,date("Y-m-d"));
                       $data=array
                            (                    
                            'id_usuario'=>$dato, 
                             'registro'=>0, 
                             'fecha2'=>  date("Y-m-d"),
                         //    'hora'=>  date("h:i:a"),
                             'hora_salida'=>  date("h:i:a"),                    
                            );
                        $this->session->set_flashdata('mensaje','Salida registrada exitosamente');
                             $actualizar = $this->asistencias_model->update($data,$dato3);
                    }else
                    {
                        $data=array
                            (                    
                            'id_usuario'=>$dato, 
                             'registro'=>1, 
                             'fecha2'=>  date("Y-m-d"),
                             'hora'=>  date("h:i:a"),                   
                            );
                     $this->session->set_flashdata('mensaje','Entrada registrada exitosamente');
                       $insertar = $this->asistencias_model->insertar($data);
                 }
                        $this->session->set_flashdata('css','success');
            //$this->session->set_flashdata('tiempo','5000');
             }
        }
             $this->layout->view("index");        
                    // $rol=$this->rol_model->getTodos();
    }
     public function ver($id = null)
    {
        //$datos ="";
         $datos=$this->categorias_model->getTodos();
         $this->layout->view("ver",compact('datos'));

    }
      public function lista()
    {
        
        if($this->uri->segment(3))
        {
            $pagina=$this->uri->segment(3);
        }else
        {
            $pagina=0;
        }
        $porpagina=10;
        //zona de car;ga de los datos
        $datos=$this->asistencias_model->getTodosPaginacion($pagina,$porpagina,"limit");
      
        $cuantos=$this->asistencias_model->getTodosPaginacion($pagina,$porpagina,"cuantos");           //zona de configuración de la librería pagination
        $config['base_url']=base_url()."asistencias/lista";
        $config['total_rows']=$cuantos;
        $config['per_page']=$porpagina;
        $config['uri_segment']='3';
        $config['num_links']='4';
        $config['first_link']='Primero';
        $config['next_link']='Siguiente';
        $config['prev_link']='Anterior';
        $config['last_link']='Última';
        
        $config['full_tag_open']='<ul class="pagination text-center">';
        
       
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';    
        
        
        $config['full_tag_close']='</ul>';
        $this->pagination->initialize($config);
        $this->layout->view("lista",compact('datos','cuantos','pagina'));
    
    }
}
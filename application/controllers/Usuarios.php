<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();        
        $this->layout->setLayout("ajax");
    }
    
 
    public function index()
    {
        
        if($this->uri->segment(3))
        {
            $pagina=$this->uri->segment(3);
        }else
        {
            $pagina=0;
        }
        $porpagina=10;
        //zona de car;ga de los datos
        $datos=$this->usuarios_model->getTodosPaginacion($pagina,$porpagina,"limit");
        $rol=$this->rol_model->getTodos();
        $cuantos=$this->usuarios_model->getTodosPaginacion($pagina,$porpagina,"cuantos");           //zona de configuración de la librería pagination
        $config['base_url']=base_url()."Usuarios/index";
        $config['total_rows']=$cuantos;
        $config['per_page']=$porpagina;
        $config['uri_segment']='3';
        $config['num_links']='4';
        $config['first_link']='Primero';
        $config['next_link']='Siguiente';
        $config['prev_link']='Anterior';
        $config['last_link']='Última';
        
        $config['full_tag_open']='<ul class="pagination text-center">';
        
       
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';    
        
        
        $config['full_tag_close']='</ul>';
        $this->pagination->initialize($config);
        $this->layout->view("index",compact('datos','cuantos','pagina','rol'));
    
    }
    public function login()
    {
        if($this->session->userdata('id'))
        {
             redirect(base_url()."home/");           
           // redirect(base_url()."acceso/login");
        }
        if($this->input->post())
        {
            if($this->form_validation->run('login'))
            {
                //crear y referenciar un método para preguntar si los datos
                //ingresados por el usuario existen en la bd
                $datos=$this->usuarios_model->getLogin($this->input->post('correo',true),sha1($this->input->post('pass',true)));
                //crear una condición para validar lo anterior
                if(sizeof($datos)==0)
                {
                    $this->session->set_flashdata('css','danger');
                    $this->session->set_flashdata('mensaje','Los datos ingresados no existen en nuestra base de datos');
                    redirect(base_url()."acceso/login");
                }else
                {
                    //darle un nombre al array general de sessiones
                    $this->session->set_userdata('manosenelcodigo');
                    //asignamos los datos a cada sessión por separado
                    $this->session->set_userdata('id',$datos->id);
                    $this->session->set_userdata('nombre',$datos->nombre);
                    //redireccionamos a la url principal de los contenidos restringidos
                    redirect(base_url()."home/");
                }
            }
        }
        $this->layout->view('login');
    }
    public function add()
    {
        //redirect(base_url()."productos");
        if($this->input->post())
        {
               //  redirect(base_url()."productos");
            $data=array
            (
                'nombre_de_usuario'=>$this->input->post('nombre_de_usuario',true).' '.$this->input->post('apellido',true),
                
                'correo'=>$this->input->post('correo',true),
                'tipo_usuario_id'=>$this->input->post('tipo_usuario',true),
                'id_rol'=>$this->input->post('rol',true),
                'clave_usuario'=>sha1($this->input->post('password',true)),
            );

                $insertar1=$this->usuarios_model->insertar($data);
                            $data2=array
            (
                'nombre1'=>$this->input->post('nombre_de_usuario',true),
                //'nombre2'=>$this->input->post('nombre_de_usuario',true),
                'apellido'=>$this->input->post('apellido',true),
                'dni'=>$this->input->post('dni',true),
               // 'correo'=>$this->input->post('correo',true),
                'id_usuario'=> $insertar1,
               /*  'dni'=>$this->input->post('dni',true),
                'correo'=>$this->input->post('correo',true),
                'tipo_usuario_id'=>$this->input->post('tipo_usuario',true),
                'fecha_registro'=>78789,
                'clave_usuario'=>sha1($this->input->post('password',true)),*/
            );
                 $insertar2=$this->datos_personales_model->insertar($data2);
                $this->session->set_flashdata('css','success');
                $this->session->set_flashdata('mensaje','El registro se ha creado exitosamente');
                redirect(base_url()."usuarios"); 
        }
        $tipo_usuario=$this->tipo_usuario_model->getTodos();
       // $categorias=$this->categorias_model->getTodos();
         $rol=$this->rol_model->getTodos();
        $this->layout->view("add",compact('tipo_usuario','rol'));
    }
    public function edit($id=null,$pagina=null)
    {
        // redirect(base_url()."usuarios");
        if(!$id){show_404();}
        // redirect(base_url()."usuarios");
        $datos=$this->usuarios_model->getTodosPorId3($id);
        if($datos==0){show_404();}
          $datos=$this->usuarios_model->getTodosPorId($id);
        if($this->input->post())
        {
          
                if($this->input->post('password',true))
                {
                    $data=array
                    (
                    'nombre_de_usuario'=>$this->input->post('nombre',true).' '.$this->input->post('apellido',true),
                  
                    'correo'=>$this->input->post('correo',true),
                        'tipo_usuario_id'=>$this->input->post('tipo_usuario',true),
                    'id_rol'=>$this->input->post('rol',true),
                    'clave_usuario'=>sha1($this->input->post('password',true)),
                    'fecha_registro'=>78789,                   
                    );
                }else{
                $data=array
                (
                    'nombre_de_usuario'=>$this->input->post('nombre',true).' '.$this->input->post('apellido',true),
                    'correo'=>$this->input->post('correo',true),
                   'tipo_usuario_id'=>$this->input->post('tipo_usuario',true),
                    'id_rol'=>$this->input->post('rol',true),
                    'fecha_registro'=>78789,
                   
                     
                    );
                   // 'pass'=>$this->input->post('password',true),
                }
                
                $this->usuarios_model->update($data,$id);
                 $this->session->set_flashdata('css','success');
                $this->session->set_flashdata('mensaje','El registro se ha modificado exitosamente');
                redirect(base_url()."usuarios");
        }


               $tipo_usuario=$this->tipo_usuario_model->getTodos();
       // $categorias=$this->categorias_model->getTodos();
         $rol=$this->rol_model->getTodos();
        $this->layout->view("edit",compact('datos','tipo_usuario','rol','id'));
      //  $roles=$this->tipo_usuario_model->getTodos();   
           
      //  $this->layout->view('add',compact('datos','roles','tipo_usuario','id','pagina'));
    }
    public function restringido1()
    {
        if($this->session->userdata('id'))
        {
            $this->layout->view("restringido1");
        }else
        {
            redirect(base_url()."acceso/login");
        }
    }
    public function salir()
        {
            $this->session->sess_destroy("manosenelcodigo");
            redirect(base_url().'acceso/login',  301);
        }


    public function registro()
    {
        if($this->input->post())
        {
            if($this->form_validation->run('add_producto'))
            {
                $data=array
                (
                    'nombre'=>$this->input->post('nombre',true),
                    'precio'=>$this->input->post('precio',true),
                    'stock'=>$this->input->post('stock',true),
                    'fecha'=>date("Y-m-d"),
                );
                $insertar=$this->productos_model->insertar($data);
                //echo $insertar;exit;
                $this->session->set_flashdata('css','success');
                $this->session->set_flashdata('mensaje','El registro se ha creado exitosamente');
                redirect(base_url()."productos");
            }
        }

        $this->layout->view("registro");
    }


    public function delete($id=null)
    {
        if(!$id){show_404();}
        $datos=$this->usuarios_model->getTodosPorId($id);
        //if(sizeof($datos)==0){show_404();}
        $this->usuarios_model->delete($id);
        $this->session->set_flashdata('css','success');
        $this->session->set_flashdata('mensaje','El registro se ha eliminado exitosamente');
        redirect(base_url()."usuarios");
    }
    public function foto()
    {

    $post = $this->input->post();
    $messageTxt='NULL';
    $attachment_name='';
    $file_ext='';
    $mime_type='';

    if(isset($post['type'])=='Attachment')
    { 
     
      $AttachmentData = $this->ChatAttachmentUpload();
      //print_r($AttachmentData);
      $attachment_name ='public/avatar/'.$AttachmentData['file_name'];
      $file_ext = $AttachmentData['file_ext'];
      $mime_type = $AttachmentData['file_type']; 

      $data=array
      (
        
         'avatar' =>"$attachment_name",
                            
      );  

        $this->usuarios_model->update($data,$this->session->userdata('id'));
        $this->session->set_flashdata('css','success');
        $this->session->set_flashdata('mensaje','El registro se ha modificado exitosamente');
              //  redirect(base_url()."usuarios");  
               redirect(base_url()."perfil"); 
    }
        
    }
    public function ver($id=null)
    {
         $datos=$this->usuarios_model->VerUsuarios($id);
          $cursos=$this->cursos_model->getProfesoresPorId($datos->id);
         $this->layout->view('ver',compact('id','datos','cursos'));

    }
    public function imprimir($id=null)
    {
         $datos=$this->usuarios_model->VerUsuarios($id);
          $cursos=$this->cursos_model->getProfesoresPorId($datos->id);
       //  $this->layout->view('ver',compact('id','datos','cursos'));
         $this->layout->view('imprimir',compact('id','datos','cursos'));
    }
    public function aistencias($id=null)
    {
         $datos=$this->usuarios_model->VerUsuarios($id);
          $cursos=$this->cursos_model->getProfesoresPorId($datos->id);
       //  $this->layout->view('ver',compact('id','datos','cursos'));
         $this->layout->view('aistencias',compact('id','datos','cursos'));
    }
  
}





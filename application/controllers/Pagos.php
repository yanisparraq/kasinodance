<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagos extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();        
        $this->layout->setLayout("ajax");
    }    
    public function index()
    {        
        //zona de car;ga de los datos
        $datos=$this->pagos_model->getTodos();
        
             $this->layout->view("index",compact('datos'));        
                    // $rol=$this->rol_model->getTodos();
    }
     public function ver($id = null)
    {
        //$datos ="";
         $datos=$this->categorias_model->getTodos();
         $this->layout->view("ver",compact('datos'));
    }
    public function auto_complete_usuario($id=null)
    {      
        //$datos=$this->productos_model->getTodos();
        //print_r($datos);exit;
         $datos=$this->usuarios_model->getAlumnos($id);     
       // $datos=$this->productos_model->auto_complete($id);
        $this->layout->view("auto_complete_usuario",compact('datos'));
    } 
    public function cursos($id=null)
    {
      
     
        $datos=$this->matriculaciones_model->getTodosId($id);
    //     echo $id;
        //print_r($datos);exit;
       //  $datos=$this->usuarios_model->getAlumnos($id);     
       // $datos=$this->productos_model->auto_complete($id);
        $this->layout->view("lista_cursos",compact('datos'));
    } 
    public function add($id=null)
    {
     if($this->input->post())
            {
                        $data=array
                        (
                        'fecha'=> date("Y-m-d"), 
                        'monto'=>$this->input->post('monto',true), 
                        'id_usuario'=>$this->input->post('id_usuario',true), 
                         'id_matricula'=>$this->input->post('id_curso',true), 
                                            
                        
                                                          
                        );
                   $insertar1=$this->pagos_model->insertar($data);
                $this->session->set_flashdata('css','success');
                $this->session->set_flashdata('mensaje','El registro se ha creado exitosamente');
                 redirect(base_url()."pagos");

            }
      
            $datos=$this->usuarios_model->getProfesores();     
            $datos_Cursos=$this->categorias_model->getCategorias();   
             $datos_niveles=$this->niveles_model->getNiveles();        
            $this->layout->view("add",compact('datos','datos_Cursos','datos_niveles'));
        }
    public function delete($id=null)
    {
        if(!$id){show_404();}  
        $this->pagos_model->delete($id);
        $this->session->set_flashdata('css','success');
        $this->session->set_flashdata('mensaje','El registro se ha eliminado exitosamente');
        redirect(base_url()."pagos");
    }
}
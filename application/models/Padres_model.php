<?php
class padres_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function insertar($data=array())
    {
        $this->db->insert('padres',$data);
        return $this->db->insert_id();
    }
    public function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('padres');
    }
    public function update($data=array(),$id)
    {
        $this->db->where('id',$id);
        $this->db->update('padres',$data);
    }
    
}


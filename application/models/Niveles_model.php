<?php
class niveles_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    
    public function getTodos()
    {
        $query=$this->db
                ->select("*")
                ->from("niveles")
               
                  ->order_by("nombre_nivel","DESC")
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->result();            
    }
    public function getid($id)
    {
        $query=$this->db
 ->select("*")
                ->from("niveles")
               
                ->where(array("id"=>$id))
               // ->order_by("nombre_categoria","desc")
                ->get();
        //echo $this->db->last_query();exit;        
        //return $query->result();     
        return $query->row();         
    }
    public function insertar($data=array())
    {
        $this->db->insert('niveles',$data);
        return $this->db->insert_id();
    }
        public function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('niveles');
    }
    public function getTodosPorId($id)
    {
        $query=$this->db
                ->select("*")
                ->from("cursos") 
                ->where(array("id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }
    public function update($data=array(),$id)
    {
        $this->db->where('id',$id);
        $this->db->update('niveles',$data);
    }
    public function getNiveles()
    {
        $query=$this->db
                ->select("id,nombre_nivel")
                ->from("niveles") 
               
                ->order_by("id","asc")
                ->get();
        //echo $this->db->last_query();exit;        
         return $query->result();           
    }

}


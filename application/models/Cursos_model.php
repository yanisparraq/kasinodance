<?php
class cursos_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    
    public function getTodos()
    {
        $query=$this->db
                ->select("cursos.*,
                   categorias.nombre_categoria,
                   usuarios.nombre_de_usuario,
                   niveles.nombre_nivel ")
                ->from("cursos")
                ->join("categorias", "cursos.id_categoria=categorias.id")
                ->join("usuarios", "cursos.id_usuario_profesor=usuarios.id") 
                ->join("niveles", "cursos.nivel=niveles.id") 
               
                  ->order_by("id","asc")
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->result();            
    }
    public function getid($id)
    {
        $query=$this->db
 ->select("cursos.*,
                   categorias.nombre_categoria,
                   usuarios.nombre_de_usuario,
                   niveles.nombre_nivel ")
                ->from("cursos")
                ->join("categorias", "cursos.id_categoria=categorias.id")
                ->join("usuarios", "cursos.id_usuario_profesor=usuarios.id") 
                ->join("niveles", "cursos.nivel=niveles.id") 
                ->where(array("cursos.id"=>$id))
               // ->order_by("nombre_categoria","desc")
                ->get();
        //echo $this->db->last_query();exit;        
        //return $query->result();     
        return $query->row();         
    }
    public function insertar($data=array())
    {
        $this->db->insert('cursos',$data);
        return $this->db->insert_id();
    }
        public function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('cursos');
    }
    public function getTodosPorId($id)
    {
        $query=$this->db
                ->select("*")
                ->from("cursos") 
                ->where(array("id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }    public function getProfesoresPorId($id)
    {
        $query=$this->db
                ->select("categorias.nombre_categoria,
                    niveles.nombre_nivel,
                    cursos.periodo")
                ->from("cursos") 
                ->join("categorias", "cursos.id_categoria=categorias.id")
                 ->join("niveles", "niveles.id=cursos.nivel")
                ->where(array("id_usuario_profesor"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
         return $query->result();      
    }
    public function update($data=array(),$id)
    {
        $this->db->where('id',$id);
        $this->db->update('cursos',$data);
    }

}


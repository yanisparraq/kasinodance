<?php
class usuarios_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function insertar($data=array())
    {
        $this->db->insert('usuarios',$data);
        return $this->db->insert_id();
    }
    public function getLogin($correo,$pass)
    {
        $query=$this->db
                ->select("id,nombre_de_usuario,correo,clave_usuario,tipo_usuario_id")
                ->from("usuarios")
                ->where(array('correo'=>$correo,'clave_usuario'=>$pass))
                ->get();
                if ($query->num_rows() > 0) {
                return '1';
            } else 
            {
                return 'Contraseña  ó Usuario incorrecto';
            }
  
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }


    public function getLogin2($correo,$pass)
    {
        $query=$this->db
                ->select("id,nombre_de_usuario,correo,clave_usuario,tipo_usuario_id")
                ->from("usuarios")
                ->where(array('correo'=>$correo,'clave_usuario'=>$pass))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }





    public function getTodosPaginacion($pagina,$porpagina,$quehago)
    {
        switch($quehago)
        {
            case 'limit':
                $query=$this->db
                        ->select("*,usuarios.id")
                        ->from("usuarios")   
                                                ->join("tipo_usuario", "usuarios.tipo_usuario_id=tipo_usuario.id")
                         ->join("rol", "usuarios.id_rol=rol.id") 
                        ->limit($porpagina,$pagina)
                        ->order_by("nombre_de_usuario","ASD")
                          ->where('id_rol >', 2)
                        ->get();
                return $query->result();        
            break;
            case 'cuantos':
                $query=$this->db
                        ->select("*,usuarios.id")
                        ->from("usuarios")   
                         ->join("tipo_usuario", "usuarios.tipo_usuario_id=tipo_usuario.id")
                         ->join("rol", "usuarios.id_rol=rol.id") 
                           ->where('id_rol >', 2)                           
                        ->count_all_results();
                return $query;
            break;
        }
    }
    public function getTodosPorId($id)
    {
        $query=$this->db
                ->select("*,usuarios.id as id,
                    datos_personales.nombre1,
                    datos_personales.apellido")
                ->from("usuarios")              
                                        ->join("tipo_usuario", "usuarios.tipo_usuario_id=tipo_usuario.id")
                         ->join("rol", "usuarios.id_rol=rol.id") 
                 ->join("datos_personales", "datos_personales.id_usuario=usuarios.id")
                ->where(array("usuarios.id"=>$id))

                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }
        public function getTodosPorId3($id)
    {
        $query=$this->db
                ->select("*,usuarios.id as id")
                ->from("usuarios")              
                ->join("tipo_usuario", "usuarios.tipo_usuario_id=tipo_usuario.id")
                ->where(array("usuarios.id"=>$id))
               // ->where('id_rol >', 2)
               // ->where('registro !=', 'Entrada')
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->num_rows();            
    }
    public function getTodosPorId2($id)
    {
        $query=$this->db
        ->select("categoria_evento.id as id_categoria,nombre,apellido,correo,pass,telefono,nombre_rol,rol_id,nombre_categoria,direccion,avatar")
        ->from("usuarios")
            ->join("rol", "usuarios.rol_id=rol.id")
            ->join("categoria_evento", "usuarios.id_categoria_evento=categoria_evento.id")     
              //   ->join("rol", "usuarios.rol_id=rol.id")
        ->where(array("usuarios.id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }
    public function getProfesores()
    {
        $query=$this->db
        ->select("*")
        ->from("usuarios")               
              //   ->join("rol", "usuarios.rol_id=rol.id")
        ->where(array("id_rol"=>4))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->result();          
    }
    public function getAlumnos($id)
    {
        $query=$this->db
        ->select("usuarios.nombre_de_usuario,
            usuarios.id,
            datos_personales.dni,
            datos_personales.nombre1,
            datos_personales.apellido")
        ->from("usuarios") 
        ->join("datos_personales", "usuarios.id=datos_personales.id_usuario")
     //   ->join("matriculaciones", "datos_personales.id_usuario<>matriculaciones.id_usuario_alumno")                    //   ->join("rol", "usuarios.rol_id=rol.id")
        ->like('nombre_de_usuario',$id,'after')
        ->or_like('dni',$id,'after') 
        ->or_like('nombre1',$id,'after')
         ->or_like('apellido',$id,'after') 
        ->where(array("id_rol"=>1))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->result();          
    }

    public function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('usuarios');
    }
    public function update($data=array(),$id)
    {
        $this->db->where('id',$id);
        $this->db->update('usuarios',$data);
    }
    
    public function VerUsuarios($id)
    {
        $query=$this->db
                ->select("usuarios.*,
                    datos_personales.nombre1,
                    datos_personales.nombre2,
                    datos_personales.apellido,
                    datos_personales.apellido2,
                    datos_personales.dni,
                    datos_personales.fecha_nacimiento,
                    datos_personales.direccion_habitacion,
                    datos_personales.telefono_movil,
                    datos_personales.telefono_fijo,
                    datos_personales.facebook,
                    datos_personales.instagran,
                    datos_personales.tiltok")
                ->from("usuarios")              
                ->join("datos_personales","datos_personales.id_usuario=usuarios.id")
                // ->join("padres","padres.id_datos_personales=datos_personales.id")
             //   ->join("usuarios","usuarios.id=alumnos.id_usuario")
               ->where(array("usuarios.id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }
      public function getProfesor($id)
    {
        $query=$this->db
                ->select("matriculaciones.id,
                    categorias.nombre_categoria,
                    usuarios.nombre_de_usuario,
                    niveles.nombre_nivel")
                ->from("matriculaciones")
                 ->join("usuarios", "usuarios.id=matriculaciones.id_usuario_alumno")
                  ->join("cursos", "cursos.id=matriculaciones.id_curso")
                  ->join("categorias", "categorias.id=cursos.id_categoria")
                  ->join("niveles", "niveles.id=cursos.nivel")
               
                
               ->where('cursos.id_usuario_profesor',$id)
                //  ->order_by("nombre_nivel","DESC")
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->result();            
    }


}


<?php
class categorias_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function getTodos()
    {
        $query=$this->db
                ->select("*")
                ->from("categorias")               
                ->order_by("id","asc")
                ->get();        
        return $query->result();            
    }
    public function getid($id)
    {
        $query=$this->db
                ->select("*")
                ->from("categorias")
                ->where(array("id"=>$id))
                ->get();     
        return $query->row();         
    }
    public function insertar($data=array())
    {
        $this->db->insert('categorias',$data);
        return $this->db->insert_id();
    }
    public function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('categorias');
    }
    public function getTodosPorId($id)
    {
        $query=$this->db
                ->select("*")
                ->from("categorias") 
                ->where(array("id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }
    public function update($data=array(),$id)
    {
        $this->db->where('id',$id);
        $this->db->update('categorias',$data);
    }
    public function getCategorias()
    {
        $query=$this->db
                ->select("id,nombre_categoria")
                ->from("categorias") 
                ->where(array("id_categori_pa"=>0))
                ->order_by("id","asc")
                ->get();
        //echo $this->db->last_query();exit;        
         return $query->result();           
    }
}
<?php
class matriculaciones_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    
    public function getTodos()
    {
        $query=$this->db
                ->select("matriculaciones.id,
                    usuarios.nombre_de_usuario,
                    usuarios.correo,
                    datos_personales.dni")
                ->from("matriculaciones")
                ->join("usuarios", "usuarios.id=matriculaciones.id_usuario_alumno")
                ->join("datos_personales", "datos_personales.id_usuario=matriculaciones.id_usuario_alumno")
                ->join("cursos", "cursos.id=matriculaciones.id_curso")
                
                
               
                //  ->order_by("nombre_nivel","DESC")
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->result();            
    }
    public function getTodosId($id)
    {
        $query=$this->db
                ->select("matriculaciones.id,
                    categorias.nombre_categoria,
                    usuarios.nombre_de_usuario,
                    niveles.nombre_nivel")
                ->from("matriculaciones")
                 ->join("usuarios", "usuarios.id=matriculaciones.id_usuario_alumno")
                  ->join("cursos", "cursos.id=matriculaciones.id_curso")
                  ->join("categorias", "categorias.id=cursos.id_categoria")
                  ->join("niveles", "niveles.id=cursos.nivel")
               
                
               ->where('matriculaciones.id_usuario_alumno',$id)
                //  ->order_by("nombre_nivel","DESC")
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->result();            
    }
    public function getid($id)
    {
        $query=$this->db
 ->select("*")
                ->from("niveles")
               
                ->where(array("id"=>$id))
               // ->order_by("nombre_categoria","desc")
                ->get();
        //echo $this->db->last_query();exit;        
        //return $query->result();     
        return $query->row();         
    }
    public function insertar($data=array())
    {
        $this->db->insert('matriculaciones',$data);
        return $this->db->insert_id();
    }
        public function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('matriculaciones');
    }
    public function getTodosPorId($id)
    {
        $query=$this->db
                ->select("*")
                ->from("cursos") 
                ->where(array("id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }
    public function update($data=array(),$id)
    {
        $this->db->where('id',$id);
        $this->db->update('niveles',$data);
    }
    public function getNiveles()
    {
        $query=$this->db
                ->select("id,nombre_nivel")
                ->from("niveles") 
               
                ->order_by("id","asc")
                ->get();
        //echo $this->db->last_query();exit;        
         return $query->result();           
    }

}


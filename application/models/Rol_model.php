<?php
class rol_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    
        public function getTodos()
    {
        $query=$this->db
                ->select("id,nombre_rol")
                ->from("rol")
                ->order_by("nombre_rol","desc")
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->result();            
    }
    public function getId($id)
    {
        $query=$this->db
                ->select("id,nombre_rol")
                ->from("rol")
                ->order_by("nombre_rol","desc")
                 ->where(array("id_categoria_evento"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->result();            
    }
}


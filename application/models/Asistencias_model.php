<?php
class asistencias_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    

    public function insertar($data=array())
    {
        $this->db->insert('asistencias',$data);
        return $this->db->insert_id();
    }
        public function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('cursos');
    }
    public function getTodosPorId($id)
    {
        $query=$this->db
                ->select("*")
                ->from("cursos") 
                ->where(array("id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }
    public function update($data=array(),$id)
    {
        $this->db->where('id',$id);
        $this->db->update('asistencias',$data);
    }
    public function getAsistencia($id,$fecha)
    {
        $query=$this->db
                ->select("id,registro")
                ->from("asistencias")
                ->where(array('id_usuario'=>$id,'fecha2'=>$fecha))
                ->limit(1)
                ->order_by("id","DESC")
               // ->where('registro !=', 'Entrada')
                ->get();
                $row = $query->row();
                if ($query->num_rows() > 0) 
                {
                return $row->registro;
                } else 
                 {
                return 0;
                }
  
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }
 public function getAsistencia2($id,$fecha)
    {
        $query=$this->db
                ->select("id")
                ->from("asistencias")
                ->where(array('id_usuario'=>$id,'fecha2'=>$fecha))
                ->limit(1)
                ->order_by("id","DESC")
               // ->where('registro !=', 'Entrada')
                ->get();
                $row = $query->row();
                if ($query->num_rows() > 0) 
                {
                return $row->id;
                } else 
                 {
                return 0;
                }
  
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }

     public function getTodosPaginacion($pagina,$porpagina,$quehago)
    {
        switch($quehago)
        {
            case 'limit':
                $query=$this->db
                        ->select("*")
                        ->from("asistencias")  
                          ->join("usuarios", "usuarios.id=asistencias.id_usuario") 
                                        
                        ->limit($porpagina,$pagina)
                        ->order_by("asistencias.id","DESC")
                        ->get();
                return $query->result();        
            break;
            case 'cuantos':
                $query=$this->db
                        ->select("*")
                        ->from("asistencias")  
                        ->join("usuarios", "usuarios.id=asistencias.id_usuario") 
                                        
                                            
                        ->count_all_results();
                return $query;
            break;
        }
    }

}


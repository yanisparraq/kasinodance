<?php
class inspecciones_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function insertar($data=array())
    {
        $this->db->insert('inspecciones',$data);
        return $this->db->insert_id();
    }
    public function getLogin($correo,$pass)
    {
        $query=$this->db
                ->select("id,nombre_de_usuario,correo,clave_usuario,tipo_usuario_id")
                ->from("usuarios")
                ->where(array('correo'=>$correo,'clave_usuario'=>$pass))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }
    public function getTodosPaginacion($pagina,$porpagina,$quehago)
    {
        switch($quehago)
        {
            case 'limit':
                $query=$this->db
                       // ->select("*,usuarios.id")
                         ->select("*")
                        ->from("inspecciones")   
                        // ->join("tipo_usuario", "usuarios.tipo_usuario_id=tipo_usuario.id")                 
                        ->limit($porpagina,$pagina)
                        ->order_by("id_documento","DESC")
                        ->get();
                return $query->result();        
            break;
            case 'cuantos':
                $query=$this->db
                        ->select("*")
                        ->from("inspecciones")                         
                        ->count_all_results();
                return $query;
            break;
        }
    }
    public function getTodosPorId($id)
    {
        $query=$this->db
                ->select("*,usuarios.id as id")
                ->from("usuarios")              
                ->join("tipo_usuario", "usuarios.tipo_usuario_id=tipo_usuario.id")
                ->where(array("usuarios.id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }
    public function getId($id)
    {
        $query=$this->db
                ->select("id_documento")
                ->from("inspecciones")              
               // ->join("tipo_usuario", "usuarios.tipo_usuario_id=tipo_usuario.id")
               // ->where(array("usuarios.id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->num_rows();            
    }
    public function getId2($id)
    {
        $query=$this->db
                ->select("*")
                ->from("inspecciones")              
               // ->join("tipo_usuario", "usuarios.tipo_usuario_id=tipo_usuario.id")
               ->where(array("id_documento"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();                 
    }
    public function getTodosPorId2($id)
    {
        $query=$this->db
        ->select("categoria_evento.id as id_categoria,nombre,apellido,correo,pass,telefono,nombre_rol,rol_id,nombre_categoria,direccion,avatar")
        ->from("usuarios")
            ->join("rol", "usuarios.rol_id=rol.id")
            ->join("categoria_evento", "usuarios.id_categoria_evento=categoria_evento.id")     
              //   ->join("rol", "usuarios.rol_id=rol.id")
        ->where(array("usuarios.id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }
    public function getChatPorId($id)
    {
        $query=$this->db
        ->select("nombre,apellido,id,avatar")
        ->from("usuarios")               
              //   ->join("rol", "usuarios.rol_id=rol.id")
        ->where(array("id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }

    public function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('usuarios');
    }
    public function update($data=array(),$id)
    {
        $this->db->where('id_documento',$id);
        $this->db->update('inspecciones',$data);
    }

    public function prueba($id)
    {
        $query=$this->db
                ->select("*")
                ->from("inspecciones")              
              //  ->where('id_documento',15)
              //   ->join("rol", "usuarios.rol_id=rol.id")
        ->where(array("id_documento"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->result();
   //echo $this->db->last_query();exit;        
               
    }

             function getTodos2($id)
    {    
        $query=$this->db
            ->select("*")
            ->from("preguntas")
             ->where(array("preguntas.id_evento"=>$id))  
                 ->order_by("numero","ASC")            
             ->get();               
        //echo $this->db->last_query();exit;        
         return $query->result();
     }
    
}


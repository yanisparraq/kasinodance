<?php
class pagos_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function insertar($data=array())
    {
        $this->db->insert('pagos',$data);
        return $this->db->insert_id();
    }
    public function getTodos()
    {
        $query=$this->db
                ->select("pagos.id,
                      categorias.nombre_categoria,
                    usuarios.nombre_de_usuario,
                    usuarios.correo,
                    datos_personales.dni,
                    pagos.fecha,
                    pagos.monto")
                ->from("matriculaciones")
                ->join("usuarios", "usuarios.id=matriculaciones.id_usuario_alumno")
                ->join("datos_personales", "datos_personales.id_usuario=matriculaciones.id_usuario_alumno")
                 ->join("pagos", "pagos.id_matricula=matriculaciones.id")
                  ->join("cursos", "cursos.id=matriculaciones.id_curso")
                  ->join("categorias", "categorias.id=cursos.id_categoria")
                
               
                //  ->order_by("nombre_nivel","DESC")
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->result();            
    }
    public function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('pagos');
    }
    
}


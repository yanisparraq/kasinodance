<?php
class datos_personales_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function insertar($data=array())
    {
        $this->db->insert('datos_personales',$data);
        return $this->db->insert_id();
    }
    public function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('datos_personales');
    }
    public function update($data=array(),$id)
    {
        $this->db->where('id',$id);
        $this->db->update('datos_personales',$data);
    }
    public function getPersonales($dni)
    {
        $query=$this->db
                ->select("datos_personales.id_usuario")
                ->from("datos_personales")
                 ->join("usuarios", "usuarios.id=datos_personales.id_usuario")  
                ->where(array('dni'=>$dni,'pago'=>0))
                ->get();
        $row = $query->row();

        if (isset($row))
        {

               return $row->id_usuario;;
        } else 
        {
                return 0;
        }  
        //echo $this->db->last_query();exit;   
    }
}
<?php
class datos_alumnas_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function insertar($data=array())
    {
        $this->db->insert('datos_alumnas',$data);
        return $this->db->insert_id();
    }
    public function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('datos_alumnas');
    }
    public function update($data=array(),$id)
    {
        $this->db->where('id',$id);
        $this->db->update('datos_alumnas',$data);
    }
     public function getTodosPaginacion($pagina,$porpagina,$quehago)
    {
        switch($quehago)
        {
            case 'limit':
                $query=$this->db
                        ->select("datos_alumnas.id as id2,
                            datos_personales.*,
                            usuarios.*")
                        ->from("datos_alumnas")
                        ->join("datos_personales", "datos_personales.id=datos_alumnas.id_datos_personales")  
                       ->join("usuarios", "usuarios.id=datos_personales.id_usuario")   
                                         
                        ->limit($porpagina,$pagina)
                        ->order_by("datos_alumnas.id","ASD")
                        ->get();
                return $query->result();        
            break;
            case 'cuantos':
                $query=$this->db
                        ->select("datos_alumnas.id as id")
                       
                        ->from("datos_alumnas")
                        ->join("datos_personales", "datos_personales.id=datos_alumnas.id_datos_personales")  
                       ->join("usuarios", "usuarios.id=datos_personales.id_usuario")                                         
                        ->count_all_results();
                return $query;
            break;
        }
    }

    public function getTodosPorId($id)
    {
        $query=$this->db
                    ->select("datos_alumnas.id as id,
                       id_datos_personales,
                       datos_personales.id_usuario ")
                    ->from("datos_alumnas")
                    ->join("datos_personales", "datos_personales.id=datos_alumnas.id_datos_personales")  
                       ->join("usuarios", "usuarios.id=datos_personales.id_usuario")
                ->where(array("datos_alumnas.id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }
       public function getTodosPorId2($id)
    {
        $query=$this->db
                    ->select("datos_alumnas.*,
                       id_datos_personales,
                       datos_personales.*")
                    ->from("datos_alumnas")
                    ->join("datos_personales", "datos_personales.id=datos_alumnas.id_datos_personales")  
                       ->join("usuarios", "usuarios.id=datos_personales.id_usuario")
                ->where(array("datos_alumnas.id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }


    
}


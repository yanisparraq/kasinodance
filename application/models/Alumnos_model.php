<?php
class alumnos_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function insertar($data=array())
    {
        $this->db->insert('alumnos',$data);
        return $this->db->insert_id();
    }
   
    public function getTodosPaginacion($pagina,$porpagina,$quehago)
    {
        switch($quehago)
        {
            case 'limit':
                $query=$this->db
                        ->select("alumnos.id,
                            usuarios.nombre_de_usuario,
                            usuarios.correo")
                        ->from("alumnos")
                        ->join("usuarios", "usuarios.id=alumnos.id_usuario")   
                                         
                        ->limit($porpagina,$pagina)
                        ->order_by("alumnos.id","ASD")
                        ->get();
                return $query->result();        
            break;
            case 'cuantos':
                $query=$this->db
                        ->select("usuarios.id")
                        ->from("alumnos")
                        ->join("usuarios", "usuarios.id=alumnos.id_usuario")                                       
                        ->count_all_results();
                return $query;
            break;
        }
    }
    public function getTodosPorId($id)
    {
        $query=$this->db
                ->select("datos_personales.*,alumnos.id as id,
                    alumnos.id_datos_personales,
                    alumnos.id_usuario,
                    usuarios.correo")
                ->from("alumnos")              
                ->join("datos_personales","datos_personales.id=alumnos.id_datos_personales")
                // ->join("padres","padres.id_datos_personales=datos_personales.id")
                ->join("usuarios","usuarios.id=alumnos.id_usuario")
               ->where(array("alumnos.id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }
        public function getverPorId($id)
    {
        $query=$this->db
                ->select("datos_personales.*,datos_alumnas.id as id,
                    datos_alumnas.id_datos_personales,
                    datos_alumnas.id_usuario,
                    usuarios.correo")
                ->from("datos_alumnas")              
                ->join("datos_personales","datos_personales.id=datos_alumnas.id_datos_personales")
                // ->join("padres","padres.id_datos_personales=datos_personales.id")
                ->join("usuarios","usuarios.id=datos_alumnas.id_usuario")
               ->where(array("datos_alumnas.id"=>14))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }
        public function getTodosPorId3($id)
    {
        $query=$this->db
                ->select("id")
                ->from("alumnos")              
               
                ->where(array("id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->num_rows();            
    }
    public function getTodosPorId2($id)
    {
        $query=$this->db
        ->select("categoria_evento.id as id_categoria,nombre,apellido,correo,pass,telefono,nombre_rol,rol_id,nombre_categoria,direccion,avatar")
        ->from("usuarios")
            ->join("rol", "usuarios.rol_id=rol.id")
            ->join("categoria_evento", "usuarios.id_categoria_evento=categoria_evento.id")     
              //   ->join("rol", "usuarios.rol_id=rol.id")
        ->where(array("usuarios.id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }
    public function getChatPorId($id)
    {
        $query=$this->db
        ->select("nombre,apellido,id,avatar")
        ->from("usuarios")               
              //   ->join("rol", "usuarios.rol_id=rol.id")
        ->where(array("id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }

    public function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('alumnos');
    }
    public function update($data=array(),$id)
    {
        $this->db->where('id',$id);
        $this->db->update('usuarios',$data);
    }
    
}


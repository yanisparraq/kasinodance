<?php
class tipo_usuario_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    
        public function getTodos()
    {
        $query=$this->db
                ->select("*")
                ->from("tipo_usuario")
                ->order_by("id","desc")
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->result();            
    }
    public function getId($id)
    {
        $query=$this->db
                ->select("id,nombre_rol")
                ->from("rol")
                ->order_by("tipo_usuario","desc")
                 ->where(array("id_categoria_evento"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->result();            
    }
}


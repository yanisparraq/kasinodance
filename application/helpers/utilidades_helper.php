<?php
if(!function_exists("saludo"))
{
    function saludo()
    {
        return "hola #manosenelcódigo";
    }
}

if(!function_exists("qr"))
{
    function qr($url,$url2,$id)
    {

       if (!file_exists("public/qr/".$id.".png")){

         require "phpqrcode/qrlib.php";
          $text_qr = base_url().'/inspecciones/doc2/'.$id;
          $ruta_qr = "public/qr/".$id.".png";

         QRcode::png($text_qr, $ruta_qr, 'Q',5, 1);

       }

  }
}


if(!function_exists("pdf"))
{
    function pdf($datos)
    {
      require('fpdf/fpdf.php');




 class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
   // $this->Image(base_url().'public/escudo.jpg',5,12,17);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Movernos a la derecha
    $this->Cell(1);
        $this->SetDrawColor(254,254,254);
        $this->SetLineWidth(0.5);
        $this->SetTextColor(225);
    // Título    $pdf->Cell(90,4,'______________________','',0,'C',false);  
    $this->Cell(10,10,'','',0,'C');
      $this->SetFillColor(250,0,0);
    $this->Cell(19,20,utf8_decode('PERU'),'1',0,'C',true);
    $this->SetFont('Arial','B',12);
    $this->SetFillColor(0);
    $this->Cell(40,7,'','LR',0,'L',true);
    $this->SetFillColor(80);
    $this->Cell(59,7,'','LRT',0,'L',true);
     $this->SetFillColor(200);
    $this->Cell(59,7,'','LRT',1,'C',true);

    $this->Cell(30);
      
      $this->SetFillColor(0);
    $this->Cell(40,5,'Ministerio','LR',0,'L',true);
     $this->SetFillColor(80);

    $this->Cell(59,5,utf8_decode('Ejército'),'LR',0,'L',true);
     $this->SetFillColor(200);
      $this->SetTextColor(0);
    $this->Cell(59,5,utf8_decode('Comando de Educación y'),'LR',1,'L',true);

    $this->Cell(30);
     $this->SetFillColor(0);
      $this->SetTextColor(250);
     $this->Cell(40,5,'de Defensa','RL',0,'L',true);

      $this->SetFillColor(80);
       $this->SetTextColor(250);
        $this->Cell(59,5,utf8_decode('del Perú'),'LR',0,'L',true);
       $this->SetFillColor(200); 
        $this->SetTextColor(0); 
    $this->Cell(59,5,utf8_decode('Doctrina del Ejército'),'LR',1,'L',true);


        $this->Cell(30);
          $this->SetFillColor(0);
     $this->Cell(40,3,'','LRB',0,'L',true);
      $this->SetFillColor(80);
        $this->Cell(59,3,'','LRB',0,'L',true);
        $this->SetFillColor(200);
         $this->SetTextColor(0);
    $this->Cell(59,3,'','LRB',0,'C',true);
   
    // Salto de línea
    $this->Ln(10);
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(240);
    // Arial italic 8
       $this->Cell(90,3,'JEFE DEL  DEPARTAMENTO','',0,'C',false);  
    $this->Cell(90,3,'ELEMENTO TECNICO','',1,'C',false);   

     $this->Cell(90,3,'DE TELEMATICA','',0,'C',false);  
    $this->Cell(90,3,'INSPECTOR','',1,'C',false);   


$this->Ln(20);

    $this->Cell(90,4,'______________________','',0,'C',false);  
    $this->Cell(90,4,'______________________','',1,'C',false); 


     $this->Cell(90,4,'O- 116010900 -B','',0,'C',false);  
    $this->Cell(90,4,'S- 621995000 - A','',1,'C',false); 

      $this->Cell(90,4,'H. LLAPAPASCA Q','',0,'C',false);  
    $this->Cell(90,4,'R. MENDOZA P','',1,'C',false); 


      $this->Cell(90,4,'TC COM','',0,'C',false);  
    $this->Cell(90,4,'EC SAF','',1,'C',false); 
    $this->SetTitle('Informe Isnpeccion');
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,utf8_decode('Pagína').' '.$this->PageNo().' de {nb}',0,0,'C');
}
}

// Creación del objeto de la clase heredada

foreach($datos as $dato)
        {
$pdf = new FPDF();
$pdf->AliasNbPages();
$pdf->AddPage();
 $fill = false;

    $pdf->Cell(25,10,'','',0,'C',$fill);
   // $pdf->Image(base_url().'/public/qr/'.$dato->id_documento.'.png',97,250,10);
    // $pdf->Cell(10);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(150,7,utf8_decode('INFORME DE INSPECCION TECNICA Nº 030 /ST-DETEL/COEDE'),'','C',false);
    $pdf->Cell(26,10,'','',1,'C',$fill);
    $pdf->Cell(201,3,'','',1,'C',false);
    $pdf->SetFillColor(194,194,194);
    $pdf->Cell(7,5,'01.','',0,'',false);
    $pdf->SetFont('Arial','UB',10);
    $pdf->Cell(55,5,'DATOS DE IDENTIFICACION:','',1,'',false);
    $pdf->SetFont('Arial','',10);
    $pdf->Cell(7,5,'','',0,'',false);
    $pdf->Cell(35,5,'N.S.G.                      :','',0,'',false);
     $pdf->Cell(55,5,$dato->id_documento,'',1,'',false);
     $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(35,5,'SERIE NUMERO     :','',0,'',false);
     $pdf->Cell(55,5,$dato->serie,'',1,'',false);
      $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(35,5,'ARTICULO               :','',0,'',false);
     $pdf->Cell(55,5,$dato->articulo,'',1,'',false);
      $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(35,5,'COLOR                    :','',0,'',false);
     $pdf->Cell(55,5,$dato->color,'',1,'',false);
      $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(35,5,'MARCA                    :','',0,'',false);
     $pdf->Cell(55,5,$dato->marca,'',1,'',false);

     $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(35,5,'MODELO                 :','',0,'',false);
     $pdf->Cell(55,5,$dato->modelo,'',1,'',false);


     $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(35,5,'TIPO                        :','',0,'',false);
     $pdf->Cell(55,5,$dato->tipo,'',1,'',false);


     $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(35,5,'TIPO                        :','',0,'',false);
     $pdf->Cell(55,5,$dato->tipo,'',1,'',false);

    $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(35,5,'UNIDAD                   :','',0,'',false);
     $pdf->Cell(55,5,$dato->unidad,'',1,'',false);

    $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(35,5,utf8_decode('AÑO FABRICAC.    :'),'',0,'',false);
     $pdf->Cell(55,5,$dato->fecha_fabrica,'',1,'',false);



    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(7,10,'02.','',0,'',false);
    $pdf->SetFont('Arial','UB',10);
    $pdf->Cell(55,10,'DATOS ADMINISTRATIVOS:','',0,'',false);

    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(7,10,'03.','',0,'',false);
    $pdf->SetFont('Arial','UB',10);
    $pdf->Cell(55,10,utf8_decode('ESTADO DEL ARTÍCULO:'),'',1,'',false);
 

    $pdf->SetFont('Arial','',10);
    $pdf->Cell(7,5,'','',0,'',false);

    $pdf->Cell(65,5,'ESTADO FISICO DEL EQUIPO CAT    :','',0,'',false);
     $pdf->Cell(55,5,utf8_decode($dato->estadofisico),'',1,'',false);
     $pdf->Cell(7,5,'','',0,'',false);
     $pdf->Cell(65,5,'ULT. FECHA REPARAC.                      :','',0,'',false);
     $pdf->Cell(55,5,$dato->fecha_reparacion,'',1,'',false);


    $pdf->Cell(7,5,'','',0,'',false);
          $pdf->Cell(65,5,'SITUACION ACTUAL                            :','',0,'',false);
     $pdf->Cell(55,5,$dato->situación_Actual,'',1,'',false);


    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(7,10,'04.','',0,'',false);
    $pdf->SetFont('Arial','UB',10);
    $pdf->Cell(55,10,'PROBLEMAS QUE PRESENTA:','',1,'',false);

  

    $pdf->SetFont('Arial','',10);
    $pdf->Cell(7,5,'','',0,'',false);
        $pdf->MultiCell(0,5,utf8_decode($dato->problema));
  
    // $pdf->Cell(55,5,utf8_decode('-   '.$dato->problema),'',1,'',false);
     

    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(7,10,'05.','',0,'',false);
    $pdf->SetFont('Arial','UB',10);
    $pdf->Cell(55,10,'ANALISIS:','',1,'',false);

  

    $pdf->SetFont('Arial','',10);
    $pdf->Cell(7,5,'','',0,'',false);

    $pdf->MultiCell(0,5,utf8_decode('-   '.$dato->analisi));
    
    


         $pdf->SetFont('Arial','B',10);
  

    $pdf->Cell(55,10,utf8_decode('RECOMENDACIÓN:'),'',1,'',false);

  

    $pdf->SetFont('Arial','',10);
    $pdf->Cell(7,5,'','',0,'',false);

   $pdf->MultiCell(0,5,utf8_decode('-   '.$dato->recomendacion));
    
  

          $pdf->SetFont('Arial','B',10);
   

    $pdf->Cell(180,10,'Chorrillos,'.  fecha($dato->fecha_registro),'',1,'R',false);   
$pdf->Ln(20);



  $pdf->Output('D',$dato->id_documento.'_TECNICA.pdf');

}



}
}if(!function_exists("pdf2"))
{
    function pdf2($datos)
    {
      require('fpdf/fpdf.php');



$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',16);
$pdf->Cell(40,10,'¡Hola, Mundo!');
$pdf->Output();}
}

/**
 * validación set Value select
 **/
   if(!function_exists('set_value_select'))
   {
     function set_value_select($result=array(),$post,$campo,$valor)
     {
        if(sizeof($result)==0)
                {
                    if(isset($_POST[$post]) and $_POST[$post]==$valor)
                    {
                         return 'selected="true"';   
                    }else
                    {
                        return '';
                    }
                }else
                {
                    if($campo==$valor)
                    {
                         return 'selected="true"';   
                    }else
                    {
                        return '';
                    }
                }
     }
   } 
   /**
 * validación set Value Producción input
 **/
   if(!function_exists('set_value_input'))
   {
     function set_value_input($result=array(),$post,$campo)
     {
        if(sizeof($result)==0)
                {
                    if(isset($_POST[$post]))
                    {
                         return $_POST[$post];   
                    }else
                    {
                        return '';
                    }
                }else
                {
                    if($campo)
                    {
                         return $campo;   
                    }else
                    {
                        return '';
                    }
                }
     }
   }
/**
 *  Detectar el dispositivo para cargar css web o mobile
 * Retorna True si es un disposivo móvil
 */
 if ( ! function_exists('detectar_SO') )
    {
        function detectar_SO()
        {
            $es_movil=FALSE; //Aquí se declara la variable falso o verdadero XD 
              $usuario = $_SERVER['HTTP_USER_AGENT']; //Con esta leemos la info de su navegador 
             $usuarios_moviles = "Android, AvantGo, Blackberry, Blazer, Cellphone, Danger, DoCoMo, EPOC,EudoraWeb, Handspring, HTC, Kyocera, LG, MMEF20, MMP, MOT-V, Mot, Motorola, NetFront, Newt,Nokia, Opera Mini, Palm, Palm, PalmOS, PlayStation Portable, ProxiNet, Proxinet, SHARP-TQ-GX10,Samsung, Small, Smartphone, SonyEricsson, SonyEricsson, Symbian, SymbianOS, TS21i-10, UP.Browser,UP.Link, WAP, webOS, Windows CE, hiptop, iPhone, iPod, portalmmm, Elaine/3.0, OPWV"; 
            $navegador_usuario = explode(',',$usuarios_moviles);   
            foreach($navegador_usuario AS $navegador){ 
            if(@preg_match('/'.trim($navegador).'/',$usuario)){     
             $es_movil=TRUE;       
            }  
              }
             
             if($es_movil==TRUE){ 
             
               return true; 
             
            } 
             
            else{   
             
              return false;
             
                } 
        }
    }   
/**
    * formatea fecha
    * */
    if(!function_exists("fecha"))
    {
        function fecha($fecha){
          $dia=substr($fecha,8,2);
          $mes=substr($fecha,5,2);
          $anio=substr($fecha,0,4);
        switch ($mes){
          case '01':
          $mes="Enero";
          break;
          case '02':
          $mes="Febrero";
          break;
          case '03':
          $mes="Marzo";
          break;
          case '04':
          $mes="Abril";
          break;
          case '05':
          $mes="Mayo";
          break;
          case '06':
          $mes="Junio";
          break;
          case '07':
          $mes="Julio";
          break;
          case '08':
          $mes="Agosto";
          break;
          case '09':
          $mes="Septiembre";
          break;
          case '10':
          $mes="Octubre";
          break;
          case '11':
          $mes="Noviembre";
          break;
          case '12':
          $mes="Diciembre";
          break;
        }
        $fecha=$dia." de ".$mes." de ".$anio;
        return $fecha; 
        }
    }
    /**
     * calcular edad desde fecha de nacimiento
     * */
     if(!function_exists("calculaEdad"))
     {
        function calculaEdad($fecha)
        {
            //fecha actual
 
                $dia=date("j");
                $mes=date("n");
                $ano=date("Y");
                 
                //fecha de nacimiento
                  $dianaz=substr($fecha,8,2);
                  $mesnaz=substr($fecha,5,2);
                  $anonaz=substr($fecha,0,4);
              
                 
                //si el mes es el mismo pero el día inferior aun no ha cumplido años, le quitaremos un año al actual
                 
                if (($mesnaz == $mes) && ($dianaz > $dia)) {
                $ano=($ano-1); }
                 
                //si el mes es superior al actual tampoco habrá cumplido años, por eso le quitamos un año al actual
                 
                if ($mesnaz > $mes) {
                $ano=($ano-1);}
                 
                //ya no habría mas condiciones, ahora simplemente restamos los años y mostramos el resultado como su edad
                 
                $edad=($ano-$anonaz);
                 
                return $edad;
        }
     }
 
 

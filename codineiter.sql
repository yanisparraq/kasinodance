-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-07-2021 a las 20:29:34
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `codineiter`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE `alumnos` (
  `id` int(4) NOT NULL,
  `id_usuario` int(100) NOT NULL,
  `id_datos_personales` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`id`, `id_usuario`, `id_datos_personales`) VALUES
(1, 266, 112),
(2, 267, 113),
(3, 269, 115);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `analisis`
--

CREATE TABLE `analisis` (
  `id_analisis` int(7) NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `id_inspeccion` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencias`
--

CREATE TABLE `asistencias` (
  `id` int(6) NOT NULL,
  `id_usuario` int(6) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha2` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `hora` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `hora_salida` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `registro` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `asistencias`
--

INSERT INTO `asistencias` (`id`, `id_usuario`, `fecha`, `fecha2`, `hora`, `hora_salida`, `registro`) VALUES
(50, 184, '2021-07-05 19:29:48', '2021-07-05', '03:29:pm', '03:32:pm', '0'),
(51, 184, '2021-07-05 19:33:10', '2021-07-05', '03:33:pm', '03:33:pm', '0'),
(52, 184, '2021-07-05 19:33:28', '2021-07-05', '03:33:pm', '03:33:pm', '0'),
(53, 184, '2021-07-05 19:33:54', '2021-07-05', '03:33:pm', '03:34:pm', '0'),
(54, 268, '2021-07-05 19:37:12', '2021-07-05', '03:37:pm', '03:37:pm', '0'),
(55, 268, '2021-07-05 19:37:24', '2021-07-05', '03:37:pm', '03:37:pm', '0'),
(56, 268, '2021-07-05 19:37:31', '2021-07-05', '03:37:pm', '03:37:pm', '0'),
(57, 268, '2021-07-05 19:37:46', '2021-07-05', '03:37:pm', '03:37:pm', '0'),
(58, 264, '2021-07-15 17:39:58', '2021-07-15', '01:39:pm', '01:40:pm', '0'),
(59, 268, '2021-07-15 23:48:08', '2021-07-15', '07:48:pm', '07:48:pm', '0'),
(60, 264, '2021-07-15 23:48:14', '2021-07-15', '07:48:pm', '07:48:pm', '0'),
(61, 265, '2021-07-15 23:48:19', '2021-07-15', '07:48:pm', '07:48:pm', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(6) NOT NULL,
  `nombre_categoria` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `id_categori_pa` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre_categoria`, `id_categori_pa`) VALUES
(23, 'dance', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_evento`
--

CREATE TABLE `categoria_evento` (
  `id` int(4) NOT NULL,
  `nombre_categoria` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categoria_evento`
--

INSERT INTO `categoria_evento` (`id`, `nombre_categoria`) VALUES
(1, 'GUIA MAYOR'),
(2, 'CONQUISTADOR'),
(3, 'AVENTUREROS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `id` int(6) NOT NULL,
  `id_categoria` int(6) NOT NULL,
  `id_usuario_profesor` int(6) NOT NULL,
  `periodo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `costo` int(6) NOT NULL,
  `nivel` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id`, `id_categoria`, `id_usuario_profesor`, `periodo`, `costo`, `nivel`) VALUES
(10, 23, 264, '07/15/2021 - 07/15/2021', 10, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_alumnas`
--

CREATE TABLE `datos_alumnas` (
  `id` int(11) NOT NULL,
  `lugar_nacimiento` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `malla` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `zapatilla` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `tacon` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `peso` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `sangre` varchar(11) COLLATE utf8_spanish_ci NOT NULL,
  `anio_escolar` varchar(11) COLLATE utf8_spanish_ci NOT NULL,
  `colegio` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `seguro` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `nombre_seguro` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `limitaciones` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `id_datos_personales` int(6) NOT NULL,
  `id_usuario` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `datos_alumnas`
--

INSERT INTO `datos_alumnas` (`id`, `lugar_nacimiento`, `malla`, `zapatilla`, `tacon`, `peso`, `sangre`, `anio_escolar`, `colegio`, `seguro`, `nombre_seguro`, `limitaciones`, `id_datos_personales`, `id_usuario`) VALUES
(14, 'sdfgsdf\r\n', '23423', 'sdfsdf', 'sdfsdf', 'sdfsdfsdf', 'sdfasdf', 'sdfasdf', 'sdfsdf', 'sdfsdf', 'sdfsdf', 'kjhkiuh', 129, 283);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_personales`
--

CREATE TABLE `datos_personales` (
  `id` int(4) NOT NULL,
  `nombre1` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `nombre2` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellido2` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `dni` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_nacimiento` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `direccion_habitacion` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `direccion_trabajo` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `telefono_movil` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `telefono_fijo` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `instagran` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `facebook` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `tiltok` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `id_usuario` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `datos_personales`
--

INSERT INTO `datos_personales` (`id`, `nombre1`, `nombre2`, `apellido`, `apellido2`, `dni`, `fecha_nacimiento`, `direccion_habitacion`, `direccion_trabajo`, `telefono_movil`, `telefono_fijo`, `instagran`, `facebook`, `tiltok`, `id_usuario`) VALUES
(109, '', '', '', '', '', '', '', '', '', '', '', '', '', '184'),
(110, 'Andres', '', 'Paz', '', '1234', '', '', '', '', '', '', '', '', '264'),
(111, 'Andre', '', 'Bello', '', '123456', '', '', '', '', '', '', '', '', '265'),
(112, 'primer', 'segundito', 'per', 'iugi', '86868768', '0006-07-08', '876', '876', '8765', '876', '876', '8', '76876', '266'),
(113, 'yanis', 'alexander', 'parra', 'quiros', '16111043', '2021-01-01', 'direccion habitacion', 'trabajo', '123456', '12345', 'instagran', 'facebook', 'tiktok', '267'),
(114, 'pruena@gmail.com', '', 'parra', '', '1', '', '', '', '', '', '', '', '', '268'),
(115, 'hhsdf', 'sdfsdf', 'sdfsdf', 'sdfsdf', '1231', '0034-02-23', 'sdf', 'sdf', '', '', 'sdew', 'werwe', 'dqwq', '269'),
(129, 'jhj', 'dfgsdrg', 'svedwsd', 'sdfsdf', '123123', '2021-01-01', '', '', '', '', '', '', '', '283');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento`
--

CREATE TABLE `evento` (
  `id` int(30) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_evento_id` int(6) NOT NULL,
  `id_categoria_evento` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `evento`
--

INSERT INTO `evento` (`id`, `nombre`, `tipo_evento_id`, `id_categoria_evento`) VALUES
(2, 'Conexión Bíblica Guía Mayor', 1, 1),
(12, 'Conexión Bíblica Aventureros', 1, 3),
(13, 'AVENTUREROS', 1, 3),
(14, 'CONQUISTADOR', 1, 2),
(15, 'Conexión Guia Mayor', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inspecciones`
--

CREATE TABLE `inspecciones` (
  `id_documento` int(4) NOT NULL,
  `serie` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `articulo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `marca` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `modelo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `color` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_fabrica` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `unidad` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `estadofisico` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_reparacion` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_registro` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `situación_Actual` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `problema` varchar(1000) COLLATE utf8_spanish_ci NOT NULL,
  `analisi` varchar(1000) COLLATE utf8_spanish_ci NOT NULL,
  `recomendacion` varchar(1000) COLLATE utf8_spanish_ci NOT NULL,
  `id_usuario` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `inspecciones`
--

INSERT INTO `inspecciones` (`id_documento`, `serie`, `articulo`, `marca`, `modelo`, `tipo`, `color`, `fecha_fabrica`, `unidad`, `estadofisico`, `fecha_reparacion`, `fecha_registro`, `situación_Actual`, `problema`, `analisi`, `recomendacion`, `id_usuario`) VALUES
(21, 'SERIE', 'ARTICULO', 'MARCA|', 'MODELO', 'TIPO', 'COLOR', '2021-01-01', 'UNIDAD', 'ESTADO FISICO', '2021-01-01', '2021-04-15 18:26:28.989237', 'SITUACION', 'PROBLEMAS QUE PRESENTA ', 'aNALISIS', 'RECOMENDACIONES', 184),
(22, 'oioij', 'oijoj', 'oijo', 'jio', 'jo', 'ijo', '2021-01-01', 'jo', 'ijo', '2021-01-01', '2021-05-07 17:33:03.425477', 'ijo', 'iho', 'kjñohi', 'ñkjbiug8', 184);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matriculaciones`
--

CREATE TABLE `matriculaciones` (
  `id` int(6) NOT NULL,
  `id_usuario_alumno` int(6) NOT NULL,
  `id_curso` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `matriculaciones`
--

INSERT INTO `matriculaciones` (`id`, `id_usuario_alumno`, `id_curso`) VALUES
(15, 267, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `niveles`
--

CREATE TABLE `niveles` (
  `id` int(6) NOT NULL,
  `nombre_nivel` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `edad_minima` int(6) NOT NULL,
  `edad_mayor` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `niveles`
--

INSERT INTO `niveles` (`id`, `nombre_nivel`, `edad_minima`, `edad_mayor`) VALUES
(5, 'Primer', 5, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `padres`
--

CREATE TABLE `padres` (
  `id` int(6) NOT NULL,
  `nombres_padres` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos_padres` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `dni_padres` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `direccion_habitacion_padres` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `telefono_padres` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `direccion_trabajo_padres` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `telefono_trabajo_padres` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `correo_padres` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `id_datos_personales` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `padres`
--

INSERT INTO `padres` (`id`, `nombres_padres`, `apellidos_padres`, `dni_padres`, `direccion_habitacion_padres`, `telefono_padres`, `direccion_trabajo_padres`, `telefono_trabajo_padres`, `correo_padres`, `id_datos_personales`) VALUES
(1, 'neidis', 'parra', '16593924', 'BORAURE', '88888', 'CASa', 'casa', 'neidis@gmil.con', 31),
(2, 'madre', 'madre', '12312', 'madre', 'madre', 'aspkfmapsfa', 'aoifnoaifa', 'afasd@gmail.com', 32),
(3, 'padre', 'xckjnsdasnb', '1231234123', 'jibasdijsdf', '23142134', '213412', '123123', 'correro@gmail.com', 32),
(4, 'yoleida', 'quiroz', '5458223', 'urb. nuevo boraure', '123431', '555555555', '3333333', 'correo@gmail.com', 33),
(5, 'pablo', 'parra', '1234567', 'pueblo nuevo', '9999999', '8888888', '8888888', 'yanis@gmail.com', 33),
(6, 'yoleida', 'quiroz', '5458223', 'urb. nuevo boraure', '123431', '555555555', '3333333', 'correo@gmail.com', 34),
(7, 'pablo', 'parra', '1234567', 'pueblo nuevo', '9999999', '8888888', '8888888', 'yanis@gmail.com', 34),
(8, 'yoleida', 'quiroz', '5458223', 'urb. nuevo boraure', '123431', '555555555', '3333333', 'correo@gmail.com', 35),
(9, 'pablo', 'parra', '1234567', 'pueblo nuevo', '9999999', '8888888', '8888888', 'yanis@gmail.com', 35),
(10, '8', '8', '8', '8', '8', '8', '8', '8', 36),
(11, '8', '8', '8', '8', '8', '8', '8', '8', 36),
(12, '8', '8', '8', '8', '8', '8', '8', '8', 37),
(13, '8', '8', '8', '8', '8', '8', '8', '8', 37),
(14, '8', '8', '8', '8', '8', '8', '8', '8', 38),
(15, '8', '8', '8', '8', '8', '8', '8', '8', 38),
(16, '8', '8', '8', '8', '8', '8', '8', '8', 39),
(17, '8', '8', '8', '8', '8', '8', '8', '8', 39),
(18, '8', '8', '8', '8', '8', '8', '8', '8', 40),
(19, '8', '8', '8', '8', '8', '8', '8', '8', 40),
(20, '8', '8', '8', '8', '8', '8', '8', '8', 41),
(21, '8', '8', '8', '8', '8', '8', '8', '8', 41),
(22, '8', '8', '8', '8', '8', '8', '8', '8', 42),
(23, '8', '8', '8', '8', '8', '8', '8', '8', 42),
(24, '7', '7', '7', '7', '7', '7', '7', '7', 43),
(25, '7', '7', '7', '7', '7', '7', '7', '7', 43),
(26, '3', '3', '3', '3', '3', '3', '3', '3', 44),
(27, '3', '3', '3', '3', '3', '3', '3', '3', 44),
(28, '9', '9', '9', '9', '9', '9', '9', '9', 45),
(29, '9', '9', '9', '9', '9', '9', '9', '9', 45),
(30, '6', '6', '6', '6', '6', '6', '6', '6', 46),
(31, '6', '6', '6', '6', '6', '6', '6', '6', 46),
(32, '', '', '', '', '', '', '', '', 47),
(33, '', '', '', '', '', '', '', '', 48),
(34, '', '', '', '', '', '', '', '', 49),
(35, '', '', '', '', '', '', '', '', 50),
(36, '', '', '', '', '', '', '', '', 51),
(37, '', '', '', '', '', '', '', '', 52),
(38, '', '', '', '', '', '', '', '', 53),
(39, '', '', '', '', '', '', '', '', 54),
(40, '', '', '', '', '', '', '', '', 55),
(41, '', '', '', '', '', '', '', '', 56),
(42, '', '', '', '', '', '', '', '', 57),
(43, '', '', '', '', '', '', '', '', 58),
(44, '', '', '', '', '', '', '', '', 59),
(45, '', '', '', '', '', '', '', '', 60),
(46, '', '', '', '', '', '', '', '', 61),
(47, '', '', '', '', '', '', '', '', 62),
(48, '', '', '', '', '', '', '', '', 63),
(49, '', '', '', '', '', '', '', '', 64),
(50, '', '', '', '', '', '', '', '', 65),
(51, '', '', '', '', '', '', '', '', 66),
(52, '', '', '', '', '', '', '', '', 67),
(53, '', '', '', '', '', '', '', '', 68),
(54, '', '', '', '', '', '', '', '', 69),
(55, '', '', '', '', '', '', '', '', 70),
(56, '', '', '', '', '', '', '', '', 71),
(57, '', '', '', '', '', '', '', '', 72),
(58, '', '', '', '', '', '', '', '', 73),
(59, '', '', '', '', '', '', '', '', 74),
(60, '', '', '', '', '', '', '', '', 75),
(61, '', '', '', '', '', '', '', '', 76),
(62, '', '', '', '', '', '', '', '', 77),
(63, '', '', '', '', '', '', '', '', 78),
(64, '', '', '', '', '', '', '', '', 79),
(65, '', '', '', '', '', '', '', '', 80),
(66, '', '', '', '', '', '', '', '', 84),
(67, '', '', '', '', '', '', '', '', 85),
(68, '', '', '', '', '', '', '', '', 86),
(69, '', '', '', '', '', '', '', '', 87),
(70, 'asfdkjbki', 'ugiugilbu', 'liugilu', 'bvilubilug', 'iubiu', 'hgiouhiouh', 'giuhgiu', 'giugi', 88),
(71, 'asfdkjbki', 'ugiugilbu', 'liugilu', 'bvilubilug', 'iubiu', 'hgiouhiouh', 'giuhgiu', 'giug@gmail.com', 89),
(72, 'asfdkjbki', 'ugiugilbu', 'liugilu', 'bvilubilug', 'iubiu', 'hgiouhiouh', 'giuhgiu', 'giug@gmail.com', 90),
(73, 'asfdkjbki', 'ugiugilbu', 'liugilu', 'bvilubilug', 'iubiu', 'hgiouhiouh', 'giuhgiu', 'giug@gmail.com', 91),
(74, '', '', '', '', '', '', '', '', 92),
(75, '', '', '', '', '', '', '', '', 93),
(76, '', '', '', '', '', '', '', '', 94),
(77, '', '', '', '', '', '', '', '', 95),
(78, '', '', '', '', '', '', '', '', 96),
(79, '', '', '', '', '', '', '', '', 97),
(80, '', '', '', '', '', '', '', '', 98),
(81, 'madre', 'apellidos', '123456', 'direccion', '7689768', '8jhgjgvj', '875875', 'yasdas@gmail.com', 99),
(82, '', '', '', '', '', '', '', '', 100),
(83, '', '', '', '', '', '', '', '', 101),
(84, '', '', '', '', '', '', '', '', 102),
(85, '', '', '', '', '', '', '', '', 103),
(86, '', '', '', '', '', '', '', '', 104),
(87, '', '', '', '', '', '', '', '', 105),
(88, '', '', '', '', '', '', '', '', 106),
(89, 'neidis', 'amaro', '16593924', 'direccion', '123123', 'trabajo', '1234', 'neidis@gmil.con', 107),
(90, 'yanis', 'parra', '16111043', 'direccion', '121312', '123121', '23423', 'yanisparra@gmail.com', 107),
(91, '', '', '', '', '', '', '', '', 116),
(92, '', '', '', '', '', '', '', '', 117),
(93, '', '', '', '', '', '', '', '', 118),
(94, '', '', '', '', '', '', '', '', 119),
(95, 'gvsdfgsdf', '', '', '', '', '', '', '', 120),
(96, 'sd', 'sdf', '2', '23234', 'sdfwe', 'weffsdf\r\n', 'sdfsdfsdf', 'yanisparfra@gmail.com', 121),
(97, 'ewfw', 'efw', '123', 'dcsdc\r\ncds', 'dcdsc', 'sdcsdcwe\r\n', 'sdvsdvsd', 'sdcsdc@gma.com', 122),
(98, 'ewfw', 'efw', '123', 'dcsdc\r\ncds', 'dcdsc', 'sdcsdcwe\r\n', 'sdvsdvsd', 'sdcsdc@gma.com', 123),
(99, 'ewfw', 'efw', '123', 'dcsdc\r\ncds', 'dcdsc', 'sdcsdcwe\r\n', 'sdvsdvsd', 'sdcsdc@gma.com', 124),
(100, 'ewfw', 'efw', '123', 'dcsdc\r\ncds', 'dcdsc', 'sdcsdcwe\r\n', 'sdvsdvsd', 'sdcsdc@gma.com', 125),
(101, 'ewfw', 'efw', '123', 'dcsdc\r\ncds', 'dcdsc', 'sdcsdcwe\r\n', 'sdvsdvsd', 'sdcsdc@gma.com', 126),
(102, 'sdfsdf', 'fsdfsdg', '86', '86', '8', '658', '58', '7@dsdf.co', 127),
(103, 'sdfsdf', 'fsdfsdg', '86', '86', '8', '658', '58', '7@dsdf.co', 128),
(104, 'asdfasd', 'asdasd', '87', '58', '758', '58', '3', '8sadfasf@dsd.coom', 128),
(105, 'i', 'sdfsasdf', '2', 'sdfsdf\r\n', 'f', 'sdfgsd', 'gvsdf', 'gsdgsd@dvsd.cod', 129),
(106, 'fsg', 'sdfsd', '23', 'sdfsdf\r\nsdf', 'f', 'fdw', 'sdfsdf', 'sdfsdf@asdfsd.com', 129);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `id` int(10) NOT NULL,
  `fecha` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `id_matricula` int(10) NOT NULL,
  `id_usuario` int(10) NOT NULL,
  `monto` int(11) NOT NULL,
  `id_tipo_pago` int(10) NOT NULL,
  `referencia` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`id`, `fecha`, `id_matricula`, `id_usuario`, `monto`, `id_tipo_pago`, `referencia`) VALUES
(7, '2021-07-15', 15, 267, 10, 0, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` int(4) NOT NULL,
  `nombre1` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `nombre2` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos2` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `problemas`
--

CREATE TABLE `problemas` (
  `id_problema` int(7) NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_inspeccion` int(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recomendaciones`
--

CREATE TABLE `recomendaciones` (
  `id` int(7) NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `id_inspeccion` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `representante`
--

CREATE TABLE `representante` (
  `id` int(6) NOT NULL,
  `nombre_representante` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellido_representante` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `dni_representante` int(7) NOT NULL,
  `correo_representante` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `id_datos_personales` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `representante`
--

INSERT INTO `representante` (`id`, `nombre_representante`, `apellido_representante`, `dni_representante`, `correo_representante`, `id_datos_personales`) VALUES
(33, '', '', 0, '', '74'),
(34, '', '', 0, '', '75'),
(35, '', '', 0, '', '76'),
(36, '', '', 0, '', '77'),
(37, '', '', 0, '', '78'),
(38, '', '', 0, '', '79'),
(39, '', '', 0, '', '80'),
(40, '', '', 0, '', '84'),
(41, '', '', 0, '', '85'),
(42, '', '', 0, '', '86'),
(43, '', '', 0, '', '87'),
(44, '', '', 0, 'kjbuhaksfdas', '96'),
(45, 'yanis', 'parra', 123456, 'representante@gmail.com', '97'),
(46, 'jugjkugkug', 'khgkgkug', 654764, 'eber@gmail.com', '98'),
(47, '', '', 0, 'lkuhgookl@gmail.com', '99'),
(48, '', '', 0, '', '100'),
(49, '', '', 0, '', '101'),
(50, '', '', 0, '', '102'),
(51, '', '', 0, '', '103'),
(52, '', '', 0, '', '104'),
(53, 'este', '', 0, '', '105'),
(54, '', '', 0, 'yasdq@gdkfdf.com', '106'),
(55, 'yanis', 'parra', 12111043, 'correo@gmail.com', '107'),
(56, '', '', 0, '', '116'),
(57, '', '', 0, '', '117'),
(58, '', '', 0, '', '118'),
(59, '', '', 0, '', '119'),
(60, '', '', 0, '', '120'),
(61, '', '', 0, '', '121'),
(62, 'u', 'yfj', 1, 'jh', '128'),
(63, 'dfsadf', 'dasd', 23, 'dasfasdf@asfdasd.com', '129');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id` int(5) NOT NULL,
  `nombre_rol` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `nombre_rol`) VALUES
(1, 'Alumno'),
(2, 'Alumno Niña'),
(3, 'Personal Administrativo'),
(4, 'Profesor'),
(5, 'Personal de limpieza');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_eventos`
--

CREATE TABLE `tipo_eventos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(25) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipo_eventos`
--

INSERT INTO `tipo_eventos` (`id`, `nombre`) VALUES
(1, '4 Opciones'),
(2, 'Verdadero ó Falso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `id` int(10) NOT NULL,
  `nombre_tipo_usuario` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`id`, `nombre_tipo_usuario`) VALUES
(1, 'Administrador'),
(2, 'Restringido'),
(3, 'Normal');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre_de_usuario` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `clave_usuario` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_usuario_id` int(5) NOT NULL,
  `id_rol` int(5) NOT NULL,
  `pago` int(2) NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre_de_usuario`, `correo`, `clave_usuario`, `tipo_usuario_id`, `id_rol`, `pago`, `fecha_registro`) VALUES
(184, 'parra yaniosd', 'admin@admin.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1, 0, 0, '0000-00-00 00:00:00'),
(264, 'Andres Paz', 'andres@gmail.com', '8cb2237d0679ca88db6464eac60da96345513964', 3, 4, 0, '0000-00-00 00:00:00'),
(265, 'Andre Bello', 'andres@andres.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1, 4, 0, '0000-00-00 00:00:00'),
(266, 'primer per', 'correo@gmail.com', 'd8d49a04a66dd4ab1cc102ae1b129ecef2e9cd17', 2, 1, 0, '2021-07-01 04:25:49'),
(267, 'yanis parra', 'yanis@gmail.com', 'ba0c27942d710eb90a28e14c7ce3d4ebadd20a0d', 2, 1, 0, '2021-07-01 08:23:02'),
(268, 'pruena@gmail.com parra', '1@1.com', '356a192b7913b04c54574d18c28d46e6395428ab', 3, 5, 0, '2021-07-05 19:37:03'),
(269, 'hhsdf sdfsdf', 'sdfsdf@sd.com', 'c348c1794df04a0473a11234389e74a236833822', 2, 1, 0, '2021-07-06 09:26:14'),
(283, 'dfsadf', 'dasfasdf@asfdasd.com', '23', 2, 2, 0, '2021-07-06 15:25:33');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `asistencias`
--
ALTER TABLE `asistencias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria_evento`
--
ALTER TABLE `categoria_evento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `datos_alumnas`
--
ALTER TABLE `datos_alumnas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `datos_personales`
--
ALTER TABLE `datos_personales`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `evento`
--
ALTER TABLE `evento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inspecciones`
--
ALTER TABLE `inspecciones`
  ADD PRIMARY KEY (`id_documento`),
  ADD UNIQUE KEY `id_documento` (`id_documento`),
  ADD UNIQUE KEY `referencia` (`serie`);

--
-- Indices de la tabla `matriculaciones`
--
ALTER TABLE `matriculaciones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `niveles`
--
ALTER TABLE `niveles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `padres`
--
ALTER TABLE `padres`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `problemas`
--
ALTER TABLE `problemas`
  ADD PRIMARY KEY (`id_problema`),
  ADD UNIQUE KEY `id_problema` (`id_problema`);

--
-- Indices de la tabla `recomendaciones`
--
ALTER TABLE `recomendaciones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `representante`
--
ALTER TABLE `representante`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_eventos`
--
ALTER TABLE `tipo_eventos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `asistencias`
--
ALTER TABLE `asistencias`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `categoria_evento`
--
ALTER TABLE `categoria_evento`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `datos_alumnas`
--
ALTER TABLE `datos_alumnas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `datos_personales`
--
ALTER TABLE `datos_personales`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT de la tabla `evento`
--
ALTER TABLE `evento`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `inspecciones`
--
ALTER TABLE `inspecciones`
  MODIFY `id_documento` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `matriculaciones`
--
ALTER TABLE `matriculaciones`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `niveles`
--
ALTER TABLE `niveles`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `padres`
--
ALTER TABLE `padres`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `problemas`
--
ALTER TABLE `problemas`
  MODIFY `id_problema` int(7) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `recomendaciones`
--
ALTER TABLE `recomendaciones`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `representante`
--
ALTER TABLE `representante`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tipo_eventos`
--
ALTER TABLE `tipo_eventos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=284;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
